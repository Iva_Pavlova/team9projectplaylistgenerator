﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Services.Tests;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.IntegrationTests.SongServiceTests
{
    [TestClass]
    public class LoadSongsInDbAsync_Should
    {
        [TestMethod]
        public async Task LoadSongs_When_NoSongsInDb()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(LoadSongsInDbAsync_Should));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                var genreService = new GenreService(arrangeContext, Utils.Mapper);
                await genreService.LoadGenresInDbAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var sut = new SongService(actContext, Utils.Mapper);

                //Act
                await sut.LoadSongsInDbAsync();

                //Assert
                Assert.IsNotNull(await actContext.Songs.FirstAsync());
                Assert.IsTrue(actContext.Songs.Count() >= 1500);
            }
        }
    }
}
