﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Tests;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.IntegrationTests.SongServiceTests
{
    [TestClass]
    public class UpdateSongsInDbAsync_Should
    {
        [TestMethod]
        public async Task NotChangeSongIds()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(NotChangeSongIds));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedSongs = await actContext.Songs.ToListAsync();
                var sut = new SongService(actContext, Utils.Mapper);

                //Act
                await sut.UpdateSongsInDbAsync();
                var actualSongs = await actContext.Songs.ToListAsync();

                //Assert
                Assert.AreEqual(expectedSongs.Count, actualSongs.Count);

                for (int i = 0; i < actualSongs.Count; i++)
                {
                    Assert.AreEqual(expectedSongs[i].Id, actualSongs[i].Id);
                }
            }
        }

        [TestMethod]
        public async Task UpdateSongs_When_ApiKeyValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(UpdateSongs_When_ApiKeyValid));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("Test", "User"));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));


                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsAdmin = true;

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new SongService(actContext, Utils.Mapper);

                //Act
                await sut.UpdateSongsInDbAsync(true, key);

                //Assert
                Assert.AreEqual(2, actContext.Songs.Count());
            }
        }
    }
}
