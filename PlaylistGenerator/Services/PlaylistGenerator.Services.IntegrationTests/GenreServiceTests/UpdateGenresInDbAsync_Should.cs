﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Tests;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.IntegrationTests.GenreServiceTests
{
    [TestClass]
    public class UpdateGenresInDbAsync_Should
    {
        [TestMethod]
        public async Task NotChangeGenreIds()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(NotChangeGenreIds));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    PictureURL = "PictureUrl2",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name3",
                    DeezerId = "DeezerId3",
                    PictureURL = "PictureUrl3",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name4",
                    DeezerId = "DeezerId4",
                    PictureURL = "PictureUrl4",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedGenres = await actContext.Genres.ToListAsync();
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act
                await sut.UpdateGenresInDbAsync();
                var actualGenres = await actContext.Genres.ToListAsync();

                //Assert
                Assert.AreEqual(expectedGenres.Count, actualGenres.Count);

                for (int i = 0; i < actualGenres.Count; i++)
                {
                    Assert.AreEqual(expectedGenres[i].Id, actualGenres[i].Id);
                }
            }
        }

        [TestMethod]
        public async Task UpdateGenres_When_ApiKeyValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(UpdateGenres_When_ApiKeyValid));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    PictureURL = "PictureUrl2",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name3",
                    DeezerId = "DeezerId3",
                    PictureURL = "PictureUrl3",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name4",
                    DeezerId = "DeezerId4",
                    PictureURL = "PictureUrl4",
                });

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsAdmin = true;

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act
                await sut.UpdateGenresInDbAsync(true, key);

                //Assert
                Assert.AreEqual(4, actContext.Genres.Count());
            }
        }
    }
}
