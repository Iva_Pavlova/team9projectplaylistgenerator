using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Services.Tests;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.IntegrationTests
{
    [TestClass]
    public class LoadGenresInDbAsync_Should
    {
        [TestMethod]
        public async Task LoadGenres_When_NoGenresInDb()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(LoadGenres_When_NoGenresInDb));

            var context = new PlaylistGeneratorDbContext(options);
            var sut = new GenreService(context, Utils.Mapper);

            //Act
            await sut.LoadGenresInDbAsync();

            //Assert
            Assert.IsNotNull(await context.Genres.FirstAsync());
            Assert.AreEqual(5, context.Genres.Count());
        }
    }
}
