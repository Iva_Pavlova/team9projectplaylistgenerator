﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.ArtistServiceTests
{
    [TestClass]
    public class GetArtistByIdAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectArtist()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectArtist));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(new Artist
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    ArtistPageURL = "ArtistPageUrl",
                    PictureURL = "PictureUrl",
                    AlbumCount = 3,
                    FanCount = 223,
                    SongListUrl = "SongListUrl"
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedArtist = await actContext.Artists.FirstAsync();
                var sut = new ArtistService(actContext, Utils.Mapper);

                //Act
                var actualArtist = await sut.GetArtistByIdAsync(expectedArtist.Id);

                //Assert
                Assert.AreEqual(expectedArtist.Id, actualArtist.Id);
                Assert.AreEqual(expectedArtist.Name, actualArtist.Name);
                Assert.AreEqual(expectedArtist.ArtistPageURL, actualArtist.ArtistPageURL);
                Assert.AreEqual(expectedArtist.PictureURL, actualArtist.PictureURL);
                Assert.AreEqual(expectedArtist.AlbumCount, actualArtist.AlbumCount);
                Assert.AreEqual(expectedArtist.FanCount, actualArtist.FanCount);
                Assert.AreEqual(expectedArtist.SongListUrl, actualArtist.SongListUrl);
            }
        }

        [TestMethod]
        public async Task Throw_When_ArtistNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_ArtistNotFound));

            var context = new PlaylistGeneratorDbContext(options);
            var sut = new ArtistService(context, Utils.Mapper);

            //Act & Assert
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetArtistByIdAsync(Guid.NewGuid()));

        }

        [TestMethod]
        public async Task ReturnArtist_When_ValidApiKey()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnArtist_When_ValidApiKey));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                (await arrangeContext.Users.FirstAsync()).ApiKey = Guid.NewGuid();

                await arrangeContext.Artists.AddAsync(new Artist
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    ArtistPageURL = "ArtistPageUrl",
                    PictureURL = "PictureUrl",
                    AlbumCount = 3,
                    FanCount = 223,
                    SongListUrl = "SongListUrl"
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedArtist = await actContext.Artists.FirstAsync();
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new ArtistService(actContext, Utils.Mapper);

                //Act
                var actualArtist = await sut.GetArtistByIdAsync(expectedArtist.Id, true, apiKey);

                //Assert
                Assert.AreEqual(expectedArtist.Id, actualArtist.Id);
                Assert.AreEqual(expectedArtist.Name, actualArtist.Name);
                Assert.AreEqual(expectedArtist.ArtistPageURL, actualArtist.ArtistPageURL);
                Assert.AreEqual(expectedArtist.PictureURL, actualArtist.PictureURL);
                Assert.AreEqual(expectedArtist.AlbumCount, actualArtist.AlbumCount);
                Assert.AreEqual(expectedArtist.FanCount, actualArtist.FanCount);
                Assert.AreEqual(expectedArtist.SongListUrl, actualArtist.SongListUrl);
            }
        }

        [TestMethod]
        public async Task Throw_When_InvalidApiKey()
        {
            var options = Utils.GetOptions(nameof(Throw_When_InvalidApiKey));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("artistName", 123));

                await arrangeContext.Artists.AddAsync(new Artist
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    ArtistPageURL = "ArtistPageUrl",
                    PictureURL = "PictureUrl",
                    AlbumCount = 3,
                    FanCount = 223,
                    SongListUrl = "SongListUrl"
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedArtist = await actContext.Artists.FirstAsync();
                var sut = new ArtistService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetArtistByIdAsync(expectedArtist.Id, true, Guid.NewGuid()));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsBanned()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsBanned));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("artistName", 123));

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsBanned = true;

                await arrangeContext.Artists.AddAsync(new Artist
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    ArtistPageURL = "ArtistPageUrl",
                    PictureURL = "PictureUrl",
                    AlbumCount = 3,
                    FanCount = 223,
                    SongListUrl = "SongListUrl"
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedArtist = await actContext.Artists.FirstAsync();
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new ArtistService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetArtistByIdAsync(expectedArtist.Id, true, apiKey));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsDeleted));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("artistName", 123));

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsDeleted = true;

                await arrangeContext.Artists.AddAsync(new Artist
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    ArtistPageURL = "ArtistPageUrl",
                    PictureURL = "PictureUrl",
                    AlbumCount = 3,
                    FanCount = 223,
                    SongListUrl = "SongListUrl"
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedArtist = await actContext.Artists.FirstAsync();
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new ArtistService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetArtistByIdAsync(expectedArtist.Id, true, apiKey));
            }
        }
    }
}
