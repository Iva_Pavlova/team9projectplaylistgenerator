﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.GenreServiceTests
{
    [TestClass]
    public class UpdateGenresInDb_Should
    {
        [TestMethod]
        public async Task Throw_When_NoGenresInDb()
        {
            var options = Utils.GetOptions(nameof(Throw_When_NoGenresInDb));

            var context = new PlaylistGeneratorDbContext(options);
            var sut = new GenreService(context, Utils.Mapper);

            //Act & Assert
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateGenresInDbAsync());
        }

        [TestMethod]
        public async Task Throw_When_ApiKeyInvalid()
        {
            var options = Utils.GetOptions(nameof(Throw_When_ApiKeyInvalid));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    PictureURL = "PictureUrl2",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name3",
                    DeezerId = "DeezerId3",
                    PictureURL = "PictureUrl3",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name4",
                    DeezerId = "DeezerId4",
                    PictureURL = "PictureUrl4",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateGenresInDbAsync(true, Guid.NewGuid()));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsBanned()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsBanned));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    PictureURL = "PictureUrl2",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name3",
                    DeezerId = "DeezerId3",
                    PictureURL = "PictureUrl3",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name4",
                    DeezerId = "DeezerId4",
                    PictureURL = "PictureUrl4",
                });

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();

                user.ApiKey = Guid.NewGuid();
                user.IsBanned = true;

                await arrangeContext.SaveChangesAsync();

            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateGenresInDbAsync(true, key));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsDeleted));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    PictureURL = "PictureUrl2",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name3",
                    DeezerId = "DeezerId3",
                    PictureURL = "PictureUrl3",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name4",
                    DeezerId = "DeezerId4",
                    PictureURL = "PictureUrl4",
                });

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();

                user.ApiKey = Guid.NewGuid();
                user.IsDeleted = true;

                await arrangeContext.SaveChangesAsync();

            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateGenresInDbAsync(true, key));
            }
        }
    }
}
