﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.GenreServiceTests
{
    [TestClass]
    public class GetGenreByIdAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectGenre()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectGenre));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    PictureURL = "PictureUrl",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedGenre = await actContext.Genres.FirstAsync();
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act
                var actualGenre = await sut.GetGenreByIdAsync(expectedGenre.Id);

                //Assert
                Assert.AreEqual(expectedGenre.Id, actualGenre.Id);
                Assert.AreEqual(expectedGenre.Name, actualGenre.Name);
                Assert.AreEqual(expectedGenre.PictureURL, actualGenre.PictureURL);
            }
        }

        [TestMethod]
        public async Task Throw_When_GenreNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_GenreNotFound));

            var context = new PlaylistGeneratorDbContext(options);
            var sut = new GenreService(context, Utils.Mapper);

            //Act & Assert
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetGenreByIdAsync(Guid.NewGuid()));

        }

        [TestMethod]
        public async Task ReturnGenre_When_ValidApiKey()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnGenre_When_ValidApiKey));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                (await arrangeContext.Users.FirstAsync()).ApiKey = Guid.NewGuid();

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    PictureURL = "PictureUrl",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedGenre = await actContext.Genres.FirstAsync();
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act
                var actualGenre = await sut.GetGenreByIdAsync(expectedGenre.Id, true, apiKey);

                //Assert
                Assert.AreEqual(expectedGenre.Id, actualGenre.Id);
                Assert.AreEqual(expectedGenre.Name, actualGenre.Name);
                Assert.AreEqual(expectedGenre.PictureURL, actualGenre.PictureURL);
            }
        }

        [TestMethod]
        public async Task Throw_When_InvalidApiKey()
        {
            var options = Utils.GetOptions(nameof(Throw_When_InvalidApiKey));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("artistName", 123));

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    PictureURL = "PictureUrl",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedGenre = await actContext.Genres.FirstAsync();
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetGenreByIdAsync(expectedGenre.Id, true, Guid.NewGuid()));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsBanned()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsBanned));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("artistName", 123));

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsBanned = true;

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    PictureURL = "PictureUrl",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedGenre = await actContext.Genres.FirstAsync();
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetGenreByIdAsync(expectedGenre.Id, true, apiKey));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsDeleted));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("artistName", 123));

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsDeleted = true;

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    PictureURL = "PictureUrl",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedGenre = await actContext.Genres.FirstAsync();
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetGenreByIdAsync(expectedGenre.Id, true, apiKey));
            }
        }
    }
}
