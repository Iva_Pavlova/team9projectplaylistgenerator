﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.GenreServiceTests
{
    [TestClass]
    public class GetAllGenresAsync_Should
    {
        [TestMethod]
        public async Task ReturnAllGenresCorrectly()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnAllGenresCorrectly));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    PictureURL = "PictureUrl2",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name3",
                    DeezerId = "DeezerId3",
                    PictureURL = "PictureUrl3",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name4",
                    DeezerId = "DeezerId4",
                    PictureURL = "PictureUrl4",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedGenres = await actContext.Genres.ToListAsync();
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act
                var actualGenres = (await sut.GetAllGenresAsync()).ToList();

                //Assert
                Assert.AreEqual(expectedGenres.Count, actualGenres.Count);

                for (int i = 0; i < actualGenres.Count; i++)
                {
                    Assert.AreEqual(expectedGenres[i].Id, actualGenres[i].Id);
                    Assert.AreEqual(expectedGenres[i].Name, actualGenres[i].Name);
                    Assert.AreEqual(expectedGenres[i].PictureURL, actualGenres[i].PictureURL);
                }
            }

        }

        [TestMethod]
        public async Task ReturnEmptyCollection_When_NoGenresInDb()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnEmptyCollection_When_NoGenresInDb));

            var context = new PlaylistGeneratorDbContext(options);
            var sut = new GenreService(context, Utils.Mapper);


            //Act
            var result = await sut.GetAllGenresAsync();

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count() == 0);
        }

        [TestMethod]
        public async Task ReturnGenres_When_ApiKeyValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnGenres_When_ApiKeyValid));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    PictureURL = "PictureUrl2",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name3",
                    DeezerId = "DeezerId3",
                    PictureURL = "PictureUrl3",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name4",
                    DeezerId = "DeezerId4",
                    PictureURL = "PictureUrl4",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedGenres = await actContext.Genres.ToListAsync();
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act
                var actualGenres = (await sut.GetAllGenresAsync()).ToList();

                //Assert
                Assert.AreEqual(expectedGenres.Count, actualGenres.Count);

                for (int i = 0; i < actualGenres.Count; i++)
                {
                    Assert.AreEqual(expectedGenres[i].Id, actualGenres[i].Id);
                    Assert.AreEqual(expectedGenres[i].Name, actualGenres[i].Name);
                    Assert.AreEqual(expectedGenres[i].PictureURL, actualGenres[i].PictureURL);
                }
            }

        }

        [TestMethod]
        public async Task ReturnGenre_When_ValidApiKey()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnGenre_When_ValidApiKey));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                (await arrangeContext.Users.FirstAsync()).ApiKey = Guid.NewGuid();

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    PictureURL = "PictureUrl2",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name3",
                    DeezerId = "DeezerId3",
                    PictureURL = "PictureUrl3",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name4",
                    DeezerId = "DeezerId4",
                    PictureURL = "PictureUrl4",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedGenres = await actContext.Genres.ToListAsync();
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act
                var actualGenres = (await sut.GetAllGenresAsync(true, apiKey)).ToList();

                //Assert
                Assert.AreEqual(expectedGenres.Count, actualGenres.Count);

                for (int i = 0; i < actualGenres.Count; i++)
                {
                    Assert.AreEqual(expectedGenres[i].Id, actualGenres[i].Id);
                    Assert.AreEqual(expectedGenres[i].Name, actualGenres[i].Name);
                    Assert.AreEqual(expectedGenres[i].PictureURL, actualGenres[i].PictureURL);
                }
            }
        }

        [TestMethod]
        public async Task Throw_When_InvalidApiKey()
        {
            var options = Utils.GetOptions(nameof(Throw_When_InvalidApiKey));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("artistName", 123));

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllGenresAsync(true, Guid.NewGuid()));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsBanned()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsBanned));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("artistName", 123));

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsBanned = true;

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    PictureURL = "PictureUrl2",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name3",
                    DeezerId = "DeezerId3",
                    PictureURL = "PictureUrl3",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name4",
                    DeezerId = "DeezerId4",
                    PictureURL = "PictureUrl4",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllGenresAsync(true, apiKey));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsDeleted));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("artistName", 123));

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsDeleted = true;

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    PictureURL = "PictureUrl2",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name3",
                    DeezerId = "DeezerId3",
                    PictureURL = "PictureUrl3",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name4",
                    DeezerId = "DeezerId4",
                    PictureURL = "PictureUrl4",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new GenreService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllGenresAsync(true, apiKey));
            }
        }

    }
}
