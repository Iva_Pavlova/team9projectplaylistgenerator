﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Models.Genre;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.GenreServiceTests
{
    [TestClass]
    public class GetGenresByPlaylistId
    {
        [TestMethod]
        public async Task ReturnCollectionGenres_When_PlaylistExistInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCollectionGenres_When_PlaylistExistInDb));
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Genre, GenreServiceModel>().ReverseMap(); });
            var mapper = new Mapper(config);

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            Genre genre1 = Utils.CreateMockGenre("genre1Name", 123);
            Genre genre2 = Utils.CreateMockGenre("genre2Name", 124);
            Artist artist1 = Utils.CreateMockArtist("artist1Name", 234, 1, 0);
            Album album1 = Utils.CreateMockAlbum("album1Name", 345, artist1.Id);

            Song song1 = Utils.CreateMockSong("song1Name", 456, album1.Id, artist1.Id, genre1.Id, 180, 1);
            Song song2 = Utils.CreateMockSong("song1Name", 567, album1.Id, artist1.Id, genre1.Id, 240, 2);
            Song song3 = Utils.CreateMockSong("song1Name", 678, album1.Id, artist1.Id, genre1.Id, 180, 3);
            Song song4 = Utils.CreateMockSong("song1Name", 678, album1.Id, artist1.Id, genre2.Id, 300, 4);
            Song song5 = Utils.CreateMockSong("song1Name", 678, album1.Id, artist1.Id, genre2.Id, 400, 5);
            Song song6 = Utils.CreateMockSong("song1Name", 678, album1.Id, artist1.Id, genre2.Id, 500, 6);
            Playlist playlist1 = Utils.CreateMockPlaylist("playlist1Name", "playlist1Description", user1.Id, 600, false, false);
            PlaylistGenre playlist1Genre1 = Utils.CreateMockPlaylistGenre(playlist1.Id, genre1.Id);
            PlaylistGenre playlist1Genre2 = Utils.CreateMockPlaylistGenre(playlist1.Id, genre2.Id);
            PlaylistSong playlist1Song1 = Utils.CreateMockPlaylistSong(playlist1.Id, song1.Id);
            PlaylistSong playlist1Song2 = Utils.CreateMockPlaylistSong(playlist1.Id, song2.Id);
            PlaylistSong playlist1Song3 = Utils.CreateMockPlaylistSong(playlist1.Id, song3.Id);
            PlaylistSong playlist1Song4 = Utils.CreateMockPlaylistSong(playlist1.Id, song4.Id);
            PlaylistSong playlist1Song5 = Utils.CreateMockPlaylistSong(playlist1.Id, song5.Id);
            PlaylistSong playlist1Song6 = Utils.CreateMockPlaylistSong(playlist1.Id, song6.Id);

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Genres.Add(genre1);
                arrangeContext.Genres.Add(genre2);
                arrangeContext.Artists.Add(artist1);
                arrangeContext.Albums.Add(album1);
                arrangeContext.Songs.Add(song1);
                arrangeContext.Songs.Add(song2);
                arrangeContext.Songs.Add(song3);
                arrangeContext.Songs.Add(song4);
                arrangeContext.Songs.Add(song5);
                arrangeContext.Songs.Add(song6);
                arrangeContext.Playlists.Add(playlist1);
                arrangeContext.PlaylistsGenres.Add(playlist1Genre1);
                arrangeContext.PlaylistsGenres.Add(playlist1Genre2);
                arrangeContext.PlaylistsSongs.Add(playlist1Song1);
                arrangeContext.PlaylistsSongs.Add(playlist1Song2);
                arrangeContext.PlaylistsSongs.Add(playlist1Song3);
                arrangeContext.PlaylistsSongs.Add(playlist1Song4);
                arrangeContext.PlaylistsSongs.Add(playlist1Song5);
                arrangeContext.PlaylistsSongs.Add(playlist1Song6);
                arrangeContext.SaveChanges();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                GenreService sut = new GenreService(actContext, mapper);
                var result = await sut.GetGenresByPlaylistId(playlist1.Id);

                // Assert
                Assert.AreEqual(2, result.Count());
            }
        }

        public async Task ReturnEmptyCollectionGenres_When_NoGenresInPlaylist()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCollectionGenres_When_PlaylistExistInDb));
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Genre, GenreServiceModel>().ReverseMap(); });
            var mapper = new Mapper(config);

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            Genre genre1 = Utils.CreateMockGenre("genre1Name", 123);
            Genre genre2 = Utils.CreateMockGenre("genre2Name", 124);
            Artist artist1 = Utils.CreateMockArtist("artist1Name", 234, 1, 0);
            Album album1 = Utils.CreateMockAlbum("album1Name", 345, artist1.Id);
            Playlist playlist1 = Utils.CreateMockPlaylist("playlist1Name", "playlist1Description", user1.Id, 600, false, false);

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Genres.Add(genre1);
                arrangeContext.Genres.Add(genre2);
                arrangeContext.Artists.Add(artist1);
                arrangeContext.Albums.Add(album1);
                arrangeContext.Playlists.Add(playlist1);
                arrangeContext.SaveChanges();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                GenreService sut = new GenreService(actContext, mapper);
                var result = await sut.GetGenresByPlaylistId(playlist1.Id);

                // Assert
                Assert.AreEqual(0, result.Count());
            }
        }

        [TestMethod]
        public async Task ThrowException_When_NoSuchPlaylistInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_NoSuchPlaylistInDb));
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Genre, GenreServiceModel>().ReverseMap(); });
            var mapper = new Mapper(config);

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                GenreService sut = new GenreService(actContext, mapper);

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetGenresByPlaylistId(Guid.NewGuid()));
            }
        }
    }
}
