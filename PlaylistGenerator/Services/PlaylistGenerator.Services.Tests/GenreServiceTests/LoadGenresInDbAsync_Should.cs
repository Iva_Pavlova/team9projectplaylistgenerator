﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.GenreServiceTests
{
    [TestClass]
    public class LoadGenresInDbAsync_Should
    {
        [TestMethod]
        public async Task NotLoad_When_DbHasGenres()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(NotLoad_When_DbHasGenres));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    PictureURL = "PictureUrl1",
                });

                await arrangeContext.Genres.AddAsync(new Genre
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    PictureURL = "PictureUrl2",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var sut = new GenreService(actContext, Utils.Mapper);
                int expextedGenreCount = 2;

                //Act 
                await sut.LoadGenresInDbAsync();

                //Assert
                Assert.AreEqual(expextedGenreCount, actContext.Genres.Count());
            }

        }
    }
}
