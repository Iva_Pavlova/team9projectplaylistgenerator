﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Models;
using PlaylistGenerator.Services.Providers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.PlaylistServiceTests
{
    [TestClass]
    public class AddPlaylistAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectPlaylistServiceModel_When_PlaylistAddedSuccessfully()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectPlaylistServiceModel_When_PlaylistAddedSuccessfully));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.SaveChanges();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                PlaylistServiceModel playlistToAdd = new PlaylistServiceModel
                {
                    Name = "Playlist 1 Name",
                    Description = "Playlist 1 Description",
                    UserId = user1.Id,
                    UserName = user1.FirstName + " " + user1.LastName,
                    UserImage = user1.Image,
                    Image = "/images/playlists/default.jpg",
                };
                var result = await sut.CreatePlaylistAsync(playlistToAdd);

                // Assert
                Assert.AreEqual(playlistToAdd.Id, result.Id);
                Assert.AreEqual(playlistToAdd.Name, result.Name);
                Assert.AreEqual(playlistToAdd.Description, result.Description);
                Assert.AreEqual(playlistToAdd.Image, result.Image);
                Assert.AreEqual(playlistToAdd.UserId, result.UserId);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_NameIsNullOrEmpty()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_NameIsNullOrEmpty));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.SaveChanges();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                PlaylistServiceModel playlistToAdd = new PlaylistServiceModel
                {
                    Name = "",
                    Description = "Playlist 1 Description",
                    UserId = user1.Id,
                    UserName = user1.FirstName + " " + user1.LastName,
                    UserImage = user1.Image,
                    Image = "/images/playlists/default.jpg",
                };

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreatePlaylistAsync(playlistToAdd));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserDoesNotExistInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_NameIsNullOrEmpty));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                PlaylistServiceModel playlistToAdd = new PlaylistServiceModel
                {
                    Name = "Playlist 1 Name",
                    Description = "Playlist 1 Description",
                    UserId = Guid.NewGuid(),
                    Image = "/images/playlists/default.jpg",
                };

                // Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.CreatePlaylistAsync(playlistToAdd));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserIsDeleted()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_NameIsNullOrEmpty));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.SaveChanges();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                actContext.Users.First(x => x.Id == user1.Id).IsDeleted = true;

                PlaylistServiceModel playlistToAdd = new PlaylistServiceModel
                {
                    Name = "Playlist 1 Name",
                    Description = "Playlist 1 Description",
                    UserId = user1.Id,
                    Image = "/images/playlists/default.jpg",
                };

                // Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.CreatePlaylistAsync(playlistToAdd));
            }
        }
    }
}
