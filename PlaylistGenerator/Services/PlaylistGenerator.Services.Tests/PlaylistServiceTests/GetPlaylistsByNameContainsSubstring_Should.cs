﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Models;
using PlaylistGenerator.Services.Providers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.PlaylistServiceTests
{
    [TestClass]
    public class GetPlaylistsByNameContainsSubstring_Should
    {
        [TestMethod]
        public async Task ReturnCorrectPlaylistsServiceModels_When_PlaylistWithNameThatContainsSubstringExists()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectPlaylistsServiceModels_When_PlaylistWithNameThatContainsSubstringExists));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            Genre genre1 = Utils.CreateMockGenre("genre1Name", 123);
            Artist artist1 = Utils.CreateMockArtist("artist1Name", 234, 1, 0);
            Album album1 = Utils.CreateMockAlbum("album1Name", 345, artist1.Id);
            Song song1 = Utils.CreateMockSong("song1Name", 456, album1.Id, artist1.Id, genre1.Id, 180, 1);
            Song song2 = Utils.CreateMockSong("song1Name", 567, album1.Id, artist1.Id, genre1.Id, 240, 2);
            Song song3 = Utils.CreateMockSong("song1Name", 678, album1.Id, artist1.Id, genre1.Id, 180, 3);

            Playlist playlist1 = Utils.CreateMockPlaylist("playlist1Name", "playlist1Description", user1.Id, 600, false, false);
            PlaylistGenre playlist1Genre1 = Utils.CreateMockPlaylistGenre(playlist1.Id, genre1.Id);
            PlaylistSong playlist1Song1 = Utils.CreateMockPlaylistSong(playlist1.Id, song1.Id);
            PlaylistSong playlist1Song2 = Utils.CreateMockPlaylistSong(playlist1.Id, song2.Id);
            PlaylistSong playlist1Song3 = Utils.CreateMockPlaylistSong(playlist1.Id, song3.Id);

            Playlist playlist2 = Utils.CreateMockPlaylist("name1Playlist", "playlist1Description", user1.Id, 600, false, false);
            PlaylistGenre playlist2Genre1 = Utils.CreateMockPlaylistGenre(playlist2.Id, genre1.Id);
            PlaylistSong playlist2Song1 = Utils.CreateMockPlaylistSong(playlist2.Id, song1.Id);
            PlaylistSong playlist2Song2 = Utils.CreateMockPlaylistSong(playlist2.Id, song2.Id);
            PlaylistSong playlist2Song3 = Utils.CreateMockPlaylistSong(playlist2.Id, song3.Id);

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Genres.Add(genre1);
                arrangeContext.Artists.Add(artist1);
                arrangeContext.Albums.Add(album1);
                arrangeContext.Songs.Add(song1);
                arrangeContext.Songs.Add(song2);
                arrangeContext.Songs.Add(song3);
                arrangeContext.Playlists.Add(playlist1);
                arrangeContext.PlaylistsGenres.Add(playlist1Genre1);
                arrangeContext.PlaylistsSongs.Add(playlist1Song1);
                arrangeContext.PlaylistsSongs.Add(playlist1Song2);
                arrangeContext.PlaylistsSongs.Add(playlist1Song3);
                arrangeContext.Playlists.Add(playlist2);
                arrangeContext.PlaylistsGenres.Add(playlist2Genre1);
                arrangeContext.PlaylistsSongs.Add(playlist2Song1);
                arrangeContext.PlaylistsSongs.Add(playlist2Song2);
                arrangeContext.PlaylistsSongs.Add(playlist2Song3);
                arrangeContext.SaveChanges();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                IEnumerable<PlaylistServiceModel> result = await sut.GetPlaylistsByNameContainsSubstring("list1");
                var count = result.Count();

                // Assert
                Assert.AreEqual(count, 1);
                Assert.AreEqual(result.First().Name, playlist1.Name);
            }
        }

        [TestMethod]
        public async Task ReturnNull_When_NoPlaylistWithNameThatContainsSubstringExists()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnNull_When_NoPlaylistWithNameThatContainsSubstringExists));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                IEnumerable<PlaylistServiceModel> result = await sut.GetPlaylistsByNameContainsSubstring("list1");

                // Assert
                Assert.AreEqual(null, result);
            }
        }
    }
}
