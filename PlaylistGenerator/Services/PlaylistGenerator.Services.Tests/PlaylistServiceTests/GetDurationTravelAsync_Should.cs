﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Services.Providers;
using System;

namespace PlaylistGenerator.Services.Tests.PlaylistServiceTests
{
    [TestClass]
    public class GetDurationTravelAsync_Should
    {
        [TestMethod]
        public void ReturnCorrectDurationTravel_When_PointAPointBStringsAreCorrect()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectDurationTravel_When_PointAPointBStringsAreCorrect));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                int result = sut.GetDurationTravelAsync("Bulgaria Burgas", "Bulgaria Sofia").GetAwaiter().GetResult();

                // Assert
                // Takes a while...
                Assert.AreEqual(12000, result);
            }
        }

        [TestMethod]
        public void ThrowException_When_DurationTravelCalculatedIsLessThan10Minutes()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_DurationTravelCalculatedIsLessThan10Minutes));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());

                // Assert
                // Takes a while...
                Assert.ThrowsException<ArgumentException>(() => sut.GetDurationTravelAsync("Bulgaria Burgas", "Bulgaria Burgas").GetAwaiter().GetResult());
            }
        }

        [TestMethod]
        public void ThrowException_When_PointAOrPointBStringsAreNull()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_PointAOrPointBStringsAreNull));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());

                // Assert
                // Takes a while...
                Assert.ThrowsException<ArgumentNullException>(() => sut.GetDurationTravelAsync(null, null).GetAwaiter().GetResult());
            }
        }

        [TestMethod]
        public void ThrowException_When_PointAOrPointBStringsAreEmpty()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_PointAOrPointBStringsAreEmpty));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());

                // Assert
                // Takes a while...
                Assert.ThrowsException<ArgumentNullException>(() => sut.GetDurationTravelAsync("", "").GetAwaiter().GetResult());
            }
        }
    }
}
