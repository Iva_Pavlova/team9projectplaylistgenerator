﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Providers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.PlaylistServiceTests
{
    [TestClass]
    public class GeneratePlaylistAsync_Should
    {
        [TestMethod]
        public async Task GeneratePlaylist_WhenParamsAreCorrect()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(GeneratePlaylist_WhenParamsAreCorrect));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            Genre genre1 = Utils.CreateMockGenre("genre1Name", 123);
            Genre genre2 = Utils.CreateMockGenre("genre2Name", 124);
            Genre genre3 = Utils.CreateMockGenre("genre3Name", 125);
            Artist artist1 = Utils.CreateMockArtist("artist1Name", 234, 1, 0);
            Artist artist2 = Utils.CreateMockArtist("artist2Name", 234, 1, 0);
            Artist artist3 = Utils.CreateMockArtist("artist3Name", 234, 1, 0);
            Album album1 = Utils.CreateMockAlbum("album1Name", 345, artist1.Id);

            Song song1 = Utils.CreateMockSong("song1Name", 101, album1.Id, artist1.Id, genre1.Id, 180, 1);
            Song song2 = Utils.CreateMockSong("song2Name", 102, album1.Id, artist1.Id, genre1.Id, 240, 2);
            Song song3 = Utils.CreateMockSong("song3Name", 103, album1.Id, artist1.Id, genre1.Id, 300, 3);
            Song song4 = Utils.CreateMockSong("song4Name", 104, album1.Id, artist1.Id, genre1.Id, 360, 4);
            Song song5 = Utils.CreateMockSong("song5Name", 105, album1.Id, artist1.Id, genre1.Id, 240, 5);
            Song song6 = Utils.CreateMockSong("song6Name", 106, album1.Id, artist1.Id, genre1.Id, 360, 6);
            Song song7 = Utils.CreateMockSong("song7Name", 107, album1.Id, artist1.Id, genre1.Id, 420, 7);
            Song song8 = Utils.CreateMockSong("song8Name", 108, album1.Id, artist1.Id, genre1.Id, 300, 8);
            Song song9 = Utils.CreateMockSong("song9Name", 109, album1.Id, artist1.Id, genre1.Id, 270, 9);
            Song song10 = Utils.CreateMockSong("song10Name", 110, album1.Id, artist2.Id, genre1.Id, 180, 10);
            Song song11 = Utils.CreateMockSong("song11Name", 201, album1.Id, artist2.Id, genre1.Id, 180, 11);
            Song song12 = Utils.CreateMockSong("song12Name", 202, album1.Id, artist2.Id, genre1.Id, 240, 12);
            Song song13 = Utils.CreateMockSong("song13Name", 203, album1.Id, artist2.Id, genre1.Id, 300, 13);
            Song song14 = Utils.CreateMockSong("song14Name", 204, album1.Id, artist2.Id, genre1.Id, 360, 14);
            Song song15 = Utils.CreateMockSong("song15Name", 205, album1.Id, artist2.Id, genre1.Id, 240, 15);
            Song song16 = Utils.CreateMockSong("song16Name", 206, album1.Id, artist2.Id, genre1.Id, 360, 16);
            Song song17 = Utils.CreateMockSong("song17Name", 207, album1.Id, artist2.Id, genre1.Id, 420, 17);
            Song song18 = Utils.CreateMockSong("song18Name", 208, album1.Id, artist2.Id, genre1.Id, 300, 18);
            Song song19 = Utils.CreateMockSong("song19Name", 209, album1.Id, artist2.Id, genre1.Id, 270, 19);
            Song song20 = Utils.CreateMockSong("song20Name", 210, album1.Id, artist2.Id, genre1.Id, 180, 20);
            Song song21 = Utils.CreateMockSong("song21Name", 301, album1.Id, artist2.Id, genre2.Id, 180, 21);
            Song song22 = Utils.CreateMockSong("song22Name", 302, album1.Id, artist1.Id, genre2.Id, 240, 22);
            Song song23 = Utils.CreateMockSong("song23Name", 303, album1.Id, artist1.Id, genre2.Id, 300, 23);
            Song song24 = Utils.CreateMockSong("song24Name", 304, album1.Id, artist1.Id, genre2.Id, 360, 24);
            Song song25 = Utils.CreateMockSong("song25Name", 305, album1.Id, artist1.Id, genre2.Id, 240, 25);
            Song song26 = Utils.CreateMockSong("song26Name", 306, album1.Id, artist1.Id, genre2.Id, 360, 26);
            Song song27 = Utils.CreateMockSong("song27Name", 307, album1.Id, artist1.Id, genre2.Id, 420, 27);
            Song song28 = Utils.CreateMockSong("song28Name", 308, album1.Id, artist1.Id, genre2.Id, 600, 28);
            Song song29 = Utils.CreateMockSong("song29Name", 309, album1.Id, artist1.Id, genre2.Id, 90, 29);
            Song song30 = Utils.CreateMockSong("song30Name", 310, album1.Id, artist3.Id, genre3.Id, 60, 30);
            Song song31 = Utils.CreateMockSong("song31Name", 401, album1.Id, artist3.Id, genre3.Id, 180, 31);
            Song song32 = Utils.CreateMockSong("song32Name", 402, album1.Id, artist3.Id, genre3.Id, 240, 32);
            Song song33 = Utils.CreateMockSong("song33Name", 403, album1.Id, artist3.Id, genre3.Id, 300, 33);
            Song song34 = Utils.CreateMockSong("song34Name", 404, album1.Id, artist3.Id, genre3.Id, 360, 34);
            Song song35 = Utils.CreateMockSong("song35Name", 405, album1.Id, artist3.Id, genre3.Id, 240, 35);
            Song song36 = Utils.CreateMockSong("song36Name", 406, album1.Id, artist3.Id, genre3.Id, 360, 36);
            Song song37 = Utils.CreateMockSong("song37Name", 407, album1.Id, artist3.Id, genre3.Id, 420, 37);
            Song song38 = Utils.CreateMockSong("song38Name", 408, album1.Id, artist3.Id, genre3.Id, 600, 38);
            Song song39 = Utils.CreateMockSong("song39Name", 409, album1.Id, artist3.Id, genre3.Id, 90, 39);
            Song song40 = Utils.CreateMockSong("song40Name", 410, album1.Id, artist3.Id, genre3.Id, 60, 40);
            Song song41 = Utils.CreateMockSong("song41Name", 501, album1.Id, artist3.Id, genre3.Id, 180, 41);
            Song song42 = Utils.CreateMockSong("song42Name", 502, album1.Id, artist3.Id, genre3.Id, 240, 42);
            Song song43 = Utils.CreateMockSong("song43Name", 503, album1.Id, artist3.Id, genre3.Id, 300, 43);
            Song song44 = Utils.CreateMockSong("song44Name", 504, album1.Id, artist1.Id, genre3.Id, 360, 44);
            Song song45 = Utils.CreateMockSong("song45Name", 505, album1.Id, artist1.Id, genre3.Id, 240, 45);
            Song song46 = Utils.CreateMockSong("song46Name", 506, album1.Id, artist1.Id, genre3.Id, 360, 46);
            Song song47 = Utils.CreateMockSong("song47Name", 507, album1.Id, artist1.Id, genre3.Id, 420, 47);
            Song song48 = Utils.CreateMockSong("song48Name", 508, album1.Id, artist1.Id, genre3.Id, 600, 48);
            Song song49 = Utils.CreateMockSong("song49Name", 509, album1.Id, artist1.Id, genre3.Id, 90, 49);
            Song song50 = Utils.CreateMockSong("song50Name", 510, album1.Id, artist1.Id, genre3.Id, 60, 50);

            Playlist playlist1 = Utils.CreateMockPlaylist("playlist1Name", "playlist1Description", user1.Id, 600, false, false);

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Genres.Add(genre1);
                arrangeContext.Genres.Add(genre2);
                arrangeContext.Artists.Add(artist1);
                arrangeContext.Albums.Add(album1);
                arrangeContext.Songs.Add(song1);
                arrangeContext.Songs.Add(song2);
                arrangeContext.Songs.Add(song3);
                arrangeContext.Songs.Add(song4);
                arrangeContext.Songs.Add(song5);
                arrangeContext.Songs.Add(song6);
                arrangeContext.Songs.Add(song7);
                arrangeContext.Songs.Add(song8);
                arrangeContext.Songs.Add(song9);
                arrangeContext.Songs.Add(song10);
                arrangeContext.Songs.Add(song11);
                arrangeContext.Songs.Add(song12);
                arrangeContext.Songs.Add(song13);
                arrangeContext.Songs.Add(song14);
                arrangeContext.Songs.Add(song15);
                arrangeContext.Songs.Add(song16);
                arrangeContext.Songs.Add(song17);
                arrangeContext.Songs.Add(song18);
                arrangeContext.Songs.Add(song19);
                arrangeContext.Songs.Add(song20);
                arrangeContext.Songs.Add(song21);
                arrangeContext.Songs.Add(song22);
                arrangeContext.Songs.Add(song23);
                arrangeContext.Songs.Add(song24);
                arrangeContext.Songs.Add(song25);
                arrangeContext.Songs.Add(song26);
                arrangeContext.Songs.Add(song27);
                arrangeContext.Songs.Add(song28);
                arrangeContext.Songs.Add(song29);
                arrangeContext.Songs.Add(song30);
                arrangeContext.Songs.Add(song31);
                arrangeContext.Songs.Add(song32);
                arrangeContext.Songs.Add(song33);
                arrangeContext.Songs.Add(song34);
                arrangeContext.Songs.Add(song35);
                arrangeContext.Songs.Add(song36);
                arrangeContext.Songs.Add(song37);
                arrangeContext.Songs.Add(song38);
                arrangeContext.Songs.Add(song39);
                arrangeContext.Songs.Add(song40);
                arrangeContext.Songs.Add(song41);
                arrangeContext.Songs.Add(song42);
                arrangeContext.Songs.Add(song43);
                arrangeContext.Songs.Add(song44);
                arrangeContext.Songs.Add(song45);
                arrangeContext.Songs.Add(song46);
                arrangeContext.Songs.Add(song47);
                arrangeContext.Songs.Add(song48);
                arrangeContext.Songs.Add(song49);
                arrangeContext.Songs.Add(song50);
                arrangeContext.Playlists.Add(playlist1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                bool result = await sut.GeneratePlaylistAsync(playlist1.Id, 605, new List<Guid> { genre1.Id, genre2.Id, genre3.Id }, false, true);

                // Assert
                Assert.AreEqual(true, result);
            }
        }

        [TestMethod]
        public async Task GeneratePlaylistOfAllGenres_WhenNoGenresAreSelectedByUser()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(GeneratePlaylistOfAllGenres_WhenNoGenresAreSelectedByUser));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            Genre genre1 = Utils.CreateMockGenre("genre1Name", 123);
            Genre genre2 = Utils.CreateMockGenre("genre2Name", 124);
            Genre genre3 = Utils.CreateMockGenre("genre3Name", 125);
            Artist artist1 = Utils.CreateMockArtist("artist1Name", 234, 1, 0);
            Artist artist2 = Utils.CreateMockArtist("artist2Name", 234, 1, 0);
            Artist artist3 = Utils.CreateMockArtist("artist3Name", 234, 1, 0);
            Album album1 = Utils.CreateMockAlbum("album1Name", 345, artist1.Id);

            Song song1 = Utils.CreateMockSong("song1Name", 101, album1.Id, artist1.Id, genre1.Id, 180, 1);
            Song song2 = Utils.CreateMockSong("song2Name", 102, album1.Id, artist1.Id, genre1.Id, 240, 2);
            Song song3 = Utils.CreateMockSong("song3Name", 103, album1.Id, artist1.Id, genre1.Id, 300, 3);
            Song song4 = Utils.CreateMockSong("song4Name", 104, album1.Id, artist1.Id, genre1.Id, 360, 4);
            Song song5 = Utils.CreateMockSong("song5Name", 105, album1.Id, artist1.Id, genre1.Id, 240, 5);
            Song song6 = Utils.CreateMockSong("song6Name", 106, album1.Id, artist1.Id, genre1.Id, 360, 6);
            Song song7 = Utils.CreateMockSong("song7Name", 107, album1.Id, artist1.Id, genre1.Id, 420, 7);
            Song song8 = Utils.CreateMockSong("song8Name", 108, album1.Id, artist1.Id, genre1.Id, 300, 8);
            Song song9 = Utils.CreateMockSong("song9Name", 109, album1.Id, artist1.Id, genre1.Id, 270, 9);
            Song song10 = Utils.CreateMockSong("song10Name", 110, album1.Id, artist2.Id, genre1.Id, 180, 10);
            Song song11 = Utils.CreateMockSong("song11Name", 201, album1.Id, artist2.Id, genre1.Id, 180, 11);
            Song song12 = Utils.CreateMockSong("song12Name", 202, album1.Id, artist2.Id, genre1.Id, 240, 12);
            Song song13 = Utils.CreateMockSong("song13Name", 203, album1.Id, artist2.Id, genre1.Id, 300, 13);
            Song song14 = Utils.CreateMockSong("song14Name", 204, album1.Id, artist2.Id, genre1.Id, 360, 14);
            Song song15 = Utils.CreateMockSong("song15Name", 205, album1.Id, artist2.Id, genre1.Id, 240, 15);
            Song song16 = Utils.CreateMockSong("song16Name", 206, album1.Id, artist2.Id, genre1.Id, 360, 16);
            Song song17 = Utils.CreateMockSong("song17Name", 207, album1.Id, artist2.Id, genre1.Id, 420, 17);
            Song song18 = Utils.CreateMockSong("song18Name", 208, album1.Id, artist2.Id, genre1.Id, 300, 18);
            Song song19 = Utils.CreateMockSong("song19Name", 209, album1.Id, artist2.Id, genre1.Id, 270, 19);
            Song song20 = Utils.CreateMockSong("song20Name", 210, album1.Id, artist2.Id, genre1.Id, 180, 20);
            Song song21 = Utils.CreateMockSong("song21Name", 301, album1.Id, artist2.Id, genre2.Id, 180, 21);
            Song song22 = Utils.CreateMockSong("song22Name", 302, album1.Id, artist1.Id, genre2.Id, 240, 22);
            Song song23 = Utils.CreateMockSong("song23Name", 303, album1.Id, artist1.Id, genre2.Id, 300, 23);
            Song song24 = Utils.CreateMockSong("song24Name", 304, album1.Id, artist1.Id, genre2.Id, 360, 24);
            Song song25 = Utils.CreateMockSong("song25Name", 305, album1.Id, artist1.Id, genre2.Id, 240, 25);
            Song song26 = Utils.CreateMockSong("song26Name", 306, album1.Id, artist1.Id, genre2.Id, 360, 26);
            Song song27 = Utils.CreateMockSong("song27Name", 307, album1.Id, artist1.Id, genre2.Id, 420, 27);
            Song song28 = Utils.CreateMockSong("song28Name", 308, album1.Id, artist1.Id, genre2.Id, 600, 28);
            Song song29 = Utils.CreateMockSong("song29Name", 309, album1.Id, artist1.Id, genre2.Id, 90, 29);
            Song song30 = Utils.CreateMockSong("song30Name", 310, album1.Id, artist3.Id, genre3.Id, 60, 30);
            Song song31 = Utils.CreateMockSong("song31Name", 401, album1.Id, artist3.Id, genre3.Id, 180, 31);
            Song song32 = Utils.CreateMockSong("song32Name", 402, album1.Id, artist3.Id, genre3.Id, 240, 32);
            Song song33 = Utils.CreateMockSong("song33Name", 403, album1.Id, artist3.Id, genre3.Id, 300, 33);
            Song song34 = Utils.CreateMockSong("song34Name", 404, album1.Id, artist3.Id, genre3.Id, 360, 34);
            Song song35 = Utils.CreateMockSong("song35Name", 405, album1.Id, artist3.Id, genre3.Id, 240, 35);
            Song song36 = Utils.CreateMockSong("song36Name", 406, album1.Id, artist3.Id, genre3.Id, 360, 36);
            Song song37 = Utils.CreateMockSong("song37Name", 407, album1.Id, artist3.Id, genre3.Id, 420, 37);
            Song song38 = Utils.CreateMockSong("song38Name", 408, album1.Id, artist3.Id, genre3.Id, 600, 38);
            Song song39 = Utils.CreateMockSong("song39Name", 409, album1.Id, artist3.Id, genre3.Id, 90, 39);
            Song song40 = Utils.CreateMockSong("song40Name", 410, album1.Id, artist3.Id, genre3.Id, 60, 40);
            Song song41 = Utils.CreateMockSong("song41Name", 501, album1.Id, artist3.Id, genre3.Id, 180, 41);
            Song song42 = Utils.CreateMockSong("song42Name", 502, album1.Id, artist3.Id, genre3.Id, 240, 42);
            Song song43 = Utils.CreateMockSong("song43Name", 503, album1.Id, artist3.Id, genre3.Id, 300, 43);
            Song song44 = Utils.CreateMockSong("song44Name", 504, album1.Id, artist1.Id, genre3.Id, 360, 44);
            Song song45 = Utils.CreateMockSong("song45Name", 505, album1.Id, artist1.Id, genre3.Id, 240, 45);
            Song song46 = Utils.CreateMockSong("song46Name", 506, album1.Id, artist1.Id, genre3.Id, 360, 46);
            Song song47 = Utils.CreateMockSong("song47Name", 507, album1.Id, artist1.Id, genre3.Id, 420, 47);
            Song song48 = Utils.CreateMockSong("song48Name", 508, album1.Id, artist1.Id, genre3.Id, 600, 48);
            Song song49 = Utils.CreateMockSong("song49Name", 509, album1.Id, artist1.Id, genre3.Id, 90, 49);
            Song song50 = Utils.CreateMockSong("song50Name", 510, album1.Id, artist1.Id, genre3.Id, 60, 50);

            Playlist playlist1 = Utils.CreateMockPlaylist("playlist1Name", "playlist1Description", user1.Id, 600, false, false);

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Genres.Add(genre1);
                arrangeContext.Genres.Add(genre2);
                arrangeContext.Artists.Add(artist1);
                arrangeContext.Albums.Add(album1);
                arrangeContext.Songs.Add(song1);
                arrangeContext.Songs.Add(song2);
                arrangeContext.Songs.Add(song3);
                arrangeContext.Songs.Add(song4);
                arrangeContext.Songs.Add(song5);
                arrangeContext.Songs.Add(song6);
                arrangeContext.Songs.Add(song7);
                arrangeContext.Songs.Add(song8);
                arrangeContext.Songs.Add(song9);
                arrangeContext.Songs.Add(song10);
                arrangeContext.Songs.Add(song11);
                arrangeContext.Songs.Add(song12);
                arrangeContext.Songs.Add(song13);
                arrangeContext.Songs.Add(song14);
                arrangeContext.Songs.Add(song15);
                arrangeContext.Songs.Add(song16);
                arrangeContext.Songs.Add(song17);
                arrangeContext.Songs.Add(song18);
                arrangeContext.Songs.Add(song19);
                arrangeContext.Songs.Add(song20);
                arrangeContext.Songs.Add(song21);
                arrangeContext.Songs.Add(song22);
                arrangeContext.Songs.Add(song23);
                arrangeContext.Songs.Add(song24);
                arrangeContext.Songs.Add(song25);
                arrangeContext.Songs.Add(song26);
                arrangeContext.Songs.Add(song27);
                arrangeContext.Songs.Add(song28);
                arrangeContext.Songs.Add(song29);
                arrangeContext.Songs.Add(song30);
                arrangeContext.Songs.Add(song31);
                arrangeContext.Songs.Add(song32);
                arrangeContext.Songs.Add(song33);
                arrangeContext.Songs.Add(song34);
                arrangeContext.Songs.Add(song35);
                arrangeContext.Songs.Add(song36);
                arrangeContext.Songs.Add(song37);
                arrangeContext.Songs.Add(song38);
                arrangeContext.Songs.Add(song39);
                arrangeContext.Songs.Add(song40);
                arrangeContext.Songs.Add(song41);
                arrangeContext.Songs.Add(song42);
                arrangeContext.Songs.Add(song43);
                arrangeContext.Songs.Add(song44);
                arrangeContext.Songs.Add(song45);
                arrangeContext.Songs.Add(song46);
                arrangeContext.Songs.Add(song47);
                arrangeContext.Songs.Add(song48);
                arrangeContext.Songs.Add(song49);
                arrangeContext.Songs.Add(song50);
                arrangeContext.Playlists.Add(playlist1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                bool result = await sut.GeneratePlaylistAsync(playlist1.Id, 605, new List<Guid>(), false, true);

                // Assert
                Assert.AreEqual(true, result);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_DurationTravelIsLessThan10Minutes()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_DurationTravelIsLessThan10Minutes));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            Genre genre1 = Utils.CreateMockGenre("genre1Name", 123);
            Artist artist1 = Utils.CreateMockArtist("artist1Name", 234, 1, 0);
            Album album1 = Utils.CreateMockAlbum("album1Name", 345, artist1.Id);
            Song song1 = Utils.CreateMockSong("song1Name", 101, album1.Id, artist1.Id, genre1.Id, 180, 1);
            Song song2 = Utils.CreateMockSong("song2Name", 102, album1.Id, artist1.Id, genre1.Id, 240, 2);

            Playlist playlist1 = Utils.CreateMockPlaylist("playlist1Name", "playlist1Description", user1.Id, 600, false, false);

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Genres.Add(genre1);
                arrangeContext.Artists.Add(artist1);
                arrangeContext.Albums.Add(album1);
                arrangeContext.Songs.Add(song1);
                arrangeContext.Songs.Add(song2);
                arrangeContext.Playlists.Add(playlist1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GeneratePlaylistAsync(playlist1.Id, 5, new List<Guid> { genre1.Id }, false, true));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_NoSuchPlaylistInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_DurationTravelIsLessThan10Minutes));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            Genre genre1 = Utils.CreateMockGenre("genre1Name", 123);

            Playlist playlist1 = Utils.CreateMockPlaylist("playlist1Name", "playlist1Description", user1.Id, 600, false, false);

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Genres.Add(genre1);
                arrangeContext.Playlists.Add(playlist1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GeneratePlaylistAsync(Guid.NewGuid(), 180, new List<Guid> { genre1.Id }, false, true));
            }
        }
    }
}
