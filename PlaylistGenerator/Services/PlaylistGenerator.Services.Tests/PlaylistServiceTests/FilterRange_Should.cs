﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Providers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.PlaylistServiceTests
{
    [TestClass]
    public class FilterRange_Should
    {
        [TestMethod]
        public async Task ReturnFilteredCollectionPlaylistServiceModelsByDuration_When_PlaylistsExistInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnFilteredCollectionPlaylistServiceModelsByDuration_When_PlaylistsExistInDb));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            Genre genre1 = Utils.CreateMockGenre("genre1Name", 123);
            Genre genre2 = Utils.CreateMockGenre("genre2Name", 124);
            Genre genre3 = Utils.CreateMockGenre("genre3Name", 124);
            Artist artist1 = Utils.CreateMockArtist("artist1Name", 234, 1, 0);
            Album album1 = Utils.CreateMockAlbum("album1Name", 345, artist1.Id);

            // 3 hours + 1 sec
            // Rank: 6 / 2 = 3|00000
            Song song1 = Utils.CreateMockSong("song1Name", 456, album1.Id, artist1.Id, genre1.Id, 1200, 100000);
            Song song2 = Utils.CreateMockSong("song2Name", 567, album1.Id, artist1.Id, genre1.Id, 1200, 200000);
            Song song3 = Utils.CreateMockSong("song3Name", 678, album1.Id, artist1.Id, genre1.Id, 1201, 300000);
            Playlist playlist1 = Utils.CreateMockPlaylist("playlist1Name", "playlist1Description", user1.Id, 3601, false, false);
            PlaylistGenre playlist1Genre1 = Utils.CreateMockPlaylistGenre(playlist1.Id, genre1.Id);
            PlaylistSong playlist1Song1 = Utils.CreateMockPlaylistSong(playlist1.Id, song1.Id);
            PlaylistSong playlist1Song2 = Utils.CreateMockPlaylistSong(playlist1.Id, song2.Id);
            PlaylistSong playlist1Song3 = Utils.CreateMockPlaylistSong(playlist1.Id, song3.Id);

            // Less than an hour
            // Rank: 7.5|00000
            Song song4 = Utils.CreateMockSong("song4Name", 679, album1.Id, artist1.Id, genre2.Id, 360, 400000);
            Song song5 = Utils.CreateMockSong("song5Name", 680, album1.Id, artist1.Id, genre2.Id, 240, 500000);
            Song song6 = Utils.CreateMockSong("song6Name", 681, album1.Id, artist1.Id, genre2.Id, 120, 600000);
            Playlist playlist2 = Utils.CreateMockPlaylist("playlist2Name", "playlist2Description", user1.Id, 720, false, false);
            PlaylistGenre playlist2Genre2 = Utils.CreateMockPlaylistGenre(playlist2.Id, genre2.Id);
            PlaylistSong playlist2Song4 = Utils.CreateMockPlaylistSong(playlist2.Id, song4.Id);
            PlaylistSong playlist2Song5 = Utils.CreateMockPlaylistSong(playlist2.Id, song5.Id);
            PlaylistSong playlist2Song6 = Utils.CreateMockPlaylistSong(playlist2.Id, song6.Id);

            // Less than an hour
            // Rank: 12|0000
            Song song7 = Utils.CreateMockSong("song7Name", 682, album1.Id, artist1.Id, genre3.Id, 720, 700000);
            Song song8 = Utils.CreateMockSong("song8Name", 683, album1.Id, artist1.Id, genre3.Id, 480, 800000);
            Song song9 = Utils.CreateMockSong("song9Name", 684, album1.Id, artist1.Id, genre3.Id, 240, 900000);
            Playlist playlist3 = Utils.CreateMockPlaylist("playlist2Name", "playlist2Description", user1.Id, 1440, false, false);
            PlaylistGenre playlist3Genre3 = Utils.CreateMockPlaylistGenre(playlist3.Id, genre3.Id);
            PlaylistSong playlist3Song7 = Utils.CreateMockPlaylistSong(playlist3.Id, song7.Id);
            PlaylistSong playlist3Song8 = Utils.CreateMockPlaylistSong(playlist3.Id, song8.Id);
            PlaylistSong playlist3Song9 = Utils.CreateMockPlaylistSong(playlist3.Id, song9.Id);

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Genres.Add(genre1);
                arrangeContext.Genres.Add(genre2);
                arrangeContext.Genres.Add(genre3);
                arrangeContext.Artists.Add(artist1);
                arrangeContext.Albums.Add(album1);
                arrangeContext.Songs.Add(song1);
                arrangeContext.Songs.Add(song2);
                arrangeContext.Songs.Add(song3);
                arrangeContext.Songs.Add(song4);
                arrangeContext.Songs.Add(song5);
                arrangeContext.Songs.Add(song6);
                arrangeContext.Songs.Add(song7);
                arrangeContext.Songs.Add(song8);
                arrangeContext.Songs.Add(song9);
                arrangeContext.Playlists.Add(playlist1);
                arrangeContext.Playlists.Add(playlist2);
                arrangeContext.Playlists.Add(playlist3);
                arrangeContext.PlaylistsGenres.Add(playlist1Genre1);
                arrangeContext.PlaylistsGenres.Add(playlist2Genre2);
                arrangeContext.PlaylistsGenres.Add(playlist3Genre3);
                arrangeContext.PlaylistsSongs.Add(playlist1Song1);
                arrangeContext.PlaylistsSongs.Add(playlist1Song2);
                arrangeContext.PlaylistsSongs.Add(playlist1Song3);
                arrangeContext.PlaylistsSongs.Add(playlist2Song4);
                arrangeContext.PlaylistsSongs.Add(playlist2Song5);
                arrangeContext.PlaylistsSongs.Add(playlist2Song6);
                arrangeContext.PlaylistsSongs.Add(playlist3Song7);
                arrangeContext.PlaylistsSongs.Add(playlist3Song8);
                arrangeContext.PlaylistsSongs.Add(playlist3Song9);
                arrangeContext.SaveChanges();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                var act = await sut.GetAllPlaylistsAsync();
                var result = sut.FilterByRange(act, "duration", 1 /*hours*/, 2 /*hours*/);

                // Assert
                Assert.AreEqual(1, result.Count());
            }
        }

        [TestMethod]
        public async Task ThrowsInvalidOperationException_When_PlaylistsExistInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowsInvalidOperationException_When_PlaylistsExistInDb));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            Genre genre1 = Utils.CreateMockGenre("genre1Name", 123);
            Genre genre2 = Utils.CreateMockGenre("genre2Name", 124);
            Genre genre3 = Utils.CreateMockGenre("genre3Name", 124);
            Artist artist1 = Utils.CreateMockArtist("artist1Name", 234, 1, 0);
            Album album1 = Utils.CreateMockAlbum("album1Name", 345, artist1.Id);

            // 3 hours + 1 sec
            // Rank: 6 / 2 = 3|00000
            Song song1 = Utils.CreateMockSong("song1Name", 456, album1.Id, artist1.Id, genre1.Id, 1200, 100000);
            Song song2 = Utils.CreateMockSong("song2Name", 567, album1.Id, artist1.Id, genre1.Id, 1200, 200000);
            Song song3 = Utils.CreateMockSong("song3Name", 678, album1.Id, artist1.Id, genre1.Id, 1201, 300000);
            Playlist playlist1 = Utils.CreateMockPlaylist("playlist1Name", "playlist1Description", user1.Id, 3601, false, false);
            PlaylistGenre playlist1Genre1 = Utils.CreateMockPlaylistGenre(playlist1.Id, genre1.Id);
            PlaylistSong playlist1Song1 = Utils.CreateMockPlaylistSong(playlist1.Id, song1.Id);
            PlaylistSong playlist1Song2 = Utils.CreateMockPlaylistSong(playlist1.Id, song2.Id);
            PlaylistSong playlist1Song3 = Utils.CreateMockPlaylistSong(playlist1.Id, song3.Id);

            // Less than an hour
            // Rank: 7.5|00000
            Song song4 = Utils.CreateMockSong("song4Name", 679, album1.Id, artist1.Id, genre2.Id, 360, 400000);
            Song song5 = Utils.CreateMockSong("song5Name", 680, album1.Id, artist1.Id, genre2.Id, 240, 500000);
            Song song6 = Utils.CreateMockSong("song6Name", 681, album1.Id, artist1.Id, genre2.Id, 120, 600000);
            Playlist playlist2 = Utils.CreateMockPlaylist("playlist2Name", "playlist2Description", user1.Id, 720, false, false);
            PlaylistGenre playlist2Genre2 = Utils.CreateMockPlaylistGenre(playlist2.Id, genre2.Id);
            PlaylistSong playlist2Song4 = Utils.CreateMockPlaylistSong(playlist2.Id, song4.Id);
            PlaylistSong playlist2Song5 = Utils.CreateMockPlaylistSong(playlist2.Id, song5.Id);
            PlaylistSong playlist2Song6 = Utils.CreateMockPlaylistSong(playlist2.Id, song6.Id);

            // Less than an hour
            // Rank: 12|0000
            Song song7 = Utils.CreateMockSong("song7Name", 682, album1.Id, artist1.Id, genre3.Id, 720, 700000);
            Song song8 = Utils.CreateMockSong("song8Name", 683, album1.Id, artist1.Id, genre3.Id, 480, 800000);
            Song song9 = Utils.CreateMockSong("song9Name", 684, album1.Id, artist1.Id, genre3.Id, 240, 900000);
            Playlist playlist3 = Utils.CreateMockPlaylist("playlist2Name", "playlist2Description", user1.Id, 1440, false, false);
            PlaylistGenre playlist3Genre3 = Utils.CreateMockPlaylistGenre(playlist3.Id, genre3.Id);
            PlaylistSong playlist3Song7 = Utils.CreateMockPlaylistSong(playlist3.Id, song7.Id);
            PlaylistSong playlist3Song8 = Utils.CreateMockPlaylistSong(playlist3.Id, song8.Id);
            PlaylistSong playlist3Song9 = Utils.CreateMockPlaylistSong(playlist3.Id, song9.Id);

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Genres.Add(genre1);
                arrangeContext.Genres.Add(genre2);
                arrangeContext.Genres.Add(genre3);
                arrangeContext.Artists.Add(artist1);
                arrangeContext.Albums.Add(album1);
                arrangeContext.Songs.Add(song1);
                arrangeContext.Songs.Add(song2);
                arrangeContext.Songs.Add(song3);
                arrangeContext.Songs.Add(song4);
                arrangeContext.Songs.Add(song5);
                arrangeContext.Songs.Add(song6);
                arrangeContext.Songs.Add(song7);
                arrangeContext.Songs.Add(song8);
                arrangeContext.Songs.Add(song9);
                arrangeContext.Playlists.Add(playlist1);
                arrangeContext.Playlists.Add(playlist2);
                arrangeContext.Playlists.Add(playlist3);
                arrangeContext.PlaylistsGenres.Add(playlist1Genre1);
                arrangeContext.PlaylistsGenres.Add(playlist2Genre2);
                arrangeContext.PlaylistsGenres.Add(playlist3Genre3);
                arrangeContext.PlaylistsSongs.Add(playlist1Song1);
                arrangeContext.PlaylistsSongs.Add(playlist1Song2);
                arrangeContext.PlaylistsSongs.Add(playlist1Song3);
                arrangeContext.PlaylistsSongs.Add(playlist2Song4);
                arrangeContext.PlaylistsSongs.Add(playlist2Song5);
                arrangeContext.PlaylistsSongs.Add(playlist2Song6);
                arrangeContext.PlaylistsSongs.Add(playlist3Song7);
                arrangeContext.PlaylistsSongs.Add(playlist3Song8);
                arrangeContext.PlaylistsSongs.Add(playlist3Song9);
                arrangeContext.SaveChanges();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                var act = await sut.GetAllPlaylistsAsync();

                // Assert
                Assert.ThrowsException<InvalidOperationException>(() => sut.FilterByRange(act, "wrong sort method", 1, 2));
            }
        }
    }
}
