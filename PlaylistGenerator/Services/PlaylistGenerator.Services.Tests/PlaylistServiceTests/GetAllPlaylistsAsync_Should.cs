﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Providers;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.PlaylistServiceTests
{
    [TestClass]
    public class GetAllPlaylistsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCollectionPlaylistServiceModels_When_PlaylistsExistInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCollectionPlaylistServiceModels_When_PlaylistsExistInDb));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            Genre genre1 = Utils.CreateMockGenre("genre1Name", 123);
            Genre genre2 = Utils.CreateMockGenre("genre2Name", 124);
            Artist artist1 = Utils.CreateMockArtist("artist1Name", 234, 1, 0);
            Album album1 = Utils.CreateMockAlbum("album1Name", 345, artist1.Id);

            Song song1 = Utils.CreateMockSong("song1Name", 456, album1.Id, artist1.Id, genre1.Id, 180, 1);
            Song song2 = Utils.CreateMockSong("song1Name", 567, album1.Id, artist1.Id, genre1.Id, 240, 2);
            Song song3 = Utils.CreateMockSong("song1Name", 678, album1.Id, artist1.Id, genre1.Id, 180, 3);
            Playlist playlist1 = Utils.CreateMockPlaylist("playlist1Name", "playlist1Description", user1.Id, 600, false, false);
            PlaylistGenre playlist1Genre1 = Utils.CreateMockPlaylistGenre(playlist1.Id, genre1.Id);
            PlaylistSong playlist1Song1 = Utils.CreateMockPlaylistSong(playlist1.Id, song1.Id);
            PlaylistSong playlist1Song2 = Utils.CreateMockPlaylistSong(playlist1.Id, song2.Id);
            PlaylistSong playlist1Song3 = Utils.CreateMockPlaylistSong(playlist1.Id, song3.Id);

            Song song4 = Utils.CreateMockSong("song1Name", 678, album1.Id, artist1.Id, genre2.Id, 300, 4);
            Song song5 = Utils.CreateMockSong("song1Name", 678, album1.Id, artist1.Id, genre2.Id, 400, 5);
            Song song6 = Utils.CreateMockSong("song1Name", 678, album1.Id, artist1.Id, genre2.Id, 500, 6);
            Playlist playlist2 = Utils.CreateMockPlaylist("playlist2Name", "playlist2Description", user1.Id, 1200, false, false);
            PlaylistGenre playlist2Genre2 = Utils.CreateMockPlaylistGenre(playlist2.Id, genre2.Id);
            PlaylistSong playlist2Song4 = Utils.CreateMockPlaylistSong(playlist2.Id, song4.Id);
            PlaylistSong playlist2Song5 = Utils.CreateMockPlaylistSong(playlist2.Id, song5.Id);
            PlaylistSong playlist2Song6 = Utils.CreateMockPlaylistSong(playlist2.Id, song6.Id);

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Genres.Add(genre1);
                arrangeContext.Genres.Add(genre2);
                arrangeContext.Artists.Add(artist1);
                arrangeContext.Albums.Add(album1);
                arrangeContext.Songs.Add(song1);
                arrangeContext.Songs.Add(song2);
                arrangeContext.Songs.Add(song3);
                arrangeContext.Songs.Add(song4);
                arrangeContext.Songs.Add(song5);
                arrangeContext.Songs.Add(song6);
                arrangeContext.Playlists.Add(playlist1);
                arrangeContext.Playlists.Add(playlist2);
                arrangeContext.PlaylistsGenres.Add(playlist1Genre1);
                arrangeContext.PlaylistsGenres.Add(playlist2Genre2);
                arrangeContext.PlaylistsSongs.Add(playlist1Song1);
                arrangeContext.PlaylistsSongs.Add(playlist1Song2);
                arrangeContext.PlaylistsSongs.Add(playlist1Song3);
                arrangeContext.PlaylistsSongs.Add(playlist2Song4);
                arrangeContext.PlaylistsSongs.Add(playlist2Song5);
                arrangeContext.PlaylistsSongs.Add(playlist2Song6);
                arrangeContext.SaveChanges();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                var result = await sut.GetAllPlaylistsAsync();

                // Assert
                Assert.AreEqual(2, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnEmptyCollection_When_NoPlaylistsInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnEmptyCollection_When_NoPlaylistsInDb));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                PlaylistService sut = new PlaylistService(actContext, Utils.Mapper, new DateTimeProvider());
                var result = await sut.GetAllPlaylistsAsync();

                // Assert
                Assert.AreEqual(0, result.Count());
            }
        }
    }
}
