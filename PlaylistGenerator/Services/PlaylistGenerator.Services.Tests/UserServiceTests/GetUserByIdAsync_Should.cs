﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Models.User;
using PlaylistGenerator.Services.Providers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetUserByIdAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectUserServiceModel_When_UserExists()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectUserServiceModel_When_UserExists));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                UserServiceModel result = await sut.GetUserByIdAsync(user1.Id);

                // Assert
                Assert.AreEqual(user1.Id, result.Id);
                Assert.AreEqual(user1.FirstName, result.FirstName);
                Assert.AreEqual(user1.LastName, result.LastName);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_NoSuchUserInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_NoSuchUserInDb));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetUserByIdAsync(Guid.NewGuid()));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserIsBanned()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_UserIsBanned));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                actContext.Users.First(x => x.Id == user1.Id).IsBanned = true;

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetUserByIdAsync(user1.Id));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserIsDeleted()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_UserIsDeleted));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                actContext.Users.First(x => x.Id == user1.Id).IsDeleted = true;

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetUserByIdAsync(user1.Id));
            }
        }
    }
}
