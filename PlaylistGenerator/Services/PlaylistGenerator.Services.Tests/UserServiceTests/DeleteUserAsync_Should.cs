﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Providers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.UserServiceTests
{
    [TestClass]
    public class DeleteUserAsync_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_UserIsSuccessfullyDeleted()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnTrue_When_UserIsSuccessfullyDeleted));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                User userDeleted = actContext.Users.First(x => x.Id == user1.Id);
                bool result = await sut.DeleteUserAsync(user1.Id);

                // Assert
                Assert.AreEqual(true, result);
                Assert.AreEqual(true, userDeleted.IsDeleted);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_NoSuchUserExistsInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_NoSuchUserExistsInDb));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.DeleteUserAsync(Guid.NewGuid()));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserIsAlreadyDeleted()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnTrue_When_UserIsSuccessfullyDeleted));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                bool preAction = await sut.DeleteUserAsync(user1.Id);

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.DeleteUserAsync(user1.Id));
            }
        }
    }
}
