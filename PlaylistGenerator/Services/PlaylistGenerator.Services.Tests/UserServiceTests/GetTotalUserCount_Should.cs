﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Providers;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetTotalUserCount_Should
    {
        [TestMethod]
        public async Task ReturnCorrectTotalUserCount_When_UsersExist()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectTotalUserCount_When_UsersExist));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            User user2 = Utils.CreateMockUser("user2FirstName", "user2LastName");
            User user3 = Utils.CreateMockUser("user3FirstName", "user3LastName");
            User user4 = Utils.CreateMockUser("user4FirstName", "user4LastName");
            User user5 = Utils.CreateMockUser("user5FirstName", "user5LastName");
            User user6 = Utils.CreateMockUser("user6FirstName", "user6LastName");
            User user7 = Utils.CreateMockUser("user7FirstName", "user7LastName");
            User user8 = Utils.CreateMockUser("user8FirstName", "user8LastName");
            User user9 = Utils.CreateMockUser("user9FirstName", "user9LastName");
            User user10 = Utils.CreateMockUser("user10FirstName", "user10LastName");
            User user11 = Utils.CreateMockUser("user11FirstName", "user11LastName");
            User user12 = Utils.CreateMockUser("user12FirstName", "user12LastName");
            User user13 = Utils.CreateMockUser("user13FirstName", "user13LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Users.Add(user2);
                arrangeContext.Users.Add(user3);
                arrangeContext.Users.Add(user4);
                arrangeContext.Users.Add(user5);
                arrangeContext.Users.Add(user6);
                arrangeContext.Users.Add(user7);
                arrangeContext.Users.Add(user8);
                arrangeContext.Users.Add(user9);
                arrangeContext.Users.Add(user10);
                arrangeContext.Users.Add(user11);
                arrangeContext.Users.Add(user12);
                arrangeContext.Users.Add(user13);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                var result = sut.GetTotalUsersCount();

                // Assert
                Assert.AreEqual(13, result);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectTotalUserCount_When_UsersExistButOneIsDeleted()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectTotalUserCount_When_UsersExistButOneIsDeleted));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            User user2 = Utils.CreateMockUser("user2FirstName", "user2LastName");
            User user3 = Utils.CreateMockUser("user3FirstName", "user3LastName");
            User user4 = Utils.CreateMockUser("user4FirstName", "user4LastName");
            User user5 = Utils.CreateMockUser("user5FirstName", "user5LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Users.Add(user2);
                arrangeContext.Users.Add(user3);
                arrangeContext.Users.Add(user4);
                arrangeContext.Users.Add(user5);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                User user = actContext.Users.First();
                user.IsDeleted = true;
                await actContext.SaveChangesAsync();
                var result = sut.GetTotalUsersCount();

                // Assert
                Assert.AreEqual(4, result);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectTotalUserCount_When_UsersExistButOneIsBanned()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectTotalUserCount_When_UsersExistButOneIsBanned));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            User user2 = Utils.CreateMockUser("user2FirstName", "user2LastName");
            User user3 = Utils.CreateMockUser("user3FirstName", "user3LastName");
            User user4 = Utils.CreateMockUser("user4FirstName", "user4LastName");
            User user5 = Utils.CreateMockUser("user5FirstName", "user5LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Users.Add(user2);
                arrangeContext.Users.Add(user3);
                arrangeContext.Users.Add(user4);
                arrangeContext.Users.Add(user5);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                User user = actContext.Users.First();
                user.IsBanned = true;
                await actContext.SaveChangesAsync();
                var result = sut.GetTotalUsersCount();

                // Assert
                Assert.AreEqual(5, result);
            }
        }

        [TestMethod]
        public void ReturnZero_When_NoUsersInDatabase()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnZero_When_NoUsersInDatabase));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                var result = sut.GetTotalUsersCount();

                // Assert
                Assert.AreEqual(0, result);
            }
        }
    }
}
