﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Services.Providers;

namespace PlaylistGenerator.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetPageCountSizing_Should
    {
        [TestMethod]
        public void ReturnCorrectPageCountSize_When_InputIsCorrect()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectPageCountSize_When_InputIsCorrect));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                var result = sut.GetPageCountSizing();

                // Assert
                Assert.AreEqual(12, result);
            }
        }
    }
}
