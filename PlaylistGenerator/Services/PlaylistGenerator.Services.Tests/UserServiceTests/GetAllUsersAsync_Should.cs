﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Providers;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetAllUsersAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectCollectiontUserServiceModel_When_UsersExistInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectCollectiontUserServiceModel_When_UsersExistInDb));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            User user2 = Utils.CreateMockUser("user2FirstName", "user2LastName");
            User user3 = Utils.CreateMockUser("user3FirstName", "user3LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Users.Add(user2);
                arrangeContext.Users.Add(user3);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                var result = await sut.GetAllUsersAsync();

                // Assert
                Assert.AreEqual(3, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnCorrectCollectiontUserServiceModel_When_SomeUsersAreDeleted()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectCollectiontUserServiceModel_When_SomeUsersAreDeleted));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");
            User user2 = Utils.CreateMockUser("user2FirstName", "user2LastName");
            User user3 = Utils.CreateMockUser("user3FirstName", "user3LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Users.Add(user2);
                arrangeContext.Users.Add(user3);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                var preAction = actContext.Users.First(x => x.Id == user2.Id).IsDeleted = true;
                await actContext.SaveChangesAsync();
                var result = await sut.GetAllUsersAsync();

                // Assert
                Assert.AreEqual(2, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnEmptyCollectiontUserServiceModel_When_NoUsersExistInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnEmptyCollectiontUserServiceModel_When_NoUsersExistInDb));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                var result = await sut.GetAllUsersAsync();

                // Assert
                Assert.AreEqual(0, result.Count());
            }
        }
    }
}
