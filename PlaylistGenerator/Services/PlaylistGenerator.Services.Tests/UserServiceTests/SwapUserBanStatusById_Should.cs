﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Providers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.UserServiceTests
{
    [TestClass]
    public class SwapUserBanStatusById_Should
    {
        [TestMethod]
        public async Task SwapBanStatus_When_UserIdIsCorrectAndNotBanned()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(SwapBanStatus_When_UserIdIsCorrectAndNotBanned));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                await sut.SwapUserBanStatusByIdAsync(user1.Id);
                var user1BanStatus = actContext.Users.First(x => x.FirstName == "user1FirstName" && x.LastName == "user1LastName").IsBanned;

                // Assert
                Assert.AreEqual(true, user1BanStatus);
            }
        }

        [TestMethod]
        public async Task SwapBanStatus_When_UserIdIsCorrectAndUserIsBanned()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(SwapBanStatus_When_UserIdIsCorrectAndUserIsBanned));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                var user = actContext.Users.First(x => x.FirstName == "user1FirstName" && x.LastName == "user1LastName");
                user.IsBanned = true;
                await actContext.SaveChangesAsync();

                await sut.SwapUserBanStatusByIdAsync(user1.Id);


                // Assert
                Assert.AreEqual(false, user.IsBanned);
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenUserIsDeleted()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenUserIsDeleted));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                var user = actContext.Users.First(x => x.FirstName == "user1FirstName" && x.LastName == "user1LastName");
                user.IsDeleted = true;
                await actContext.SaveChangesAsync();

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.SwapUserBanStatusByIdAsync(user.Id));
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenNoSuchUserInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenNoSuchUserInDb));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.SwapUserBanStatusByIdAsync(Guid.NewGuid()));
            }
        }
    }
}
