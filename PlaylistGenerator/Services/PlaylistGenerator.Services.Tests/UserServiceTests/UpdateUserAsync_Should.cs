﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Models.User;
using PlaylistGenerator.Services.Providers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.UserServiceTests
{
    [TestClass]
    public class UpdateUserAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectUserServiceModel_When_UserIsUpdatedSuccessfully()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectUserServiceModel_When_UserIsUpdatedSuccessfully));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                UserServiceModel modelToUpdate = new UserServiceModel
                {
                    Id = user1.Id,
                    FirstName = "user1FirstNameUpdated",
                    LastName = "user1LastNameUpdated"
                };

                UserServiceModel result = await sut.UpdateUserAsync(modelToUpdate);

                // Assert
                Assert.AreEqual("user1FirstNameUpdated", result.FirstName);
                Assert.AreEqual("user1LastNameUpdated", result.LastName);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserIsDeleted()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_UserIsDeleted));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                UserServiceModel modelToUpdate = new UserServiceModel
                {
                    Id = user1.Id,
                    FirstName = "user1FirstNameUpdated",
                    LastName = "user1LastNameUpdated"
                };

                bool preAction = await sut.DeleteUserAsync(user1.Id);

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateUserAsync(modelToUpdate));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserIsBanned()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_UserIsBanned));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                UserServiceModel modelToUpdate = new UserServiceModel
                {
                    Id = user1.Id,
                    FirstName = "user1FirstNameUpdated",
                    LastName = "user1LastNameUpdated"
                };

                actContext.Users.First(x => x.Id == user1.Id).IsBanned = true;

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateUserAsync(modelToUpdate));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_NoSuchUserIdInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_NoSuchUserIdInDb));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                UserServiceModel modelWithNonExistingId = new UserServiceModel
                {
                    Id = Guid.NewGuid(),
                    FirstName = "FirstName",
                    LastName = "LastName"
                };

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateUserAsync(modelWithNonExistingId));
            }
        }
    }
}
