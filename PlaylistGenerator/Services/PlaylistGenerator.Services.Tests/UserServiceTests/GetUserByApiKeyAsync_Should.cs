﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Models.User;
using PlaylistGenerator.Services.Providers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetUserByApiKeyAsync_Should
    {
        [TestClass]
        public class GetUserByIdAsync_Should
        {
            [TestMethod]
            public async Task ReturnCorrectUserServiceModel_When_UserWithThisApiKeyExists()
            {
                // Arrange
                var options = Utils.GetOptions(nameof(ReturnCorrectUserServiceModel_When_UserWithThisApiKeyExists));

                User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

                using (var arrangeContext = new PlaylistGeneratorDbContext(options))
                {
                    arrangeContext.Users.Add(user1);
                    await arrangeContext.SaveChangesAsync();
                }

                // Act
                using (var actContext = new PlaylistGeneratorDbContext(options))
                {
                    UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                    Guid? generatedGuid = await sut.UpdateUserApiKeyAsync(user1.Id);
                    UserServiceModel result = await sut.GetUserByApiKeyAsync(generatedGuid);

                    // Assert
                    Assert.AreEqual(user1.Id, result.Id);
                    Assert.AreEqual(user1.FirstName, result.FirstName);
                    Assert.AreEqual(user1.LastName, result.LastName);
                }
            }

            [TestMethod]
            public async Task ThrowException_When_NoSuchUserWithApiKeyInDb()
            {
                // Arrange
                var options = Utils.GetOptions(nameof(ThrowException_When_NoSuchUserWithApiKeyInDb));

                // Act
                using (var actContext = new PlaylistGeneratorDbContext(options))
                {
                    UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                    // Assert
                    await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetUserByApiKeyAsync(Guid.NewGuid()));
                }
            }

            [TestMethod]
            public async Task ReturnNull_When_UserWithApiKeyIsBanned()
            {
                // Arrange
                var options = Utils.GetOptions(nameof(ReturnNull_When_UserWithApiKeyIsBanned));

                User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

                using (var arrangeContext = new PlaylistGeneratorDbContext(options))
                {
                    arrangeContext.Users.Add(user1);
                    await arrangeContext.SaveChangesAsync();
                }

                // Act
                using (var actContext = new PlaylistGeneratorDbContext(options))
                {
                    UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                    Guid? apiKey = await sut.UpdateUserApiKeyAsync(user1.Id);
                    actContext.Users.First(x => x.Id == user1.Id).IsBanned = true;

                    // Assert
                    await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetUserByApiKeyAsync(apiKey));
                }
            }

            [TestMethod]
            public async Task ThrowException_WhenUserWithApiKeyIsDeleted()
            {
                // Arrange
                var options = Utils.GetOptions(nameof(ThrowException_WhenUserWithApiKeyIsDeleted));

                User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

                using (var arrangeContext = new PlaylistGeneratorDbContext(options))
                {
                    arrangeContext.Users.Add(user1);
                    await arrangeContext.SaveChangesAsync();
                }

                // Act
                using (var actContext = new PlaylistGeneratorDbContext(options))
                {
                    UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                    Guid? apiKey = await sut.UpdateUserApiKeyAsync(user1.Id);
                    actContext.Users.First(x => x.Id == user1.Id).IsDeleted = true;

                    // Assert
                    await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetUserByApiKeyAsync(apiKey));
                }
            }
        }
    }
}
