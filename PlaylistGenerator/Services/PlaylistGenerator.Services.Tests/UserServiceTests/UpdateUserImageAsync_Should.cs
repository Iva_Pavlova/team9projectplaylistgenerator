﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Providers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.UserServiceTests
{
    [TestClass]
    public class UpdateUserImageAsync_Should
    {
        [TestMethod]
        public async Task ReturnTrueAndChangeImagePath_When_UserIdIsValid()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnTrueAndChangeImagePath_When_UserIdIsValid));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                bool result = await sut.UpdateUserImageAsync(user1.Id);
                User userUpdated = actContext.Users.First(x => x.Id == user1.Id);
                // Assert
                Assert.AreEqual(true, result);
                Assert.AreEqual($"/images/users/{user1.Id}.jpg", userUpdated.Image);
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenUserIsDeleted()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenUserIsDeleted));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                bool preAction = await sut.DeleteUserAsync(user1.Id);

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateUserImageAsync(user1.Id));
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenUserIsBanned()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenUserIsBanned));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                actContext.Users.First(x => x.Id == user1.Id).IsBanned = true;

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateUserImageAsync(user1.Id));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_NoSuchPlaylistIdInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_NoSuchPlaylistIdInDb));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                bool preAction = await sut.DeleteUserAsync(user1.Id);

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateUserImageAsync(user1.Id));
            }
        }
    }
}
