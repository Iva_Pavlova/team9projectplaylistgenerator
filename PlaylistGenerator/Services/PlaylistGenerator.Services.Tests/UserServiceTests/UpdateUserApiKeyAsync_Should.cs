﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Providers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.UserServiceTests
{
    [TestClass]
    public class UpdateUserApiKeyAsync_Should
    {
        [TestMethod]
        public async Task ReturnApiKey_When_UserIdIsValid()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ReturnApiKey_When_UserIdIsValid));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                var result = await sut.UpdateUserApiKeyAsync(user1.Id);

                // Assert
                Assert.IsInstanceOfType(result, typeof(Guid));
            }
        }

        [TestMethod]
        public async Task ThrowException_WhenNoSuchUserInDb()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_WhenNoSuchUserInDb));

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateUserApiKeyAsync(Guid.NewGuid()));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserIsBanned()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_UserIsBanned));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                actContext.Users.First(x => x.Id == user1.Id).IsBanned = true;

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateUserApiKeyAsync(user1.Id));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserIsDeleted()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(ThrowException_When_UserIsDeleted));

            User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                arrangeContext.Users.Add(user1);
                await arrangeContext.SaveChangesAsync();
            }

            // Act
            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                actContext.Users.First(x => x.Id == user1.Id).IsDeleted = true;

                // Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateUserApiKeyAsync(user1.Id));
            }
        }
    }
}
