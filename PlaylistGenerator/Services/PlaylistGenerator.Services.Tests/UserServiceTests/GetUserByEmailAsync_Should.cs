﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Models.User;
using PlaylistGenerator.Services.Providers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetUserByEmailAsync_Should
    {
        [TestClass]
        public class GetUserByIdAsync_Should
        {
            [TestMethod]
            public async Task ReturnCorrectUserServiceModel_When_UserWithEmailExists()
            {
                // Arrange
                var options = Utils.GetOptions(nameof(ReturnCorrectUserServiceModel_When_UserWithEmailExists));

                User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

                using (var arrangeContext = new PlaylistGeneratorDbContext(options))
                {
                    arrangeContext.Users.Add(user1);
                    await arrangeContext.SaveChangesAsync();
                }

                // Act
                using (var actContext = new PlaylistGeneratorDbContext(options))
                {
                    UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                    UserServiceModel result = await sut.GetUserByEmailAsync("user1FirstName".ToLower() + "user1LastName".ToLower() + "@mail.com");

                    // Assert
                    Assert.AreEqual(user1.Id, result.Id);
                    Assert.AreEqual(user1.FirstName, result.FirstName);
                    Assert.AreEqual(user1.LastName, result.LastName);
                }
            }

            [TestMethod]
            public async Task ReturnNull_When_NoSuchUserEmailInDb()
            {
                // Arrange
                var options = Utils.GetOptions(nameof(ReturnNull_When_NoSuchUserEmailInDb));

                // Act
                using (var actContext = new PlaylistGeneratorDbContext(options))
                {
                    UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                    var result = await sut.GetUserByEmailAsync("wrongEmail@mail.io");
                    // Assert
                    Assert.AreEqual(null, result);
                }
            }

            [TestMethod]
            public async Task ReturnNull_When_UserIsBanned()
            {
                // Arrange
                var options = Utils.GetOptions(nameof(ReturnNull_When_UserIsBanned));

                User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

                using (var arrangeContext = new PlaylistGeneratorDbContext(options))
                {
                    arrangeContext.Users.Add(user1);
                    await arrangeContext.SaveChangesAsync();
                }

                // Act
                using (var actContext = new PlaylistGeneratorDbContext(options))
                {
                    UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                    actContext.Users.First(x => x.Id == user1.Id).IsBanned = true;

                    // Assert
                    await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetUserByEmailAsync("user1FirstName".ToLower() + "user1LastName".ToLower() + "@mail.com"));
                }
            }

            [TestMethod]
            public async Task ThrowException_WhenUserIsDeleted()
            {
                // Arrange
                var options = Utils.GetOptions(nameof(ThrowException_WhenUserIsDeleted));

                User user1 = Utils.CreateMockUser("user1FirstName", "user1LastName");

                using (var arrangeContext = new PlaylistGeneratorDbContext(options))
                {
                    arrangeContext.Users.Add(user1);
                    await arrangeContext.SaveChangesAsync();
                }

                // Act
                using (var actContext = new PlaylistGeneratorDbContext(options))
                {
                    UserService sut = new UserService(actContext, Utils.Mapper, new DateTimeProvider());
                    actContext.Users.First(x => x.Id == user1.Id).IsDeleted = true;

                    // Assert
                    await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetUserByEmailAsync("user1FirstName".ToLower() + "user1LastName".ToLower() + "@mail.com"));
                }
            }
        }
    }
}
