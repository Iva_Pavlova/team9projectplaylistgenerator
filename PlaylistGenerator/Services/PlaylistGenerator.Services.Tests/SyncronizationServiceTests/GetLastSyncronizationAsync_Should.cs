﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Providers;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.SyncronizationServiceTests
{
    [TestClass]
    public class GetLastSyncronizationAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectSyncronization()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectSyncronization));
            var dateTimeProvider = new Mock<IDateTimeProvider>();
            var expectedDate = DateTime.UtcNow;
            dateTimeProvider.Setup(d => d.GetDateTime()).Returns(expectedDate);

            var expectedSyncronization = new Syncronization
            {
                Date = expectedDate,
                IsComplete = true,
                WasInterrupted = false
            };

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                var syncronizaton1 = new Syncronization
                {
                    Date = DateTime.UtcNow.AddYears(-1),
                    IsComplete = true,
                    WasInterrupted = false
                };

                await arrangeContext.Syncronisations.AddAsync(syncronizaton1);
                await arrangeContext.Syncronisations.AddAsync(expectedSyncronization);
                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var genreService = new GenreService(actContext, Utils.Mapper);
                var songService = new SongService(actContext, Utils.Mapper);
                var sut = new SyncronizationService(songService, genreService, actContext, dateTimeProvider.Object, Utils.Mapper);

                //Act
                var actual = await sut.GetLastSyncronizationAsync();

                //Assert
                Assert.AreEqual(expectedSyncronization.Date, actual.Date);
                Assert.AreEqual(expectedSyncronization.IsComplete, actual.IsComplete);
                Assert.AreEqual(expectedSyncronization.WasInterrupted, actual.WasInterrupted);
            }
        }

        [TestMethod]
        public async Task ReturnNull_When_NoSyncronizationsInDb()
        {
            var options = Utils.GetOptions(nameof(ReturnNull_When_NoSyncronizationsInDb));

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var genreService = new GenreService(actContext, Utils.Mapper);
                var songService = new SongService(actContext, Utils.Mapper);
                var sut = new SyncronizationService(songService, genreService, actContext, new DateTimeProvider(), Utils.Mapper);

                //Act
                var actual = await sut.GetLastSyncronizationAsync();

                //Assert
                Assert.IsNull(actual);
            }
        }
    }
}
