﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.SyncronizationServiceTests
{
    [TestClass]
    public class SyncronizeGenresAndSongsAsync_Should
    {
        //[TestMethod]
        public async Task AddSyncronizationToDatabase()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(AddSyncronizationToDatabase));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                var genre1 = new Genre
                {
                    Name = "Pop",
                    DeezerId = "132",
                    PictureURL = "https://api.deezer.com/genre/132/image"
                };

                await arrangeContext.Genres.AddAsync(genre1);
                await arrangeContext.SaveChangesAsync();

                var songService = new SongService(arrangeContext, Utils.Mapper);
                await songService.LoadSongsInDbAsync();
            };



            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedDate = DateTime.UtcNow;

                var genreService = new GenreService(actContext, Utils.Mapper);
                var songService = new SongService(actContext, Utils.Mapper);
                var dateTimeProvider = new Mock<IDateTimeProvider>();

                dateTimeProvider.Setup(d => d.GetDateTime()).Returns(expectedDate);

                var sut = new SyncronizationService(songService, genreService, actContext, dateTimeProvider.Object, Utils.Mapper);

                //Act
                await sut.SyncronizeGenresAndSongsAsync();
                var syncronization = await actContext.Syncronisations.FirstOrDefaultAsync();

                //Assert
                Assert.IsNotNull(syncronization);
                Assert.AreEqual(expectedDate, syncronization.Date);
            }
        }
    }
}