using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Models;
using PlaylistGenerator.Services.Models.Album;
using PlaylistGenerator.Services.Models.Artist;
using PlaylistGenerator.Services.Models.Genre;
using PlaylistGenerator.Services.Models.Song;
using PlaylistGenerator.Services.Models.Syncronization;
using PlaylistGenerator.Services.Models.User;
using PlaylistGenerator.Web.Models.Syncronization;
using System;
using System.Linq;

namespace PlaylistGenerator.Services.Tests
{
    public class Utils
    {
        private static MapperConfiguration mapperConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Playlist, PlaylistServiceModel>()
              .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => $"{src.User.FirstName} {src.User.LastName}"))
              .ForMember(dest => dest.UserImage, opt => opt.MapFrom(src => src.User.Image))
              .ForMember(dest => dest.DurationPlaylist, opt => opt.MapFrom(src => src.Songs.Sum(x => x.Song.Duration)))
              .ForMember(dest => dest.Rank, opt => opt.MapFrom(src => (int)(src.Songs.Select(playlistSong => playlistSong.Song).Sum(song => song.Rank))));

            cfg.CreateMap<PlaylistServiceModel, Playlist>()
                .ForMember(dest => dest.User, opt => opt.Ignore());

            cfg.CreateMap<Genre, GenreServiceModel>();

            cfg.CreateMap<User, UserServiceModel>().ReverseMap();

            cfg.CreateMap<Song, SongServiceModel>()
                .ForMember(dest => dest.Album, opt => opt.MapFrom(src => src.Album.Name))
                .ForMember(dest => dest.Artist, opt => opt.MapFrom(src => src.Artist.Name))
                .ForMember(dest => dest.Genre, opt => opt.MapFrom(src => src.Genre.Name))
                .ReverseMap();

            cfg.CreateMap<Album, AlbumServiceModel>()
                .ForMember(dest => dest.Artist, opt => opt.MapFrom(src => src.Artist.Name));

            cfg.CreateMap<Artist, ArtistServiceModel>();

            cfg.CreateMap<Syncronization, SyncronizationServiceModel>();
            cfg.CreateMap<SyncronizationServiceModel, SyncronizationViewModel>();
        });

        private static IMapper mapper;

        public static DbContextOptions<PlaylistGeneratorDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<PlaylistGeneratorDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }


        public static IMapper Mapper
        {
            get
            {
                if (mapper == null)
                {
                    mapper = new Mapper(mapperConfig);
                }

                return mapper;
            }
        }

        public static User CreateMockUser(string firstName, string lastName)
        {
            return new User
            {
                Id = Guid.NewGuid(),
                FirstName = firstName,
                LastName = lastName,
                IsDeleted = false,
                IsAdmin = false,
                IsBanned = false,
                UserName = $"{firstName.ToLower()}{lastName.ToLower()}@mail.com",
                Email = $"{firstName.ToLower()}{lastName.ToLower()}@mail.com",
                NormalizedUserName = $"{firstName.ToUpper()}{lastName.ToUpper()}@MAIL.COM",
                NormalizedEmail = $"{firstName.ToUpper()}{lastName.ToUpper()}@MAIL.COM",
            };
        }

        public static Genre CreateMockGenre(string name, int deezerId)
        {
            return new Genre
            {
                Id = Guid.NewGuid(),
                DeezerId = deezerId.ToString(),
                Name = name,
                PictureURL = $"https://api.deezer.com/genre/{deezerId.ToString()}/image",
            };
        }

        public static Artist CreateMockArtist(string name, int deezerId, int albumCount = 0, int fanCount = 0)
        {
            return new Artist
            {
                Id = Guid.NewGuid(),
                DeezerId = deezerId.ToString(),
                Name = name,
                AlbumCount = albumCount,
                FanCount = fanCount,
                ArtistPageURL = $"https://api.deezer.com/artist/{deezerId.ToString()}",
                PictureURL = $"https://api.deezer.com/artist/{deezerId.ToString()}/image",
                SongListUrl = $"https://api.deezer.com/artist/{deezerId.ToString()}/top?limit=50",
            };
        }

        public static Album CreateMockAlbum(string name, int deezerId, Guid artistId)
        {
            return new Album
            {
                Id = Guid.NewGuid(),
                DeezerId = deezerId.ToString(),
                Name = name,
                SongListUrl = $"https://api.deezer.com/album/{deezerId.ToString()}/tracks",
                ArtistId = artistId,
            };
        }

        public static Song CreateMockSong(string name, int deezerId, Guid albumId, Guid artistId, Guid genreId, int duration, int rank = 100000)
        {
            return new Song
            {
                Id = Guid.NewGuid(),
                DeezerId = deezerId.ToString(),
                Name = name,
                SongURL = $"https://www.deezer.com/track/{deezerId.ToString()}",
                Duration = duration,
                Rank = rank,
                PreviewURL = "https://cdns-preview-9.dzcdn.net/stream/c-960105cd0b227422d47a4224f32c1c38-3.mp3",
                AlbumId = albumId,
                ArtistId = artistId,
                GenreId = genreId,
            };
        }

        public static Playlist CreateMockPlaylist(string name, string description, Guid userId, int durationTravel = (60 * 10), bool isUnlisted = false, bool isDeleted = false)
        {
            return new Playlist
            {
                Id = Guid.NewGuid(),
                Name = name,
                Description = description,
                IsUnlisted = isUnlisted,
                IsDeleted = isDeleted,
                DateCreated = DateTime.Parse("10/10/2010"),
                DateModified = DateTime.Parse("10/10/2010"),
                DurationTravel = durationTravel,
                Image = "/images/playlists/default.jpg",
                UserId = userId,
            };
        }

        public static PlaylistGenre CreateMockPlaylistGenre(Guid playlistId, Guid genreId)
        {
            return new PlaylistGenre
            {
                GenreId = genreId,
                PlaylistId = playlistId,
            };
        }

        public static PlaylistSong CreateMockPlaylistSong(Guid playlistId, Guid songId)
        {
            return new PlaylistSong
            {
                PlaylistId = playlistId,
                SongId = songId,
            };
        }
    }
}
