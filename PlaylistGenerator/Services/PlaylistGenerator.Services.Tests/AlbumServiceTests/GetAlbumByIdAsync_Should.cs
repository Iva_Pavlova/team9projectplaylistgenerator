﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.AlbumServiceTests
{
    [TestClass]
    public class GetAlbumByIdAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectAlbum()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectAlbum));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("artistName", 123));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(new Album
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    SongListUrl = "SongListUrl",
                    ArtistId = (await arrangeContext.Artists.FirstAsync()).Id
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedAlbum = await actContext.Albums.FirstAsync();
                var sut = new AlbumService(actContext, Utils.Mapper);

                //Act
                var actualAlbum = await sut.GetAlbumByIdAsync(expectedAlbum.Id);

                //Assert
                Assert.AreEqual(expectedAlbum.Id, actualAlbum.Id);
                Assert.AreEqual(expectedAlbum.Name, actualAlbum.Name);
                Assert.AreEqual(expectedAlbum.SongListUrl, actualAlbum.SongListUrl);
            }
        }

        [TestMethod]
        public async Task Throw_When_AlbumNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_AlbumNotFound));

            var context = new PlaylistGeneratorDbContext(options);
            var sut = new AlbumService(context, Utils.Mapper);

            //Act & Assert
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAlbumByIdAsync(Guid.NewGuid()));

        }

        [TestMethod]
        public async Task ReturnAlbum_When_ValidApiKey()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnAlbum_When_ValidApiKey));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("artistName", 123));

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                (await arrangeContext.Users.FirstAsync()).ApiKey = Guid.NewGuid();

                await arrangeContext.Albums.AddAsync(new Album
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    SongListUrl = "SongListUrl",
                    ArtistId = (await arrangeContext.Artists.FirstAsync()).Id
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedAlbum = await actContext.Albums.FirstAsync();
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new AlbumService(actContext, Utils.Mapper);

                //Act
                var actualAlbum = await sut.GetAlbumByIdAsync(expectedAlbum.Id, true, apiKey);

                //Assert
                Assert.AreEqual(expectedAlbum.Id, actualAlbum.Id);
                Assert.AreEqual(expectedAlbum.Name, actualAlbum.Name);
                Assert.AreEqual(expectedAlbum.SongListUrl, actualAlbum.SongListUrl);
            }
        }

        [TestMethod]
        public async Task Throw_When_InvalidApiKey()
        {
            var options = Utils.GetOptions(nameof(Throw_When_InvalidApiKey));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("artistName", 123));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(new Album
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    SongListUrl = "SongListUrl",
                    ArtistId = (await arrangeContext.Artists.FirstAsync()).Id
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedAlbum = await actContext.Albums.FirstAsync();
                var sut = new AlbumService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAlbumByIdAsync(expectedAlbum.Id, true, Guid.NewGuid()));

            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsBanned()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsBanned));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("artistName", 123));

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsBanned = true;

                await arrangeContext.Albums.AddAsync(new Album
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    SongListUrl = "SongListUrl",
                    ArtistId = (await arrangeContext.Artists.FirstAsync()).Id
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedAlbum = await actContext.Albums.FirstAsync();
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new AlbumService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAlbumByIdAsync(expectedAlbum.Id, true, apiKey));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsDeleted));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("artistName", 123));

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsDeleted = true;

                await arrangeContext.Albums.AddAsync(new Album
                {
                    Name = "Name",
                    DeezerId = "DeezerId",
                    SongListUrl = "SongListUrl",
                    ArtistId = (await arrangeContext.Artists.FirstAsync()).Id
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedAlbum = await actContext.Albums.FirstAsync();
                var apiKey = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new AlbumService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAlbumByIdAsync(expectedAlbum.Id, true, apiKey));
            }
        }
    }
}