﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.SongServiceTests
{
    [TestClass]
    public class UpdateSongsInDbAsync_Should
    {
        [TestMethod]
        public async Task Throw_When_NoSongsInDb()
        {
            var options = Utils.GetOptions(nameof(Throw_When_NoSongsInDb));

            var context = new PlaylistGeneratorDbContext(options);
            var sut = new SongService(context, Utils.Mapper);

            //Act & Assert
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateSongsInDbAsync());
        }

        [TestMethod]
        public async Task Throw_When_ApiKeyInvalid()
        {
            var options = Utils.GetOptions(nameof(Throw_When_ApiKeyInvalid));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var sut = new SongService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateSongsInDbAsync(true, Guid.NewGuid()));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsBanned()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsBanned));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {

                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();

                user.ApiKey = Guid.NewGuid();
                user.IsBanned = true;

                await arrangeContext.SaveChangesAsync();

            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new SongService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateSongsInDbAsync(true, key));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsDeleted));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });


                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstName", "lastName"));

                await arrangeContext.SaveChangesAsync();

                var user = await arrangeContext.Users.FirstAsync();

                user.ApiKey = Guid.NewGuid();
                user.IsDeleted = true;

                await arrangeContext.SaveChangesAsync();

            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new SongService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateSongsInDbAsync(true, key));
            }
        }
    }
}
