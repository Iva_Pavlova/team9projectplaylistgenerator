﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.SongServiceTests
{
    [TestClass]
    public class GetSongsByPlaylistIdAsync_Should
    {
        [TestMethod]
        public async Task ReturnPlaylistSongsCorrectly()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnPlaylistSongsCorrectly));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstname", "lastname"));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Playlists.AddAsync(Utils.CreateMockPlaylist("playlist", "description", (await arrangeContext.Users.FirstAsync()).Id));
                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));

                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();

                var playlistId = (await arrangeContext.Playlists.FirstAsync()).Id;

                var playlistSong1 = new PlaylistSong
                {
                    SongId = (await arrangeContext.Songs.FirstAsync()).Id,
                    PlaylistId = playlistId
                };

                var playlistSong2 = new PlaylistSong
                {
                    SongId = (await arrangeContext.Songs.Skip(1).FirstAsync()).Id,
                    PlaylistId = playlistId
                };


                await arrangeContext.PlaylistsSongs.AddAsync(playlistSong1);
                await arrangeContext.PlaylistsSongs.AddAsync(playlistSong2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedSongs = await actContext.Songs.ToListAsync();
                var playlistId = (await actContext.Playlists.FirstAsync()).Id;
                var sut = new SongService(actContext, Utils.Mapper);

                //Act
                var result = await sut.GetSongsByPlaylistIdAsync(playlistId);
                var actualSongs = result.Reverse().ToList();

                //Assert
                Assert.AreEqual(expectedSongs.Count, actualSongs.Count);

                for (int i = 0; i < actualSongs.Count; i++)
                {
                    Assert.AreEqual(expectedSongs[i].Id, actualSongs[i].Id);
                    Assert.AreEqual(expectedSongs[i].DeezerId, actualSongs[i].DeezerId);
                    Assert.AreEqual(expectedSongs[i].Name, actualSongs[i].Name);
                    Assert.AreEqual(expectedSongs[i].ArtistId, actualSongs[i].ArtistId);
                    Assert.AreEqual(expectedSongs[i].AlbumId, actualSongs[i].AlbumId);
                    Assert.AreEqual(expectedSongs[i].GenreId, actualSongs[i].GenreId);
                    Assert.AreEqual(expectedSongs[i].SongURL, actualSongs[i].SongURL);
                }
            }
        }

        [TestMethod]
        public async Task ReturnPlaylistSongs_When_ApiKeyValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnPlaylistSongs_When_ApiKeyValid));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstname", "lastname"));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Playlists.AddAsync(Utils.CreateMockPlaylist("playlist", "description", (await arrangeContext.Users.FirstAsync()).Id));
                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                (await arrangeContext.Users.FirstAsync()).ApiKey = Guid.NewGuid();

                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();

                var playlistId = (await arrangeContext.Playlists.FirstAsync()).Id;

                var playlistSong1 = new PlaylistSong
                {
                    SongId = (await arrangeContext.Songs.FirstAsync()).Id,
                    PlaylistId = playlistId
                };

                var playlistSong2 = new PlaylistSong
                {
                    SongId = (await arrangeContext.Songs.Skip(1).FirstAsync()).Id,
                    PlaylistId = playlistId
                };


                await arrangeContext.PlaylistsSongs.AddAsync(playlistSong1);
                await arrangeContext.PlaylistsSongs.AddAsync(playlistSong2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedSongs = await actContext.Songs.ToListAsync();
                var playlistId = (await actContext.Playlists.FirstAsync()).Id;
                var sut = new SongService(actContext, Utils.Mapper);
                var key = (await actContext.Users.FirstAsync()).ApiKey;

                //Act
                var result = await sut.GetSongsByPlaylistIdAsync(playlistId, true, key);
                var actualSongs = result.Reverse().ToList();

                //Assert
                Assert.AreEqual(expectedSongs.Count, actualSongs.Count);

                for (int i = 0; i < actualSongs.Count; i++)
                {
                    Assert.AreEqual(expectedSongs[i].Id, actualSongs[i].Id);
                    Assert.AreEqual(expectedSongs[i].DeezerId, actualSongs[i].DeezerId);
                    Assert.AreEqual(expectedSongs[i].Name, actualSongs[i].Name);
                    Assert.AreEqual(expectedSongs[i].ArtistId, actualSongs[i].ArtistId);
                    Assert.AreEqual(expectedSongs[i].AlbumId, actualSongs[i].AlbumId);
                    Assert.AreEqual(expectedSongs[i].GenreId, actualSongs[i].GenreId);
                    Assert.AreEqual(expectedSongs[i].SongURL, actualSongs[i].SongURL);
                }
            }
        }

        [TestMethod]
        public async Task Throw_When_InvalidApiKey()
        { //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_InvalidApiKey));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstname", "lastname"));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Playlists.AddAsync(Utils.CreateMockPlaylist("playlist", "description", (await arrangeContext.Users.FirstAsync()).Id));
                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));

                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();

                var playlistId = (await arrangeContext.Playlists.FirstAsync()).Id;

                var playlistSong1 = new PlaylistSong
                {
                    SongId = (await arrangeContext.Songs.FirstAsync()).Id,
                    PlaylistId = playlistId
                };

                var playlistSong2 = new PlaylistSong
                {
                    SongId = (await arrangeContext.Songs.Skip(1).FirstAsync()).Id,
                    PlaylistId = playlistId
                };


                await arrangeContext.PlaylistsSongs.AddAsync(playlistSong1);
                await arrangeContext.PlaylistsSongs.AddAsync(playlistSong2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var playlistId = (await actContext.Playlists.FirstAsync()).Id;
                var sut = new SongService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetSongsByPlaylistIdAsync(playlistId, true, Guid.NewGuid()));
            }

        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsBanned()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsBanned));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstname", "lastname"));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Playlists.AddAsync(Utils.CreateMockPlaylist("playlist", "description", (await arrangeContext.Users.FirstAsync()).Id));
                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsBanned = true;

                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();

                var playlistId = (await arrangeContext.Playlists.FirstAsync()).Id;

                var playlistSong1 = new PlaylistSong
                {
                    SongId = (await arrangeContext.Songs.FirstAsync()).Id,
                    PlaylistId = playlistId
                };

                var playlistSong2 = new PlaylistSong
                {
                    SongId = (await arrangeContext.Songs.Skip(1).FirstAsync()).Id,
                    PlaylistId = playlistId
                };


                await arrangeContext.PlaylistsSongs.AddAsync(playlistSong1);
                await arrangeContext.PlaylistsSongs.AddAsync(playlistSong2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var playlistId = (await actContext.Playlists.FirstAsync()).Id;
                var sut = new SongService(actContext, Utils.Mapper);
                var key = (await actContext.Users.FirstAsync()).ApiKey;

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetSongsByPlaylistIdAsync(playlistId, true, key));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsDeleted));


            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("firstname", "lastname"));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Playlists.AddAsync(Utils.CreateMockPlaylist("playlist", "description", (await arrangeContext.Users.FirstAsync()).Id));
                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsDeleted = true;

                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();

                var playlistId = (await arrangeContext.Playlists.FirstAsync()).Id;

                var playlistSong1 = new PlaylistSong
                {
                    SongId = (await arrangeContext.Songs.FirstAsync()).Id,
                    PlaylistId = playlistId
                };

                var playlistSong2 = new PlaylistSong
                {
                    SongId = (await arrangeContext.Songs.Skip(1).FirstAsync()).Id,
                    PlaylistId = playlistId
                };


                await arrangeContext.PlaylistsSongs.AddAsync(playlistSong1);
                await arrangeContext.PlaylistsSongs.AddAsync(playlistSong2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var playlistId = (await actContext.Playlists.FirstAsync()).Id;
                var sut = new SongService(actContext, Utils.Mapper);
                var key = (await actContext.Users.FirstAsync()).ApiKey;

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetSongsByPlaylistIdAsync(playlistId, true, key));
            }

        }
    }
}
