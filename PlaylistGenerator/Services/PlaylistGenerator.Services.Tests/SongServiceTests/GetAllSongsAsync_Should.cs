﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.SongServiceTests
{
    [TestClass]
    public class GetAllSongsAsync_Should
    {
        [TestMethod]
        public async Task ReturnAllSongsCorrectly()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnAllSongsCorrectly));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var sut = new SongService(actContext, Utils.Mapper);
                var expectedSongs = await actContext.Songs.Take(sut.SongsPerPage).ToListAsync();

                //Act
                var result = await sut.GetAllSongsAsync();
                var actualSongs = result.ToList();

                //Assert
                Assert.AreEqual(expectedSongs.Count, actualSongs.Count);

                for (int i = 0; i < actualSongs.Count; i++)
                {
                    Assert.AreEqual(expectedSongs[i].Id, actualSongs[i].Id);
                    Assert.AreEqual(expectedSongs[i].DeezerId, actualSongs[i].DeezerId);
                    Assert.AreEqual(expectedSongs[i].Name, actualSongs[i].Name);
                    Assert.AreEqual(expectedSongs[i].ArtistId, actualSongs[i].ArtistId);
                    Assert.AreEqual(expectedSongs[i].AlbumId, actualSongs[i].AlbumId);
                    Assert.AreEqual(expectedSongs[i].GenreId, actualSongs[i].GenreId);
                    Assert.AreEqual(expectedSongs[i].SongURL, actualSongs[i].SongURL);
                }
            }
        }

        [TestMethod]
        public async Task ReturnEmptyCollection_When_NoSongsInDb()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnEmptyCollection_When_NoSongsInDb));

            var context = new PlaylistGeneratorDbContext(options);
            var sut = new SongService(context, Utils.Mapper);

            //Act
            var result = await sut.GetAllSongsAsync();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count());

        }

        [TestMethod]
        public async Task ReturnSongs_When_ApiKeyValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnSongs_When_ApiKeyValid));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("user", "name"));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                (await arrangeContext.Users.FirstAsync()).ApiKey = Guid.NewGuid();
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new SongService(actContext, Utils.Mapper);
                var expectedSongs = await actContext.Songs.Take(sut.SongsPerPage).ToListAsync();

                //Act
                await sut.GetAllSongsAsync(1, true, key);
                var actualSongs = await actContext.Songs.ToListAsync();

                //Assert
                Assert.AreEqual(expectedSongs.Count, actualSongs.Count);

                for (int i = 0; i < actualSongs.Count; i++)
                {
                    Assert.AreEqual(expectedSongs[i].Id, actualSongs[i].Id);
                    Assert.AreEqual(expectedSongs[i].DeezerId, actualSongs[i].DeezerId);
                    Assert.AreEqual(expectedSongs[i].Name, actualSongs[i].Name);
                    Assert.AreEqual(expectedSongs[i].ArtistId, actualSongs[i].ArtistId);
                    Assert.AreEqual(expectedSongs[i].AlbumId, actualSongs[i].AlbumId);
                    Assert.AreEqual(expectedSongs[i].GenreId, actualSongs[i].GenreId);
                    Assert.AreEqual(expectedSongs[i].SongURL, actualSongs[i].SongURL);
                }
            }
        }

        [TestMethod]
        public async Task Throw_When_InvalidApiKey()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_InvalidApiKey));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var sut = new SongService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllSongsAsync(1, true, Guid.NewGuid()));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsBanned()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsBanned));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("user", "name"));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsBanned = true;

                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new SongService(actContext, Utils.Mapper);
                var expectedSongs = await actContext.Songs.Take(sut.SongsPerPage).ToListAsync();

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllSongsAsync(1, true, key));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsDeleted));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("user", "name"));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsDeleted = true;

                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var sut = new SongService(actContext, Utils.Mapper);
                var expectedSongs = await actContext.Songs.Take(sut.SongsPerPage).ToListAsync();

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllSongsAsync(1, true, key));
            }
        }
    }
}
