﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.SongServiceTests
{
    [TestClass]
    public class GetSongByIdAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectSong()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectSong));

            using(var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using(var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedSong = await actContext.Songs.FirstAsync();
                var sut = new SongService(actContext, Utils.Mapper);

                //Act
                var actualSong = await sut.GetSongByIdAsync(expectedSong.Id);

                //Assert
                Assert.AreEqual(expectedSong.Id, actualSong.Id);
                Assert.AreEqual(expectedSong.DeezerId, actualSong.DeezerId);
                Assert.AreEqual(expectedSong.Name, actualSong.Name);
                Assert.AreEqual(expectedSong.SongURL, actualSong.SongURL);
                Assert.AreEqual(expectedSong.ArtistId, actualSong.ArtistId);
                Assert.AreEqual(expectedSong.AlbumId, actualSong.AlbumId);
                Assert.AreEqual(expectedSong.GenreId, actualSong.GenreId);
            }
        }

        [TestMethod]
        public async Task Throw_When_SongNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_SongNotFound));

            var context = new PlaylistGeneratorDbContext(options);
            var sut = new SongService(context, Utils.Mapper);

            //Act & Assert
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetSongByIdAsync(Guid.NewGuid()));
        }

        [TestMethod]
        public async Task ReturnSong_When_ApiKeyValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnSong_When_ApiKeyValid));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("user", "name"));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                (await arrangeContext.Users.FirstAsync()).ApiKey = Guid.NewGuid();
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var expectedSong = await actContext.Songs.FirstAsync();
                var sut = new SongService(actContext, Utils.Mapper);

                //Act
                var actualSong = await sut.GetSongByIdAsync(expectedSong.Id, true, key);

                //Assert
                Assert.AreEqual(expectedSong.Id, actualSong.Id);
                Assert.AreEqual(expectedSong.DeezerId, actualSong.DeezerId);
                Assert.AreEqual(expectedSong.Name, actualSong.Name);
                Assert.AreEqual(expectedSong.SongURL, actualSong.SongURL);
                Assert.AreEqual(expectedSong.ArtistId, actualSong.ArtistId);
                Assert.AreEqual(expectedSong.AlbumId, actualSong.AlbumId);
            }
        }

        [TestMethod]
        public async Task Throw_When_InvalidApiKey()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_InvalidApiKey));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedSong = await actContext.Songs.FirstAsync();
                var sut = new SongService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException> (() => sut.GetSongByIdAsync(expectedSong.Id, true, Guid.NewGuid()));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsBanned()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsBanned));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("user", "name"));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsBanned = true;

                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var expectedSong = await actContext.Songs.FirstAsync();
                var sut = new SongService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetSongByIdAsync(expectedSong.Id, true, key));
            }
        }

        [TestMethod]
        public async Task Throw_When_UserWithKeyIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserWithKeyIsDeleted));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Users.AddAsync(Utils.CreateMockUser("user", "name"));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));

                var user = await arrangeContext.Users.FirstAsync();
                user.ApiKey = Guid.NewGuid();
                user.IsDeleted = true;

                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var key = (await actContext.Users.FirstAsync()).ApiKey;
                var expectedSong = await actContext.Songs.FirstAsync();
                var sut = new SongService(actContext, Utils.Mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetSongByIdAsync(expectedSong.Id, true, key));
            }
        }
    }
}
