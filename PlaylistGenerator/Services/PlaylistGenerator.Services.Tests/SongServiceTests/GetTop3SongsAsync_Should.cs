﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.SongServiceTests
{
    [TestClass]
    public class GetTop3SongsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectSongs()
        {
            //ArrangeContext
            var options = Utils.GetOptions(nameof(ReturnCorrectSongs));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId,
                    Rank = 232
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId,
                    Rank = 247
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name3",
                    DeezerId = "DeezerId3",
                    SongURL = "SongUrl3",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId,
                    Rank = 333
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name4",
                    DeezerId = "DeezerId4",
                    SongURL = "SongUrl4",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId,
                    Rank = 878
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name5",
                    DeezerId = "DeezerId5",
                    SongURL = "SongUrl5",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId,
                    Rank = 1124
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedSongs = await actContext.Songs.OrderBy(s => s.Rank).Take(3).ToListAsync();
                var sut = new SongService(actContext, Utils.Mapper);

                //Act
                var result = await sut.GetTop3SongsAsync();
                var actualSongs = result.ToList();

                //Assert
                Assert.AreEqual(expectedSongs.Count, actualSongs.Count);

                for (int i = 0; i < actualSongs.Count; i++)
                {
                    Assert.AreEqual(expectedSongs[i].Id, actualSongs[i].Id);
                    Assert.AreEqual(expectedSongs[i].DeezerId, actualSongs[i].DeezerId);
                    Assert.AreEqual(expectedSongs[i].Name, actualSongs[i].Name);
                    Assert.AreEqual(expectedSongs[i].Rank, actualSongs[i].Rank);
                    Assert.AreEqual(expectedSongs[i].ArtistId, actualSongs[i].ArtistId);
                    Assert.AreEqual(expectedSongs[i].AlbumId, actualSongs[i].AlbumId);
                    Assert.AreEqual(expectedSongs[i].GenreId, actualSongs[i].GenreId);
                    Assert.AreEqual(expectedSongs[i].SongURL, actualSongs[i].SongURL);
                }
            }
        }

        [TestMethod]
        public async Task NotThrow_When_LessThan3SongsInDb()
        {
            //ArrangeContext
            var options = Utils.GetOptions(nameof(NotThrow_When_LessThan3SongsInDb));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId,
                    Rank = 232
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId,
                    Rank = 247
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var expectedSongs = await actContext.Songs.OrderBy(s => s.Rank).Take(3).ToListAsync();
                var sut = new SongService(actContext, Utils.Mapper);

                //Act
                var result = await sut.GetTop3SongsAsync();
                var actualSongs = result.ToList();

                //Assert
                Assert.AreEqual(expectedSongs.Count, actualSongs.Count);

                for (int i = 0; i < actualSongs.Count; i++)
                {
                    Assert.AreEqual(expectedSongs[i].Id, actualSongs[i].Id);
                    Assert.AreEqual(expectedSongs[i].DeezerId, actualSongs[i].DeezerId);
                    Assert.AreEqual(expectedSongs[i].Name, actualSongs[i].Name);
                    Assert.AreEqual(expectedSongs[i].Rank, actualSongs[i].Rank);
                    Assert.AreEqual(expectedSongs[i].ArtistId, actualSongs[i].ArtistId);
                    Assert.AreEqual(expectedSongs[i].AlbumId, actualSongs[i].AlbumId);
                    Assert.AreEqual(expectedSongs[i].GenreId, actualSongs[i].GenreId);
                    Assert.AreEqual(expectedSongs[i].SongURL, actualSongs[i].SongURL);
                }
            }
        }
    }
}
