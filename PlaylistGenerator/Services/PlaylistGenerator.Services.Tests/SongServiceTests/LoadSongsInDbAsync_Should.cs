﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Tests.SongServiceTests
{
    public class LoadSongsInDbAsync_Should
    {
        [TestMethod]
        public async Task NotLoad_When_DbHasSongs()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(NotLoad_When_DbHasSongs));

            using (var arrangeContext = new PlaylistGeneratorDbContext(options))
            {
                await arrangeContext.Artists.AddAsync(Utils.CreateMockArtist("name", 123, 3, 234));
                await arrangeContext.Genres.AddAsync(Utils.CreateMockGenre("genre", 556));

                await arrangeContext.SaveChangesAsync();

                await arrangeContext.Albums.AddAsync(Utils.CreateMockAlbum("album", 234, (await arrangeContext.Artists.FirstAsync()).Id));
                await arrangeContext.SaveChangesAsync();

                var artistId = (await arrangeContext.Artists.FirstAsync()).Id;
                var albumId = (await arrangeContext.Albums.FirstAsync()).Id;
                var genreId = (await arrangeContext.Genres.FirstAsync()).Id;

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name1",
                    DeezerId = "DeezerId1",
                    SongURL = "SongUrl",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.Songs.AddAsync(new Song
                {
                    Name = "Name2",
                    DeezerId = "DeezerId2",
                    SongURL = "SongUrl2",
                    ArtistId = artistId,
                    AlbumId = albumId,
                    GenreId = genreId
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new PlaylistGeneratorDbContext(options))
            {
                var sut = new SongService(actContext, Utils.Mapper);
                int expextedSongCount = 2;

                //Act 
                await sut.LoadSongsInDbAsync();

                //Assert
                Assert.AreEqual(expextedSongCount, actContext.Songs.Count());
            }

        }
    }
}
