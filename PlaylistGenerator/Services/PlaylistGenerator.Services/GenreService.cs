﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Extensions;
using PlaylistGenerator.Services.Models.Genre;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services
{
    public class GenreService : IGenreService
    {
        private readonly PlaylistGeneratorDbContext db;
        private readonly IMapper mapper;
        private readonly string[] genreIds = { "132", "116", "152", "113", "165" };


        /// <summary>
        /// A constructor that creates a new instance of GenreService.
        /// </summary>
        /// <param name="db">A PlaylistGeneratorDbContext dependency injected into the constructor..</param>
        /// <param name="mapper">An IMapper dependency injected into the constructor..</param>
        public GenreService(PlaylistGeneratorDbContext db, IMapper mapper)
        {
            this.db = db;
            this.mapper = mapper;
        }

        /// <summary>
        /// An async method which returns all Genres from the database.
        /// </summary>
        /// <param name="requireApiKey">Whether or not to require an API key for the execution of the method.</param>
        /// <param name="apiKey">The API key to validate if requireApiKey parameter is true.</param>
        /// <returns>
        /// A Task<IEnumerable> collection of GenreServiceModel;
        /// </returns>
        public async Task<IEnumerable<GenreServiceModel>> GetAllGenresAsync(bool requireApiKey = false, Guid? apiKey = null)
        {
            if (requireApiKey)
                await this.ValidateAPIKeyAsync(apiKey, this.db);

            var genres = this.db.Genres;

            if (genres == null)
                return new List<GenreServiceModel>();

            var result = this.mapper.Map<IEnumerable<GenreServiceModel>>(genres);

            return result;
        }

        /// <summary>
        /// An async method which returns a Genre from the database.
        /// </summary>
        /// <param name="genreId">The Id of the Genre to return.</param>
        /// <param name="requireApiKey">Whether or not to require an Api key for the execution of the method.</param>
        /// <param name="apiKey">The API key to validate if requireApiKey parameter is true.</param>
        /// <returns></returns>
        public async Task<GenreServiceModel> GetGenreByIdAsync(Guid genreId, bool requireApiKey = false, Guid? apiKey = null)
        {
            if (requireApiKey)
                await this.ValidateAPIKeyAsync(apiKey, this.db);

            var genre = await this.db.Genres.FindAsync(genreId);

            if (genre == null)
                throw new ArgumentException("Genre Not Found.");

            var genreServiceModel = this.mapper.Map<GenreServiceModel>(genre);

            return genreServiceModel;
        }

        /// <summary>
        /// An async method which returns all Genres that a Playlist contains.
        /// </summary>
        /// <param name="id">The id of the Playlist containing the Genres.</param>
        /// <param name="requireApiKey">Whether or not to require an API key for the execution of the method.</param>
        /// <param name="apiKey">The API key to validate if requireApiKey parameter is true.</param>
        /// <returns>
        /// A Task<IEnumerable> collection of GenreServiceModel; All Genres with PlaylistId equal to id parameter.
        /// </returns>
        public async Task<IEnumerable<GenreServiceModel>> GetGenresByPlaylistId(Guid id, bool requireApiKey = false, Guid? apiKey = null)
        {
            if (requireApiKey)
                await this.ValidateAPIKeyAsync(apiKey, this.db);

            if (!db.Playlists.Any(x => x.Id == id))
            {
                throw new ArgumentException("There is no playlist with such Id in the database.");
            }

            var genresFromThisPlaylistServiceModels = await db.PlaylistsGenres
                .Where(x => x.PlaylistId == id)
                .Select(x => db.Genres.First(y => y.Id == x.GenreId))
                .ToListAsync();

            return this.mapper.Map<IEnumerable<GenreServiceModel>>(genresFromThisPlaylistServiceModels);
        }

        /// <summary>
        /// An async method which loads Genres from Deezer API into the database.
        /// </summary>
        /// <returns>
        /// A Task representing an asyncronous operation which fills the database with Genres.
        /// </returns>
        public async Task LoadGenresInDbAsync()
        {
            if (this.DatabaseContainsGenres())
                return;

            foreach (var id in this.genreIds)
            {
                var genre = await this.GetGenreFromDeezerAPIAsync(id);
                await this.db.Genres.AddAsync(genre);
            }

            await this.db.SaveChangesAsync();
        }

        /// <summary>
        /// An async method which updates the data of Genres in the database by sending requests to Deezer Api.
        /// </summary>
        /// <param name="requireApiKey">Whether or not to require an API key for the execution of the method.</param>
        /// <param name="apiKey">The Api key to validate if requireApiKey parameter is true.</param>
        /// <returns>
        /// A Task representing an asyncronous operation which updates the data of the Genres in the database.
        /// </returns>
        public async Task UpdateGenresInDbAsync(bool requireApiKey = false, Guid? apiKey = null)
        {
            if (requireApiKey)
            {
                await this.CheckIfUserIsAdminByApiKey(apiKey, this.db);
            }

            if (!this.DatabaseContainsGenres())
            {
                throw new ArgumentException("There are no genres to update in the Database.");
            }


            Genre currentGenre;

            foreach (var genre in this.db.Genres)
            {
                currentGenre = await this.GetGenreFromDeezerAPIAsync(genre.DeezerId);
                genre.Name = currentGenre.Name;
                genre.PictureURL = currentGenre.PictureURL;
            }

            await this.db.SaveChangesAsync();
        }

        private bool DatabaseContainsGenres()
        {
            if (this.db.Genres == null || this.db.Genres.Count() == 0)
                return false;

            return true;
        }

        private async Task<Genre> GetGenreFromDeezerAPIAsync(string genreId)
        {
            string url = $"https://api.deezer.com/genre/{genreId}";

            string jsonGenre = await this.GetJsonStreamFromUrlAsync(url);
            Genre genre = JsonConvert.DeserializeObject<Genre>(jsonGenre);

            return genre;
        }
    }
}
