﻿using PlaylistGenerator.Services.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface IPlaylistService : IService
    {
        Task<PlaylistServiceModel> CreatePlaylistAsync(PlaylistServiceModel playlistServiceModel);

        Task<bool> DeletePlaylistAsync(Guid playlistId, bool isApiKeyRequired = false, Guid? userApiKey = null);

        IEnumerable<PlaylistServiceModel> FilterByGenre(IEnumerable<PlaylistServiceModel> collectionToFilter, IEnumerable<Guid> genresIds);

        IEnumerable<PlaylistServiceModel> FilterByRange(IEnumerable<PlaylistServiceModel> collectionToFilter, string filterMethod, int min, int max);

        Task<bool> GeneratePlaylistAsync(Guid playlistId, int durationTravel, IEnumerable<Guid> genresToUse, bool IsTopTracksOptionEnabled, bool IsTracksFromSameArtistEnabled);

        Task<IEnumerable<PlaylistServiceModel>> GetAllPlaylistsAsync(bool isApiKeyRequired = false, Guid? userApiKey = null, bool isAdmin = false);

        Task<int> GetDurationTravelAsync(string queryFirst, string querySecond);

        Task<PlaylistServiceModel> GetPlaylistByIdAsync(Guid playlistId, bool isApiKeyRequired = false, Guid? userApiKey = null);

        Task<IEnumerable<PlaylistServiceModel>> GetPlaylistsByNameContainsSubstring(string substring);

        Task<IEnumerable<PlaylistServiceModel>> GetPlaylistsByUserIdAsync(Guid userId);

        IEnumerable<PlaylistServiceModel> Sort(IEnumerable<PlaylistServiceModel> collectionToFilter, string sortMethod, string sortOrder);

        Task SwapUnlistedStatusByIdAsync(Guid id);

        Task<PlaylistServiceModel> UpdatePlaylistAsync(PlaylistServiceModel playlist, bool isApiKeyRequired = false, Guid? userApiKey = null);

        Task<bool> UpdatePlaylistDurationTravelAsync(Guid id, int durationTravel);

        Task<bool> UpdatePlaylistImageAsync(Guid id);
    }
}
