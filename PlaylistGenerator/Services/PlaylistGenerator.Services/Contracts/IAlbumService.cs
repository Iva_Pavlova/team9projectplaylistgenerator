﻿using PlaylistGenerator.Services.Models.Album;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface IAlbumService : IService
    {
        Task<AlbumServiceModel> GetAlbumByIdAsync(Guid albumId, bool requireApiKey = false, Guid? apiKey = null);
    }
}
