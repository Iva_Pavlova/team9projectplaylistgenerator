﻿using System;

namespace PlaylistGenerator.Services.Contracts
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
