﻿using PlaylistGenerator.Services.Models.Syncronization;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface ISyncronizationService : IService
    {
        Task SyncronizeGenresAndSongsAsync(bool requireApiKey = false, Guid? apiKey = null);

        Task<SyncronizationServiceModel> GetLastSyncronizationAsync(bool requireApiKey = false, Guid? apiKey = null);
    }
}
