﻿using PlaylistGenerator.Services.Models.Artist;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface IArtistService : IService
    {
        Task<ArtistServiceModel> GetArtistByIdAsync(Guid artistId, bool requireApiKey = false, Guid? apiKey = null);
    }
}
