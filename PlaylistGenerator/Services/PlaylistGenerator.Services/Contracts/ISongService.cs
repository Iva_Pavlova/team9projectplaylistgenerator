﻿using PlaylistGenerator.Services.Models.Song;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface ISongService : IService
    {
        int TotalPages { get; }

        int SongsPerPage { get; }

        Task<IEnumerable<SongServiceModel>> GetAllSongsAsync(int page = 1, bool requireApiKey = false, Guid? apiKey = null);

        Task<IEnumerable<SongServiceModel>> GetTop3SongsAsync();

        Task<SongServiceModel> GetSongByIdAsync(Guid songId, bool requireApiKey = false, Guid? apiKey = null);

        Task<IEnumerable<SongServiceModel>> GetSongsByPlaylistIdAsync(Guid playlistId, bool requireApiKey = false, Guid? apiKey = null);

        Task UpdateSongsInDbAsync(bool requireApiKey = false, Guid? apiKey = null);

        Task LoadSongsInDbAsync();
    }
}
