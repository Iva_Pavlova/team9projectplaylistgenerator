﻿using PlaylistGenerator.Services.Models.Genre;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface IGenreService : IService
    {
        Task<IEnumerable<GenreServiceModel>> GetAllGenresAsync(bool requireApiKey = false, Guid? apiKey = null);

        Task<GenreServiceModel> GetGenreByIdAsync(Guid genreId, bool requireApiKey = false, Guid? apiKey = null);

        Task UpdateGenresInDbAsync(bool requireApiKey = false, Guid? apiKey = null);

        Task<IEnumerable<GenreServiceModel>> GetGenresByPlaylistId(Guid id, bool requireApiKey = false, Guid? apiKey = null);
    }
}
