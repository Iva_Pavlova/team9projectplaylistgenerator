﻿using PlaylistGenerator.Services.Models;
using PlaylistGenerator.Services.Models.Home;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface IHomeService : IService
    {
        bool SendEmail(EmailSendServiceModel model);

        Task<IEnumerable<NewsServiceModel>> GetMusicNews();
    }
}
