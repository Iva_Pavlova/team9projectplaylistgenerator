﻿using Microsoft.AspNetCore.Identity;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Models.User;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface IUserService : IService
    {
        Task<bool> DeleteUserAsync(Guid userId, bool isApiKeyRequired = false, Guid? userApiKey = null);

        Task<IEnumerable<UserServiceModel>> GetAllUsersAsync(bool isAdmin = false, int page = 1);

        Task<UserServiceModel> CreateUserAsync(CreateUserServiceModel user, UserManager<User> userManager);

        Task<UserServiceModel> GetUserByApiKeyAsync(Guid? apiKey = null);

        Task<UserServiceModel> GetUserByEmailAsync(string email);

        Task<UserServiceModel> GetUserByIdAsync(Guid id, bool isApiKeyRequired = false, Guid? userApiKey = null);

        Task SwapUserBanStatusByIdAsync(Guid id);

        Task<UserServiceModel> UpdateUserAsync(UserServiceModel user, bool isApiKeyRequired = false, Guid? userApiKey = null);

        Task<bool> UpdateUserImageAsync(Guid id);

        Task<Guid?> UpdateUserApiKeyAsync(Guid id);

        int GetTotalUsersCount();

        int GetPageCountSizing();
    }
}
