﻿using PlaylistGenerator.Services.Contracts;
using System;

namespace PlaylistGenerator.Services.Providers
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime()
        {
            return DateTime.UtcNow;
        }
    }
}
