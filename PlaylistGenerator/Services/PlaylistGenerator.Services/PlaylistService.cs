﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Data.Models.BingApiObjects;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Extensions;
using PlaylistGenerator.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services
{
    public class PlaylistService : IPlaylistService
    {
        private readonly PlaylistGeneratorDbContext db;
        private readonly IMapper mapper;
        private readonly IDateTimeProvider dateTimeProvider;


        /// <summary>
        /// A constructor that instantiates a new instance of PlaylistService.
        /// </summary>
        /// <param name="db">A PlaylistGeneratorDbContext dependency injected into the constructor..</param>
        /// <param name="mapper">An IMapper dependency injected into the constructor..</param>
        /// <param name="dateTimeProvider">An IDateTimeProvider dependency injected into the constructor..</param>
        public PlaylistService(PlaylistGeneratorDbContext db, IMapper mapper, IDateTimeProvider dateTimeProvider)
        {
            this.db = db;
            this.mapper = mapper;
            this.dateTimeProvider = dateTimeProvider;
        }

        /// <summary>
        /// An async method that creates a playlist based on the information received in a PlaylistServiceModel that comes as a method parameter.
        /// </summary>
        /// <param name="playlistServiceModel">A PlaylistServiceModel that keeps the information of the playlist to be created.</param>
        /// <returns>A task that represents a PlaylistServiceModel that holds all information on the newly created user, including the new Guid id.</returns>
        public async Task<PlaylistServiceModel> CreatePlaylistAsync(PlaylistServiceModel playlistServiceModel)
        {
            ValidateIfNameIsNullOrEmpty(playlistServiceModel.Name);

            ValidateIfAuthorExistsInDb(playlistServiceModel.UserId);
            ValidateIfAuthorIsDeleted(playlistServiceModel.UserId);

            playlistServiceModel.DateCreated = dateTimeProvider.GetDateTime();
            playlistServiceModel.DateModified = dateTimeProvider.GetDateTime();

            Playlist playlist = this.mapper.Map<Playlist>(playlistServiceModel);
            await this.db.Playlists.AddAsync(playlist);

            await db.SaveChangesAsync();

            var playlistJustCreated = this.db.Playlists.First(x => x.Name == playlistServiceModel.Name);

            playlistServiceModel.Id = playlistJustCreated.Id;
            playlistServiceModel.DateCreated = playlistJustCreated.DateCreated;
            playlistServiceModel.DateModified = playlistJustCreated.DateModified;
            playlistServiceModel.IsDeleted = playlistJustCreated.IsDeleted;
            playlistServiceModel.IsUnlisted = playlistJustCreated.IsUnlisted;

            return playlistServiceModel;
        }

        /// <summary>
        /// An async method that deletes a playlist if a playlist with such id (from the parameter of the method) exists.
        /// </summary>
        /// <param name="playlistId">A Guid that is the id of the playlist to delete.</param>
        /// <param name="isApiKeyRequired">A boolean that reveals if the method is called from an ApiController and if API Key is needed to proceed.</param>
        /// <param name="userApiKey">A Guid? that is the API Key of the caller if method is called from an ApiController.</param>
        /// <returns>A task that represents a boolean that reveals if the action is successfully achieved.</returns>
        public async Task<bool> DeletePlaylistAsync(Guid playlistId, bool isApiKeyRequired = false, Guid? userApiKey = null)
        {
            ValidatePlaylistId(playlistId);
            ValidateIfPlaylistIsUnlisted(playlistId);
            ValidateIfPlaylistIsDeleted(playlistId);

            User userWithApiKey = null;

            if (isApiKeyRequired)
            {
                userWithApiKey = await this.ValidateAPIKeyAsync(userApiKey, this.db);
                ValidateIfUserWithThisApiKeyIsTheAuthorOfPlaylist(playlistId, userWithApiKey);
            }

            var playlistInDb = await this.db.Playlists.FindAsync(playlistId);

            playlistInDb.DateModified = dateTimeProvider.GetDateTime();
            playlistInDb.IsDeleted = true;

            await db.SaveChangesAsync();
            return true;
        }

        // PLAYLIST GENERATOR ALGORITHM
        /// <summary>
        /// An async method that finds an already created, blank playlist by a playlistId that comes a parameter.
        /// Then it generates an algorithm that fills up the playlist with songs until the duration of the playlist and the duration of the travel have a difference of less than 5 minutes.
        /// </summary>
        /// <param name="playlistId">A Guid that is the id of the playlist to fill with songs.</param>
        /// <param name="durationTravel">An integer that is the duration of the travel.</param>
        /// <param name="genresIdsToUse">An IEnumerable<Guid> collection of all ids of genres to be used for generating songs to fill the playlist.</param>
        /// <param name="IsTopTracksOptionEnabled">A boolean that reveals if the user wants to have the "use top tracks" option enabled.</param>
        /// <param name="IsTracksFromSameArtistEnabled">A boolean that reveals if the user wants to have the "tracks from same artist" enabled.</param>
        /// <returns>A task that represents a boolean that reveals if the action is successfully achieved.</returns>
        public async Task<bool> GeneratePlaylistAsync(Guid playlistId, int durationTravel, IEnumerable<Guid> genresIdsToUse, bool IsTopTracksOptionEnabled, bool IsTracksFromSameArtistEnabled)
        {
            ValidatePlaylistId(playlistId);
            ValidateDurationTravel(durationTravel);

            Random random = new Random();
            HashSet<Guid> listArtistsIdsAlreadyUsed = new HashSet<Guid>();

            int durationDubbedCurrent = 0;

            // If for some reason we don't receive any Genres by the User - use all available Genres.
            if (genresIdsToUse == null || genresIdsToUse.Count() == 0)
            {
                genresIdsToUse = db.Genres.Select(x => x.Id);
            }

            while (true)
            {
                // If Duration Travel - 5 min <= Duration Playlist Current < Duration Travel + 5 min => break.
                if (durationDubbedCurrent >= durationTravel - (5 * 60) && durationDubbedCurrent <= durationTravel + (5 * 60))
                {
                    break;
                }

                Guid randomGenreId = genresIdsToUse.Skip(random.Next(0, genresIdsToUse.Count())).First();

                IEnumerable<Guid> collectionSongsIdsOfThisGenre = new List<Guid>();

                // Take all songs from the random Genre and if "Is Top Tracks Option Enabled" => take only those of Rank 100.000.
                if (IsTopTracksOptionEnabled)
                {
                    collectionSongsIdsOfThisGenre = db.Songs.Where(x => x.GenreId == randomGenreId).Where(x => x.Rank <= 500000).Select(x => x.Id);
                }
                else
                {
                    collectionSongsIdsOfThisGenre = db.Songs.Where(x => x.GenreId == randomGenreId).Select(x => x.Id);
                }

                // Take a random Song Id - skipping a random count of songs.
                Guid randomSongId = collectionSongsIdsOfThisGenre.Skip(random.Next(0, collectionSongsIdsOfThisGenre.Count())).First();
                Song randomSong = db.Songs.First(x => x.Id == randomSongId);

                // If "Is Tracks From Same Artist Enabled" and the current list of "used" Artists contains the author of the randomly chosen Song => continue.
                if (IsTracksFromSameArtistEnabled && listArtistsIdsAlreadyUsed.Any(x => x == randomSong.ArtistId))
                {
                    continue;
                }

                // If the random Genre does not exist in the Playlist<>Genre table => Add it.
                if (!db.PlaylistsGenres.Where(x => x.PlaylistId == playlistId).Any(b => b.GenreId == randomGenreId))
                {
                    db.PlaylistsGenres.Add(new PlaylistGenre
                    {
                        PlaylistId = playlistId,
                        GenreId = randomGenreId,
                    });
                    await db.SaveChangesAsync();
                }

                // If the random Song does not exist in the Playlist<>Song table => Add it.
                if (!db.PlaylistsSongs.Where(x => x.PlaylistId == playlistId).Any(b => b.SongId == randomSongId))
                {
                    db.PlaylistsSongs.Add(new PlaylistSong
                    {
                        PlaylistId = playlistId,
                        SongId = randomSongId,
                    });
                    await db.SaveChangesAsync();

                    durationDubbedCurrent += randomSong.Duration;
                    listArtistsIdsAlreadyUsed.Add(randomSong.ArtistId);
                }
            }

            Playlist playlistToEdit = await db.Playlists.FirstAsync(x => x.Id == playlistId);

            // If the playlist does not have any Description => add a "default" one.
            if (playlistToEdit.Description == null)
            {
                int countSongsInPlaylist = db.PlaylistsSongs.Where(x => x.PlaylistId == playlistId).Count();
                playlistToEdit.Description = $"'{playlistToEdit.Name}' consists of {countSongsInPlaylist} songs that will make your journey from point A to point B sound like {playlistToEdit.DurationTravel / 60} minutes in Heaven...";
                await db.SaveChangesAsync();
            }

            return true;
        }

        /// <summary>
        /// An async method that gets all playlists within the database that are not deleted.
        /// </summary>
        /// <param name="isApiKeyRequired">A boolean that reveals if the method is called from an ApiController and if API Key is needed to proceed.</param>
        /// <param name="userApiKey">A Guid? that is the API Key of the caller if method is called from an ApiController.</param>
        /// <param name="isAdmin">A boolean that reveals if the method is called from an Admin or not.</param>
        /// <returns>A task that represents a collection of PlaylistServiceModels with all playlists in the database that are not deleted.</returns>
        public async Task<IEnumerable<PlaylistServiceModel>> GetAllPlaylistsAsync(bool isApiKeyRequired = false, Guid? userApiKey = null, bool isAdmin = false)
        {
            if (isApiKeyRequired)
                await this.ValidateAPIKeyAsync(userApiKey, this.db);

            var playlists = await this.db.Playlists
                .Where(x => x.IsDeleted == false)
                .Where(x => x.DurationTravel != 0)
                .Include(x => x.User)
                .Include(x => x.Genres)
                .Include(x => x.Songs)
                .ThenInclude(playlistSong => playlistSong.Song)
                .ToListAsync();

            if (!isAdmin)
            {
                playlists = playlists.Where(x => x.IsUnlisted == false).ToList();
            }

            var result = this.mapper.Map<IEnumerable<PlaylistServiceModel>>(playlists);
            return result;
        }

        /// <summary>
        /// An async method that gets the duration of a travel based on the Point A input and Point B input that comes from the user as strings.
        /// </summary>
        /// <param name="queryFirst">A string that is the address of the Point A (start) of the travel.</param>
        /// <param name="querySecond">A string that is the address of the Point B (destination) of the travel.</param>
        /// <returns>A task that represents an integer - the duration of travel based on strings that represent addresses of Point A and Point B.</returns>
        public async Task<int> GetDurationTravelAsync(string queryFirst, string querySecond)
        {
            ValidateIfNameIsNullOrEmpty(queryFirst);
            ValidateIfNameIsNullOrEmpty(querySecond);

            List<double> coordinatesFirstPoint = await GetLocationByQueryAsync(queryFirst);
            List<double> coordinatesSecondPoint = await GetLocationByQueryAsync(querySecond);

            if (coordinatesFirstPoint == null || coordinatesSecondPoint == null)
            {
                throw new ArgumentException("You did not get coordinates.");
            }

            int duration = await GetDurationByCoordinatesAsync(coordinatesFirstPoint, coordinatesSecondPoint) * 60 /*seconds*/;

            ValidateDurationTravel(duration);

            return duration;
        }

        /// <summary>
        /// An async method that gets a playlist by an id that comes as a method parameter.
        /// </summary>
        /// <param name="playlistId">A Guid that is the id of the playlist to get from the database.</param>
        /// <param name="isApiKeyRequired">A boolean that reveals if the method is called from an ApiController and if API Key is needed to proceed.</param>
        /// <param name="userApiKey">A Guid? that is the API Key of the caller if method is called from an ApiController.</param>
        /// <returns>A task that represents a PlaylistServiceModel of the playlist with an id that comes as a method parameter.</returns>
        public async Task<PlaylistServiceModel> GetPlaylistByIdAsync(Guid playlistId, bool isApiKeyRequired = false, Guid? userApiKey = null)
        {
            if (isApiKeyRequired)
                await this.ValidateAPIKeyAsync(userApiKey, this.db);

            ValidatePlaylistId(playlistId);
            ValidateIfPlaylistIsDeleted(playlistId);

            Playlist playlist = await this.db.Playlists
                .Include(x => x.User)
                .Include(x => x.Genres)
                .Include(x => x.Songs)
                .ThenInclude(playlistSong => playlistSong.Song)
                .FirstOrDefaultAsync(p => p.Id == playlistId);

            PlaylistServiceModel playlistServiceModel = this.mapper.Map<PlaylistServiceModel>(playlist);

            return playlistServiceModel;
        }

        /// <summary>
        /// An async method that gets all playlists with names that contain a substring that comes as a method parameter.
        /// </summary>
        /// <param name="substring">A string that is the basis to search for playlists in the database that have names containing this substring.</param>
        /// <returns>A task that represents a collection of PlaylistServiceModels of all playlists that have names which contain the substring that comes as a method parameter.</returns>
        public async Task<IEnumerable<PlaylistServiceModel>> GetPlaylistsByNameContainsSubstring(string substring)
        {
            IEnumerable<Playlist> playlistsDataModels = await db.Playlists
                    .Where(x => x.IsDeleted == false)
                    .Where(x => x.IsUnlisted == false)
                    .Where(x => x.DurationTravel != 0)
                    .Include(x => x.Songs)
                    .ThenInclude(playlistSong => playlistSong.Song)
                    .ToListAsync();

            if (substring != null && db.Playlists.Any(x => x.Name.Contains(substring)))
            {
                playlistsDataModels = playlistsDataModels.Where(x => x.Name.Contains(substring));
                return this.mapper.Map<IEnumerable<PlaylistServiceModel>>(playlistsDataModels).ToList();
            }

            return null;
        }

        /// <summary>
        /// An async method that gets all playlists of a user with a certain userId that comes as a method parameter.
        /// </summary>
        /// <param name="userId">A Guid that is the identity of a user in the database.</param>
        /// <returns>A task that represents a collection of PlaylistServiceModels that are created by a user with the userId that comes as a method parameter.</returns>
        public async Task<IEnumerable<PlaylistServiceModel>> GetPlaylistsByUserIdAsync(Guid userId)
        {
            if (!db.Users.Any(x => x.Id == userId))
            {
                throw new ArgumentException("You are trying to get the playlists of a user with an Id that does not exist in the database.");
            }

            if (!db.Playlists.Any(x => x.UserId == userId))
            {
                return new List<PlaylistServiceModel>();
            }
            else
            {
                var playlistsOfThisUser = await db.Playlists
                    .Where(x => x.UserId == userId)
                    .Where(x => x.IsDeleted == false)
                    .Where(x => x.DurationTravel != 0)
                    .Include(x => x.Genres)
                    .Include(x => x.Songs)
                    .ThenInclude(playlistSong => playlistSong.Song)
                    .ToListAsync();

                return this.mapper.Map<IEnumerable<PlaylistServiceModel>>(playlistsOfThisUser);
            }
        }

        /// <summary>
        /// A method that filters a collection of a PlaylistServiceModels by genres.
        /// </summary>
        /// <param name="collectionToFilter">An IEnumerable<PlaylistServiceModel> that is the collection to filter by genre.</param>
        /// <param name="genresIds">An IEnumerable<Guid> that is the collection the Ids of genres to filter by.</param>
        /// <returns>A task that represents a collection of PlaylistServiceModels filtered by genre.</returns>
        public IEnumerable<PlaylistServiceModel> FilterByGenre(IEnumerable<PlaylistServiceModel> collectionToFilter, IEnumerable<Guid> genresIds)
        {
            if (genresIds == null || genresIds.Count() == 0)
            {
                return collectionToFilter;
            }
            else
            {
                List<PlaylistServiceModel> collectionToReturn = new List<PlaylistServiceModel>();

                foreach (PlaylistServiceModel playlist in collectionToFilter)
                {
                    bool areGenresIncludedInPlaylist = false;

                    foreach (Guid genreId in genresIds)
                    {
                        if (db.PlaylistsGenres.Any(x => x.GenreId == genreId && x.PlaylistId == playlist.Id))
                        {
                            areGenresIncludedInPlaylist = true;
                        }
                        else
                        {
                            areGenresIncludedInPlaylist = false;
                            break;
                        }
                    }

                    if (areGenresIncludedInPlaylist)
                    {
                        collectionToReturn.Add(playlist);
                    }
                }

                return collectionToReturn;
            }
        }

        /// <summary>
        /// FA method that filters a collection of PlaylistServiceModels by a filterMethod and a range of min value and max value.
        /// </summary>
        /// <param name="collectionToFilter">An IEnumerable<PlaylistServiceModel> that is the collection to filter by range.</param>
        /// <param name="filterMethod">A string that represents the filter method - duration or rank.</param>
        /// <param name="min">An integer that represents the minimum range of filtering.</param>
        /// <param name="max">An integer that represents the maximum range of filtering.</param>
        /// <returns>A task that represents a collection of PlaylistServiceModels filtered by range.</returns>
        public IEnumerable<PlaylistServiceModel> FilterByRange(IEnumerable<PlaylistServiceModel> collectionToFilter, string filterMethod, int min, int max)
        {
            if (filterMethod.ToLowerInvariant() == "duration")
            {
                int durationMinutesMin = Math.Min(min /*hours*/ * 60 /*minutes*/ * 60 /*seconds*/, max * 60 /*minutes*/ * 60 /*seconds*/);
                int durationMinutesMax = Math.Max(min /*hours*/ * 60 /*minutes*/ * 60 /*seconds*/, max * 60 /*minutes*/ * 60 /*seconds*/);

                if ((durationMinutesMin < 10 /*minutes*/ * 60 /*seconds*/) || (durationMinutesMin > 24 /*hours*/ * 60 /*minutes*/ * 60 /*seconds*/))
                {
                    durationMinutesMin = 10 /*minutes*/ * 60 /*seconds*/;
                }
                if ((durationMinutesMax < 10 /*minutes*/ * 60 /*seconds*/) || (durationMinutesMax > 24 /*hours*/ * 60 /*minutes*/ * 60 /*seconds*/))
                {
                    durationMinutesMax = 24 /*hours*/ * 60 /*minutes*/ * 60 /*seconds*/;
                }

                collectionToFilter = collectionToFilter.Where(x => x.DurationTravel >= durationMinutesMin && x.DurationTravel <= durationMinutesMax).ToList();
            }
            else if (filterMethod.ToLowerInvariant() == "rank")
            {
                int rankMin = Math.Min(min, max);
                int rankMax = Math.Max(min, max);

                if (rankMin < 100000 || rankMin > 1000000)
                {
                    rankMin = 100000;
                }
                if (rankMax < 100000 || rankMax > 1000000)
                {
                    rankMax = 1000000;
                }

                collectionToFilter = collectionToFilter.Where(x => x.Rank >= rankMin && x.Rank <= rankMax).ToList();
            }
            else
            {
                throw new InvalidOperationException("FilterRange() method only supports filtering by 'duration' or by 'rank'");
            }

            return collectionToFilter;
        }

        /// <summary>
        /// A method that sorts a collection of PlaylistServiceModels by a sortMethod and sortOrder that come as parameters of the method.
        /// </summary>
        /// <param name="collectionToFilter">An IEnumerable<PlaylistServiceModel> that is the collection to filter.</param>
        /// <param name="sortMethod">A string that represents the sort method - rank, duration or name.</param>
        /// <param name="sortOrder">A string that represents the sort order - asc or desc.</param>
        /// <returns>A task that represents a collection of PlaylistServiceModels sorted.</returns>
        public IEnumerable<PlaylistServiceModel> Sort(IEnumerable<PlaylistServiceModel> collectionToFilter, string sortMethod, string sortOrder)
        {
            if (collectionToFilter == null || collectionToFilter.Count() == 0)
            {
                return collectionToFilter;
            }

            if (sortMethod.ToLowerInvariant() == "duration")
            {
                if (sortOrder.ToLowerInvariant() == "asc")
                {
                    collectionToFilter = collectionToFilter.OrderBy(x => x.DurationTravel);
                }
                else
                {
                    collectionToFilter = collectionToFilter.OrderByDescending(x => x.DurationTravel);
                }
            }
            else if (sortMethod.ToLowerInvariant() == "rank")
            {
                if (sortOrder.ToLowerInvariant() == "asc")
                {
                    collectionToFilter = collectionToFilter.OrderBy(x => x.Rank);
                }
                else
                {
                    collectionToFilter = collectionToFilter.OrderByDescending(x => x.Rank);
                }
            }
            else if (sortMethod.ToLowerInvariant() == "name")
            {
                if (sortOrder.ToLowerInvariant() == "asc")
                {
                    collectionToFilter = collectionToFilter.OrderBy(x => x.Name);
                }
                else
                {
                    collectionToFilter = collectionToFilter.OrderByDescending(x => x.Name);
                }
            }
            else
            {
                throw new InvalidOperationException("Sort() method only supports sorting by 'duration', 'rank' and 'name'");
            }

            return collectionToFilter;
        }

        /// <summary>
        /// An async method that Swaps the IsUnlisted boolean of a playlist with a certain id.
        /// </summary>
        /// <param name="id">A Guid that is the identity of a playlist in the database.</param>
        public async Task SwapUnlistedStatusByIdAsync(Guid id)
        {
            ValidatePlaylistId(id);
            ValidateIfPlaylistIsDeleted(id);

            Playlist playlistInDb = await db.Playlists.FindAsync(id);

            playlistInDb.IsUnlisted = !playlistInDb.IsUnlisted;
            await db.SaveChangesAsync();
        }

        /// <summary>
        /// An async method that updates a playlist based on the information provided in a PlaylistServiceModel that comes as a method parameter.
        /// </summary>
        /// <param name="playlist">A PlaylistServiceModel that keeps the information of the playlist to be updated.</param>
        /// <param name="isApiKeyRequired">A boolean that reveals if the method is called from an ApiController and if API Key is needed to proceed.</param>
        /// <param name="userApiKey">A Guid? that is the API Key of the caller if method is called from an ApiController.</param>
        /// <returns>A task that represents a PlaylistServiceModel that holds all information of the updated user.</returns>
        public async Task<PlaylistServiceModel> UpdatePlaylistAsync(PlaylistServiceModel playlist, bool isApiKeyRequired = false, Guid? userApiKey = null)
        {
            ValidatePlaylistId(playlist.Id);
            ValidateIfPlaylistIsUnlisted(playlist.Id);
            ValidateIfPlaylistIsDeleted(playlist.Id);

            User user = null;

            if (isApiKeyRequired)
            {
                user = await this.ValidateAPIKeyAsync(userApiKey, this.db);
                ValidateIfUserWithThisApiKeyIsTheAuthorOfPlaylist(playlist.Id, user);
            }

            var playlistInDb = await this.db.Playlists.FindAsync(playlist.Id);

            ValidateIfNameIsNullOrEmpty(playlist.Name);

            playlistInDb.Name = playlist.Name;
            playlistInDb.Description = playlist.Description;
            playlistInDb.DateModified = dateTimeProvider.GetDateTime();

            await db.SaveChangesAsync();

            var playlistInDbUpdated = await this.db.Playlists
                .Include(x => x.User)
                .Include(x => x.Genres)
                .Include(x => x.Songs)
                .ThenInclude(playlistSong => playlistSong.Song)
                .FirstOrDefaultAsync(p => p.Id == playlist.Id);

            return this.mapper.Map<PlaylistServiceModel>(playlistInDbUpdated);
        }

        /// <summary>
        /// An async method that receives the id of a playlist and updates its playlist duration if a playlist with such id is found in the database.
        /// </summary>
        /// <param name="id">A Guid that is the identity of a playlist in the database.</param>
        /// <param name="durationTravel">An integer that is the duration of travel in seconds.</param>
        /// <returns>A task that represents a boolean that reveals if the action is successfully achieved or not.</returns>
        public async Task<bool> UpdatePlaylistDurationTravelAsync(Guid id, int durationTravel)
        {
            ValidatePlaylistId(id);
            ValidateIfPlaylistIsDeleted(id);

            ValidateDurationTravel(durationTravel);

            var playlist = await this.db.Playlists.FindAsync(id);

            playlist.DurationTravel = durationTravel;
            await db.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// An async method that receives the id of a playlist and sets the image path of the playlist to "/directory/{id}.jpg".
        /// </summary>
        /// <param name="id">A Guid which is the identity of the playlist in the database.</param>
        /// <returns>A task that represents a boolean that reveals if the action is successfully achieved or not.</returns>
        public async Task<bool> UpdatePlaylistImageAsync(Guid id)
        {
            ValidatePlaylistId(id);
            ValidateIfPlaylistIsDeleted(id);

            var playlist = await this.db.Playlists.FindAsync(id);

            playlist.Image = $"/images/playlists/{id}.jpg";
            await db.SaveChangesAsync();

            return true;
        }


        private async Task<string> GatherDataAsync(string url)
        {
            string result = string.Empty;

            var client = new HttpClient();

            for (int i = 0; i < 10; i++)
            {
                try
                {
                    var response = await client.GetAsync(url);
                    response.EnsureSuccessStatusCode();
                    result = await response.Content.ReadAsStringAsync();
                    break;
                }
                catch (Exception)
                {
                    Thread.Sleep(500);
                }
            }

            return result;
        }

        private async Task<int> GetDurationByCoordinatesAsync(List<double> coordinatesFirstPoint, List<double> coordinatesSecondPoint)
        {
            string apiKey = $"Ah23AFfxih6bgoSaVF8nmoI_GVIKvpR4Fah58v4rMWAC7aZeOGkjIqANdSR1LT-q";
            string url = $"https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix?origins={coordinatesFirstPoint[0]},{coordinatesFirstPoint[1]}&destinations={coordinatesSecondPoint[0]},{coordinatesSecondPoint[1]}&travelMode=driving&key={apiKey}";

            string result = await GatherDataAsync(url);

            if (!string.IsNullOrEmpty(result))
            {
                DistanceRoot rootLocation = JsonConvert.DeserializeObject<DistanceRoot>(result);
                int duration = Convert.ToInt32(Math.Round(rootLocation.resourceSets.SelectMany(x => x.resources.SelectMany(x => x.results.Select(x => x.travelDuration))).First()));
                return duration;
            }
            return -1;
        }

        private async Task<List<double>> GetLocationByQueryAsync(string query)
        {
            string apiKey = $"Ah23AFfxih6bgoSaVF8nmoI_GVIKvpR4Fah58v4rMWAC7aZeOGkjIqANdSR1LT-q";
            string queryFixed = query.Trim().Replace(" ", "%20").ToString();

            string url = $"http://dev.virtualearth.net/REST/v1/Locations/{queryFixed}?o=json&key={apiKey}";

            string result = await GatherDataAsync(url);

            if (!string.IsNullOrEmpty(result))
            {
                LocationRoot rootLocation = JsonConvert.DeserializeObject<LocationRoot>(result);
                LocationPoint locationPoint = rootLocation.resourceSets.SelectMany(x => x.resources.Select(x => x.point)).FirstOrDefault();
                if (locationPoint != null && locationPoint.coordinates != null)
                {
                    return locationPoint.coordinates;
                }
            }
            return null;
        }

        private void ValidateDurationTravel(int durationTravel)
        {
            if (durationTravel < 10 /*minutes*/ * 60 /*seconds*/)
            {
                throw new ArgumentException("The duration of your travel cannot be less than 10 minutes.");
            }
        }

        private void ValidateIfAuthorExistsInDb(Guid id)
        {
            if (!db.Users.Any(x => x.Id == id))
            {
                throw new InvalidOperationException($"The User that is the author of the Playlist you are trying to create does not exist in the database.");
            }
        }

        private void ValidateIfAuthorIsDeleted(Guid id)
        {
            if (db.Users.First(x => x.Id == id).IsDeleted)
            {
                throw new InvalidOperationException($"The User that is the author of the Playlist you are trying to create is deleted from the database.");
            }
        }

        private void ValidateIfNameIsNullOrEmpty(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentNullException("Value cannot be an empty string or null.");
            }
        }

        private void ValidateIfPlaylistIsDeleted(Guid id)
        {
            if (db.Playlists.First(x => x.Id == id).IsDeleted)
            {
                throw new ArgumentException("Playlist is deleted.");
            }
        }

        private void ValidateIfPlaylistIsUnlisted(Guid id)
        {
            if (db.Playlists.First(x => x.Id == id).IsUnlisted)
            {
                throw new ArgumentException("Playlist is unlisted.");
            }
        }

        private void ValidateIfUserWithThisApiKeyIsTheAuthorOfPlaylist(Guid playlistId, User user)
        {
            ValidatePlaylistId(playlistId);

            var playlist = db.Playlists.FirstOrDefault(x => x.Id == playlistId);

            if (playlist.UserId != user.Id)
            {
                throw new InvalidOperationException("The current User is not the author of the Playlist so no operations with it are allowed.");
            }
        }

        private void ValidatePlaylistId(Guid id)
        {
            if (!this.db.Playlists.Any(x => x.Id == id))
            {
                throw new ArgumentException("A Playlist with such Id does not exist in the database.");
            }
        }
    }
}
