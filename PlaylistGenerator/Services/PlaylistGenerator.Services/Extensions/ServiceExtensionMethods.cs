﻿using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Extensions
{
    public static class ServiceExtensionMethods
    {
        public static async Task<string> GetJsonStreamFromUrlAsync(this IService service, string url)
        {
            var client = new HttpClient();

            var response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            string result = await response.Content.ReadAsStringAsync();

            return result;
        }

        public static async Task<User> ValidateAPIKeyAsync(this IService service, Guid? apiKey, PlaylistGeneratorDbContext db)
        {
            if (apiKey is null)
                throw new ArgumentException("Invalid API Key.");

            User userWithThisApiKey = await db.Users.FirstOrDefaultAsync(x => x.ApiKey == apiKey);

            if (userWithThisApiKey == null || userWithThisApiKey.IsDeleted)
                throw new ArgumentException("Invalid API Key.");

            if (userWithThisApiKey.IsBanned)
                throw new ArgumentException("This User is temporarily Banned.");

            return userWithThisApiKey;
        }

        public static async Task CheckIfUserIsAdminByApiKey(this IService service, Guid? apiKey, PlaylistGeneratorDbContext db)
        {
            var userWithKey = await service.ValidateAPIKeyAsync(apiKey, db);

            if (!userWithKey.IsAdmin)
                throw new ArgumentException("Only an Admin can trigger an update in the database.");
        }
    }
}
