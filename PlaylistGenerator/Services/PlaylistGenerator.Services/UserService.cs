﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Extensions;
using PlaylistGenerator.Services.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services
{
    public class UserService : IUserService
    {
        public const int PageCountSize = 12;

        private readonly PlaylistGeneratorDbContext db;
        private readonly IMapper mapper;
        private readonly IDateTimeProvider dateTimeProvider;


        /// <summary>
        /// A constructor that instantiates a new instance of UserService.
        /// </summary>
        /// <param name="db">A PlaylistGeneratorDbContext; dependency injected into the constructor.</param>
        /// <param name="mapper">An IMapper; dependency injected into the constructor.</param>
        /// <param name="dateTimeProvider">An IDateTimeProvider; dependency injected into the constructor.</param>
        public UserService(PlaylistGeneratorDbContext db, IMapper mapper, IDateTimeProvider dateTimeProvider)
        {
            this.db = db;
            this.mapper = mapper;
            this.dateTimeProvider = dateTimeProvider;
        }

        /// <summary>
        /// An async method that creates a new user in the database based on a CreateUserServiceModel input that comes as a method parameter.
        /// </summary>
        /// <param name="input">A CreateUserServiceModel that holds all necessary information for a new user to be created.</param>
        /// <param name="userManager">A UserManager<User> that is needed to call its CreateAsync() method.</param>
        /// <returns>A task that represents a UserServiceModel that holds all information for the newly created user.</returns>
        public async Task<UserServiceModel> CreateUserAsync(CreateUserServiceModel input, UserManager<User> userManager)
        {
            var user = new User
            {
                FirstName = input.FirstName,
                LastName = input.LastName,
                DateCreated = this.dateTimeProvider.GetDateTime(),
                DateModified = this.dateTimeProvider.GetDateTime(),
                UserName = input.Email,
                Email = input.Email
            };

            var result = await userManager.CreateAsync(user, input.Password);

            if (result.Succeeded)
            {
                var userServiceModel = await this.GetUserByEmailAsync(input.Email);
                return userServiceModel;
            }

            return null;
        }

        /// <summary>
        /// An async method that deletes a user from the database and returns a boolean - if a user with such userId Guid parameter is found and successfully deleted.
        /// </summary>
        /// <param name="userId">The Guid that is the identity of the user to delete.</param>
        /// <param name="isApiKeyRequired">The boolean that reveals if Api Key is required for this action (if the method is called from an ApiController).</param>
        /// <param name="userApiKey">The Guid? that is the Api Key which needs to be validated if the method is called from an ApiController.</param>
        /// <returns>A task that represents a boolean that reveals if the action is successfully achieved.</returns>
        public async Task<bool> DeleteUserAsync(Guid userId, bool isApiKeyRequired = false, Guid? userApiKey = null)
        {
            ValidateUserId(userId);
            ValidateIfUserIsDeleted(userId);

            User userWithApiKey = null;

            if (isApiKeyRequired)
            {
                userWithApiKey = await this.ValidateAPIKeyAsync(userApiKey, this.db);
                ValidateIfUserWithApiKeyIsProfileOwner(userId, userWithApiKey);
            }

            var userInDb = await this.db.Users.FindAsync(userId);

            userInDb.DateModified = dateTimeProvider.GetDateTime();
            userInDb.IsDeleted = true;

            await db.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// An async method that gets all users from the database and returns them as a collection of UserServiceModels.
        /// </summary>
        /// <param name="isAdmin">The boolean that reveals if the caller of the method is an Admin or not.</param>
        /// <param name="page">The integer that defines which page of the collection of all users the caller demands.</param>
        /// <returns>A task that represents a collection of UserServiceModels - all users in a certain page.</returns>
        public async Task<IEnumerable<UserServiceModel>> GetAllUsersAsync(bool isAdmin = false, int page = 1)
        {
            var users = await this.db.Users
                .Where(x => x.IsDeleted == false)
                .Include(u => u.Playlists)
                .ToListAsync();

            if (!isAdmin)
            {
                users = users.Where(x => x.IsBanned == false).ToList();
            }

            var usersByPage = users
                .Skip((page - 1) * PageCountSize)
                .Take(PageCountSize);

            return this.mapper.Map<IEnumerable<UserServiceModel>>(usersByPage);
        }

        /// <summary>
        /// A method that returns the value of the const int PageCountSize.
        /// </summary>
        /// <returns>An integer - the count of entities within a single page.</returns>
        public int GetPageCountSizing()
        {
            int pageCountSize = PageCountSize;

            return pageCountSize;
        }

        /// <summary>
        /// A method that returns the count of all users in the database that are not deleted.
        /// </summary>
        /// <returns>An integer - the count of all users that are not deleted in the database.</returns>
        public int GetTotalUsersCount()
        {
            return this.db.Users.Where(x => !x.IsDeleted).Count();
        }

        /// <summary>
        /// An async method that finds a user in the database based on an API key and returns a UserServiceModel with information on that user.
        /// </summary>
        /// <param name="apiKey">The Guid? is an API Key of a user that is the criteria for searching in the database for a user with such API Key.</param>
        /// <returns>A task that represents a UserServiceModel; all information on the user found. If no such user - throws ArgumentException()</returns>
        public async Task<UserServiceModel> GetUserByApiKeyAsync(Guid? apiKey = null)
        {
            var user = await this.db.Users
                .Include(u => u.Playlists)
                .FirstOrDefaultAsync(x => x.ApiKey == apiKey);

            if (user == null)
            {
                throw new ArgumentException("The API Key is not valid!");
            }
            else
            {
                ValidateIfUserIsBanned(user.Id);
                ValidateIfUserIsDeleted(user.Id);
            }

            return this.mapper.Map<UserServiceModel>(user);
        }

        /// <summary>
        /// An async method that finds a user in the database based on an email string and returns a UserServiceModel with information on that user.
        /// </summary>
        /// <param name="email">A string that is the criteria for searching in the database for a user with such email.</param>
        /// <returns>A task that represents a UserServiceModel; all information on the user found.</returns>
        public async Task<UserServiceModel> GetUserByEmailAsync(string email)
        {
            var user = await this.db.Users
                .Include(u => u.Playlists)
                .FirstOrDefaultAsync(x => x.Email == email);

            if (user != null)
            {
                ValidateIfUserIsBanned(user.Id);
                ValidateIfUserIsDeleted(user.Id);
            }

            return this.mapper.Map<UserServiceModel>(user);
        }

        /// <summary>
        /// An async method that receives the id of a user as Guid and if a user with such Id is found in the database - returns a new UserServiceModel with the information of this user.
        /// </summary>
        /// <param name="userId">A Guid that is the id of a user.</param>
        /// <param name="isApiKeyRequired">A boolean that reveals if the method is called from an ApiController.</param>
        /// <param name="userApiKey">A Guid that is the Api Key that needs to be validated if the method is called from an ApiController.</param>
        /// <returns>A task that represents a UserServiceModel; holds the information of the user found by id.</returns>
        public async Task<UserServiceModel> GetUserByIdAsync(Guid userId, bool isApiKeyRequired = false, Guid? userApiKey = null)
        {
            ValidateUserId(userId);
            ValidateIfUserIsBanned(userId);
            ValidateIfUserIsDeleted(userId);

            User userWithApiKey = null;
            if (isApiKeyRequired)
            {
                userWithApiKey = await this.ValidateAPIKeyAsync(userApiKey, this.db);
                ValidateIfUserWithApiKeyIsProfileOwner(userId, userWithApiKey);
            }

            var user = await this.db.Users
                .Include(u => u.Playlists)
                .Where(x => x.Id == userId).FirstOrDefaultAsync();

            return this.mapper.Map<UserServiceModel>(user);
        }

        /// <summary>
        /// An async method that receives the id of a user and swaps its ban status.
        /// </summary>
        /// <param name="id">A Guid which is the identity of the user in the database.</param>
        public async Task SwapUserBanStatusByIdAsync(Guid id)
        {
            ValidateUserId(id);
            ValidateIfUserIsDeleted(id);

            var userInDb = await db.Users.FindAsync(id);

            userInDb.IsBanned = !userInDb.IsBanned;
            await db.SaveChangesAsync();
        }

        /// <summary>
        /// An async method that receives the id of a user and sets the API Key of the user from NULL to a new Guid.
        /// </summary>
        /// <param name="id">A Guid that is the identity of the user to update in the database.</param>
        /// <returns>A task that represents a Guid; the new Api Key for this user.</returns>
        public async Task<Guid?> UpdateUserApiKeyAsync(Guid id)
        {
            ValidateUserId(id);
            ValidateIfUserIsBanned(id);
            ValidateIfUserIsDeleted(id);

            var user = await this.db.Users.FindAsync(id);

            var newApiKey = Guid.NewGuid();
            user.ApiKey = newApiKey;
            await db.SaveChangesAsync();

            return newApiKey;
        }

        /// <summary>
        /// UpdateUserAsync() is an asynchronous method that updates the first name, the last name and the date modified of a user.
        /// </summary>
        /// <param name="user">Param "user" is a UserServiceModel that holds the information to update.</param>
        /// <param name="isApiKeyRequired">Param "isApiKeyRequired" is a boolean that reveals if an Api Key is required for this action (if the method is called from the ApiControllers) or not (Controllers).</param>
        /// <param name="userApiKey">Param "userApiKey" is a Guid that is the Api Key which needs to be validated if the method is called from the ApiController.</param>
        /// <returns>Returns a UserServiceModel with the updated properties of the user.</returns>
        public async Task<UserServiceModel> UpdateUserAsync(UserServiceModel user, bool isApiKeyRequired = false, Guid? userApiKey = null)
        {
            ValidateUserId(user.Id);
            ValidateIfUserIsBanned(user.Id);
            ValidateIfUserIsDeleted(user.Id);

            User userWithApiKey = null;

            if (isApiKeyRequired)
            {
                userWithApiKey = await this.ValidateAPIKeyAsync(userApiKey, this.db);
                ValidateIfUserWithApiKeyIsProfileOwner(user.Id, userWithApiKey);
            }

            var userInDb = await this.db.Users.FindAsync(user.Id);

            userInDb.FirstName = user.FirstName;
            userInDb.LastName = user.LastName;
            userInDb.DateModified = dateTimeProvider.GetDateTime();

            await db.SaveChangesAsync();

            User userUpdated = await this.db.Users
                .Include(u => u.Playlists)
                .Where(x => x.Id == user.Id).FirstOrDefaultAsync();

            return this.mapper.Map<UserServiceModel>(userUpdated);
        }

        /// <summary>
        /// An async method that receives the id of a user and sets the image path of the user to "/directory/{id}.jpg".
        /// </summary>
        /// <param name="id">A Guid which is the identity of the user in the database.</param>
        /// <returns>A task that represents a boolean; reveals if the action is successfully achieved or not.</returns>
        public async Task<bool> UpdateUserImageAsync(Guid id)
        {
            ValidateUserId(id);
            ValidateIfUserIsBanned(id);
            ValidateIfUserIsDeleted(id);

            var user = await this.db.Users.FindAsync(id);

            user.Image = $"/images/users/{id}.jpg";
            await db.SaveChangesAsync();

            return true;
        }


        private void ValidateUserId(Guid id)
        {
            if (!db.Users.Any(x => x.Id == id))
            {
                throw new ArgumentException("No such user with that id.");
            }
        }

        private void ValidateIfUserIsBanned(Guid id)
        {
            if (db.Users.First(x => x.Id == id).IsBanned)
            {
                throw new ArgumentException("User is banned.");
            }
        }

        private void ValidateIfUserIsDeleted(Guid id)
        {
            if (db.Users.First(x => x.Id == id).IsDeleted)
            {
                throw new ArgumentException("User is deleted.");
            }
        }

        private void ValidateIfUserWithApiKeyIsProfileOwner(Guid userId, User userWithApiKey)
        {
            if (userId != userWithApiKey.Id)
            {
                throw new ArgumentException("You don't own the user profile you are trying to access. You can manipulate your profile only.");
            }
        }
    }
}
