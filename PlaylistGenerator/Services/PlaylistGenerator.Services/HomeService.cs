﻿using Newtonsoft.Json;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Extensions;
using PlaylistGenerator.Services.Models;
using PlaylistGenerator.Services.Models.Home;
using PlaylistGenerator.Services.Models.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services
{
    public class HomeService : IHomeService
    {
        private const string newsApiKey = "7a72c8b3f54440eda78efb38b7a7e2fc";

        /// <summary>
        /// An async method that takes a message and a subject from a EmailSendServiceModel and sends it via a Gmail SMTP server to a predefined domain address. Returns a boolean that reveals if the action is successfull.
        /// </summary>
        /// <param name="model">The EmailSendServiceModel that holds the subject and the message of the email to be sent.</param>
        /// <returns>A boolean; If the message is sent successfully.</returns>
        public bool SendEmail(EmailSendServiceModel model)
        {
            #region CommentsOnSmtpClient
            // MailAddress fromAddress/string fromPassword - email/password in Gmail to get access to the SmtpClient Host ("smpt.gmail.com").
            // That would throw a SmtpException: 5.7.0 Authentication Required.
            // You would also receive an email in Gmail saying "Critical security alert".
            // What you need to do is to press "Check Activity" => Less secure app blocked => Learn more => Less secure app access => Allow less secure apps: ON.
            // Now it should work.
            #endregion

            MailAddress fromAddress = new MailAddress("yourgmail@gmail.com");
            string fromPassword = "yourgmailpassword";
            var toAddress = new MailAddress("yourdomainemail@mail.com");

            string subject = model.Subject;
            string body = model.Message;

            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            try
            {
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                    return true;
                }
            }
            catch (SmtpException ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// An async method which gets a collection of 3 news and returns it as a collection of NewsServiceModels.
        /// </summary>
        /// <returns>A Task<IEnumerable<NewsServiceModel>>; The 3 news that are randomly received from newsapi.com.</returns>
        public async Task<IEnumerable<NewsServiceModel>> GetMusicNews()
        {
            var url = $"http://newsapi.org/v2/everything?q=music&language=en&sortBy=publishedAt&apiKey={newsApiKey}";

            try
            {
                var jsonNews = await this.GetJsonStreamFromUrlAsync(url);
                var news = JsonConvert.DeserializeObject<CollectionNewsResultsFromApi>(jsonNews);

                var newsServiceModel = news.Articles.Take(3).ToList();

                this.TrimNewsContent(newsServiceModel);
                
                return newsServiceModel;
            }
            catch
            {
                return new List<NewsServiceModel>();
            }

        }

        private void TrimNewsContent(List<NewsServiceModel> news)
        {
            foreach (var item in news)
            {
                if(item.Content.Length > 100)
                {
                    item.Content = item.Content.Substring(0, 100) + "...";
                }
            }
        }

    }
}

