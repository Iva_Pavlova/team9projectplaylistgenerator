﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Extensions;
using PlaylistGenerator.Services.Models.Album;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services
{
    public class AlbumService : IAlbumService
    {
        private readonly PlaylistGeneratorDbContext db;
        private readonly IMapper mapper;

        /// <summary>
        /// A constructor that creates a new instance of AlbumService.
        /// </summary>
        /// <param name="db">A PlaylistGeneratorDbContext dependency injected into the constructor..</param>
        /// <param name="mapper">An IMapper dependency injected into the constructor..</param>
        public AlbumService(PlaylistGeneratorDbContext db, IMapper mapper)
        {
            this.db = db;
            this.mapper = mapper;
        }

        /// <summary>
        /// An async method which returns an Album from the database.
        /// </summary>
        /// <param name="albumId">The Id of the Album to return.</param>
        /// <param name="requireApiKey">Whether or not to require an API key for the execution of the method.</param>
        /// <param name="apiKey">The API key to validate if requireApiKey parameter is true.</param>
        /// <returns>
        /// A Task<AlbumServiceModel>; The Album with Id equal to albumId parameter.
        /// </returns>
        public async Task<AlbumServiceModel> GetAlbumByIdAsync(Guid albumId, bool requireApiKey = false, Guid? apiKey = null)
        {
            if (requireApiKey)
                await this.ValidateAPIKeyAsync(apiKey, this.db);

            var album = await this.db.Albums.Include(a => a.Artist).FirstOrDefaultAsync(a => a.Id == albumId);

            if (album == null)
                throw new ArgumentException("Album Not Found.");

            var albumServiceModel = this.mapper.Map<AlbumServiceModel>(album);

            return albumServiceModel;
        }
    }
}
