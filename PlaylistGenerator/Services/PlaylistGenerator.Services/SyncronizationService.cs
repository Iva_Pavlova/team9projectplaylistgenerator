﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Models.Syncronization;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services
{
    public class SyncronizationService : ISyncronizationService
    {
        private readonly PlaylistGeneratorDbContext db;
        private readonly IGenreService genreService;
        private readonly ISongService songService;
        private readonly IMapper mapper;
        private readonly IDateTimeProvider dateTimeProvider;


        /// <summary>
        /// A constructor that instantiates a new instance of SyncronizationService.
        /// </summary>
        /// <param name="songService">An ISongService dependency injected into the constructor..</param>
        /// <param name="genreService">An IGenreService dependency injected into the constructor..</param>
        /// <param name="db">A PlaylistGeneratorDbContext dependency injected into the constructor..</param>
        /// <param name="dateTimeProvider">An IDateTimeProvider dependency injected into the constructor..</param>
        /// <param name="mapper">An IMapper dependency injected into the constructor..</param>
        public SyncronizationService(ISongService songService, IGenreService genreService, PlaylistGeneratorDbContext db, IDateTimeProvider dateTimeProvider, IMapper mapper)
        {
            this.db = db;
            this.genreService = genreService;
            this.songService = songService;
            this.mapper = mapper;
            this.dateTimeProvider = dateTimeProvider;
        }

        /// <summary>
        /// An async method which updates the data of Genres and Songs in the database by sending requests to Deezer API.
        /// </summary>
        /// <param name="requireApiKey">Whether or not to require an API key for the execution of the method.</param>
        /// <param name="apiKey">The API key to validate if requireApiKey parameter is true.</param>
        /// <returns>
        /// A Task representing an asyncronous operation which updates the data of the Genres and Songs in the database.
        /// </returns>
        public async Task SyncronizeGenresAndSongsAsync(bool requireApiKey = false, Guid? apiKey = null)
        {
            try
            {
                if (await this.LastSyncronizationStillInProgress())
                    return;

                await this.genreService.UpdateGenresInDbAsync(requireApiKey, apiKey);

                await this.AddIncompleteSyncronizationToDatabaseAsync();

                await this.songService.UpdateSongsInDbAsync(requireApiKey, apiKey);

                await this.MarkLastSyncronizationCompleteAsync();
            }
            catch
            {
                await this.MarkLastSyncronizationInterruptedAsync();
                throw new Exception("Unable to fetch data form Deezer API.");
            }
        }

        /// <summary>
        /// An async method which returns the Syncronization with latest Date from the database.
        /// </summary>
        /// <param name="requireApiKey">Whether or not to require an API key for the execution of the method.</param>
        /// <param name="apiKey">The API key to validate if requireApiKey parameter is true.</param>
        /// <returns>
        /// A Task<SyncronizationServiceModel>; The last Syncronization of Genres and Songs from the database;
        /// </returns>
        public async Task<SyncronizationServiceModel> GetLastSyncronizationAsync(bool requireApiKey = false, Guid? apiKey = null)
        {
            var syncronization = await this.db.Syncronisations.OrderByDescending(s => s.Date).FirstOrDefaultAsync();
            if (syncronization == null)
                return null;

            if (this.SyncronizationTookTooLong(syncronization))
                await this.MarkLastSyncronizationInterruptedAsync();

            var syncronizationServiceModel = this.mapper.Map<SyncronizationServiceModel>(syncronization);

            return syncronizationServiceModel;
        }

        private async Task AddIncompleteSyncronizationToDatabaseAsync()
        {
            await this.db.Syncronisations.AddAsync(new Syncronization
            {
                Date = this.dateTimeProvider.GetDateTime(),
                IsComplete = false
            });

            await this.db.SaveChangesAsync();
        }

        private async Task MarkLastSyncronizationCompleteAsync()
        {
            var currentSyncronization = await this.db.Syncronisations.OrderByDescending(s => s.Date).FirstAsync();
            currentSyncronization.IsComplete = true;

            await this.db.SaveChangesAsync();
        }

        private async Task MarkLastSyncronizationInterruptedAsync()
        {
            var currentSyncronization = await this.db.Syncronisations.OrderByDescending(s => s.Date).FirstAsync();
            currentSyncronization.WasInterrupted = true;

            await this.db.SaveChangesAsync();
        }

        private async Task<bool> LastSyncronizationStillInProgress()
        {
            var lastSynconization = await this.db.Syncronisations.OrderByDescending(s => s.Date).FirstOrDefaultAsync();

            if (this.SyncronizationTookTooLong(lastSynconization))
                await this.MarkLastSyncronizationInterruptedAsync();

            return lastSynconization != null && !lastSynconization.IsComplete && !lastSynconization.WasInterrupted;
        }

        private bool SyncronizationTookTooLong(Syncronization syncronization)
        {
            if (syncronization == null)
                return false;

            TimeSpan? timeSinceSyncronization = this.dateTimeProvider.GetDateTime() - syncronization.Date;
            if (timeSinceSyncronization.Value.TotalMinutes >= 20)
                return true;

            return false;
        }
    }
}
