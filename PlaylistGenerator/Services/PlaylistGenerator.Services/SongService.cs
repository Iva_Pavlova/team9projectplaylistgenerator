﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Data.Models.DeezerObjects;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Extensions;
using PlaylistGenerator.Services.Models.Song;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services
{
    public class SongService : ISongService
    {
        private readonly PlaylistGeneratorDbContext db;
        private readonly IMapper mapper;

        /// <summary>
        /// A constructor that creates a new instance of SongService.
        /// </summary>
        /// <param name="db">A PlaylistGeneratorDbContext dependency injected into the constructor..</param>
        /// <param name="mapper">An IMapper dependency injected into the constructor..</param>
        public SongService(PlaylistGeneratorDbContext db, IMapper mapper)
        {
            this.db = db;
            this.mapper = mapper;
        }

        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((double)this.db.Songs.Count() / SongsPerPage);
            }
        }

        public int SongsPerPage => 25;

        /// <summary>
        /// An async method which returns Songs from the database.
        /// </summary>
        /// <param name="page">The page of Songs to be returned;</param>
        /// <param name="requireApiKey">Whether or not to require an API key for the execution of the method</param>
        /// <param name="apiKey">The API key to validate if requireApiKey parameter is true.</param>
        /// <returns>
        /// A Task<IEnumerable<SongServiceModel>>; The songs to be shown on a given page.
        /// </returns>
        public async Task<IEnumerable<SongServiceModel>> GetAllSongsAsync(int page = 1, bool requireApiKey = false, Guid? apiKey = null)
        {
            if (requireApiKey)
                await this.ValidateAPIKeyAsync(apiKey, this.db);

            var songs = this.db.Songs
                .Include(s => s.Album)
                .Include(s => s.Artist)
                .Include(s => s.Genre)
                .Skip((page - 1) * SongsPerPage)
                .Take(SongsPerPage);

            if (songs == null)
                return new List<SongServiceModel>();

            var songServiceModels = this.mapper.Map<IEnumerable<SongServiceModel>>(songs);

            return songServiceModels;
        }


        /// <summary>
        /// An async method which returns a Song from the database.
        /// </summary>
        /// <param name="songId">The Id of the Song to return.</param>
        /// <param name="requireApiKey">Whether or not to require an API key for the execution of the method</param>
        /// <param name="apiKey">The API key to validate if requireApiKey parameter is true.</param>
        /// <returns>
        /// A Task<SongServiceModel>; The Song with Id equal to songId parameter.
        /// </returns>
        public async Task<SongServiceModel> GetSongByIdAsync(Guid songId, bool requireApiKey = false, Guid? apiKey = null)
        {
            if (requireApiKey)
                await this.ValidateAPIKeyAsync(apiKey, this.db);

            var song = await this.db.Songs
                .Include(s => s.Album)
                .Include(s => s.Artist)
                .Include(s => s.Genre)
                .FirstOrDefaultAsync(s => s.Id == songId);

            if (song == null)
                throw new ArgumentException("Song Not Found.");

            var songServiceModel = this.mapper.Map<SongServiceModel>(song);

            return songServiceModel;
        }


        /// <summary>
        /// An async method which returns all Songs from a Playlist.
        /// </summary>
        /// <param name="playlistId">The Id of the Playlist containing the songs.</param>
        /// <param name="requireApiKey">Whether or not to require an API key for the execution of the method</param>
        /// <param name="apiKey">The API key to validate if requireApiKey parameter is true.</param>
        /// <returns>
        /// A Task<IEnumerable<SongServiceModel>>; The Songs that the Playlist contains.
        /// </returns>
        public async Task<IEnumerable<SongServiceModel>> GetSongsByPlaylistIdAsync(Guid playlistId, bool requireApiKey = false, Guid? apiKey = null)
        {
            if (requireApiKey)
                await this.ValidateAPIKeyAsync(apiKey, this.db);

            var songs = this.db.PlaylistsSongs
                .Where(s => s.PlaylistId == playlistId)
                .Include(s => s.Song)
                .ThenInclude(sng => sng.Album)
                .Include(s => s.Song)
                .ThenInclude(sng => sng.Artist)
                .Include(s => s.Song)
                .ThenInclude(sng => sng.Genre)
                .Select(s => s.Song);

            var songServiceModels = this.mapper.Map<IEnumerable<SongServiceModel>>(songs);

            return songServiceModels;
        }


        /// <summary>
        /// An async method which returns the 3 Songs with Rank closest to 1 from the database.
        /// </summary>
        /// <returns>
        /// A Task<IEnumerable<SongServiceModel>; The 3 Songs with best Rank.
        /// </returns>
        public async Task<IEnumerable<SongServiceModel>> GetTop3SongsAsync()
        {
            var songs = await this.db.Songs.OrderBy(s => s.Rank).Take(3).ToListAsync();

            var songServiceModels = this.mapper.Map<IEnumerable<SongServiceModel>>(songs);

            return songServiceModels;
        }


        /// <summary>
        /// An async method which loads Songs from Deezer API into the database.
        /// </summary>
        /// <returns>
        /// A Task representing an asyncronous operation which fills the database with Songs.
        /// </returns>
        public async Task LoadSongsInDbAsync()
        {
            if (this.DatabaseContainsSongs())
                return;

            List<Song> songs = await this.GetSongsForAllGenres(300);
            songs = await this.LoadSongArtistsInDbAsync(songs);
            songs = await this.LoadSongAlbumsInDbAsync(songs);

            await this.AddSongsToDbAsync(songs);
        }


        /// <summary>
        /// An async method which updates the data of Songs in the database by sending requests to Deezer API.
        /// </summary>
        /// <param name="requireApiKey">Whether or not to require an API key for the execution of the method.</param>
        /// <param name="apiKey">The API key to validate if requireApiKey parameter is true.</param>
        /// <returns>
        /// A Task representing an asyncronous operation which updates the data of the Songs in the database.
        /// </returns>
        public async Task UpdateSongsInDbAsync(bool requireApiKey = false, Guid? apiKey = null)
        {
            if (requireApiKey)
                await this.CheckIfUserIsAdminByApiKey(apiKey, this.db);

            if (!this.DatabaseContainsSongs())
                throw new ArgumentException("There are no songs to update in the Database.");


            int deletedSongs = await this.GetDbChangesOfSongs();

            if (deletedSongs == 0)
                return;


            int songsPerGenre = (int)Math.Ceiling((double)deletedSongs / this.db.Genres.Count());
            var songs = await this.GetSongsForAllGenres(songsPerGenre);
            List<Song> filteredSongs = this.FilterNewSongsFromExistingSongs(songs);

            filteredSongs = await this.LoadSongArtistsInDbAsync(filteredSongs);
            filteredSongs = await this.LoadSongAlbumsInDbAsync(filteredSongs);

            await this.AddSongsToDbAsync(filteredSongs);
        }


        private async Task AddSongsToDbAsync(IEnumerable<Song> songs)
        {
            foreach (var song in songs)
            {
                song.Artist = null;
                song.Album = null;
                await this.db.AddAsync(song);
            }

            await this.db.SaveChangesAsync();
        }

        private async Task<int> GetDbChangesOfSongs()
        {
            string url;
            int numberOfDeletedSongs = 0;
            string jsonSong;

            foreach (var song in this.db.Songs)
            {
                try
                {
                    url = $"https://api.deezer.com/track/{song.DeezerId}";
                    jsonSong = await this.GetJsonStreamFromUrlAsync(url);
                    this.UpdateSongRank(jsonSong, song);
                }
                catch
                {
                    await this.MarkSongDeletedAsync(song);
                    numberOfDeletedSongs++;
                }

                Thread.Sleep(120);
            }

            await this.db.SaveChangesAsync();

            return numberOfDeletedSongs;
        }

        private bool DatabaseContainsSongs()
        {
            if (this.db.Songs == null || this.db.Songs.Count() == 0)
                return false;

            return true;
        }

        private List<Song> FilterNewSongsFromExistingSongs(IEnumerable<Song> songs)
        {
            var filteredSongs = new List<Song>();

            foreach (var song in songs)
            {
                if (!this.db.Songs.Any(s => s.DeezerId == song.DeezerId))
                    filteredSongs.Add(song);
            }

            return filteredSongs;
        }

        private async Task<List<DeezerPlaylist>> GetPlaylistsOfGenreFromDeezerAPIAsync(string genre)
        {
            string url = $"https://api.deezer.com/search/playlist?q={genre}";

            string jsonPlaylist = await this.GetJsonStreamFromUrlAsync(url);

            DeezerPlaylistData playlistsData = JsonConvert.DeserializeObject<DeezerPlaylistData>(jsonPlaylist);
            var playlists = playlistsData.Playlists;


            return playlists;
        }

        private async Task<List<Song>> GetSongsForAllGenres(int songsPerGenre)
        {
            List<Song> songs = new List<Song>();
            List<DeezerPlaylist> playlists;
            List<string> trackListsUrls;
            List<Song> songsFromTrackLists;

            foreach (var genre in this.db.Genres)
            {
                playlists = await this.GetPlaylistsOfGenreFromDeezerAPIAsync(genre.Name);
                trackListsUrls = this.GetTracklistsURLsOfPlaylists(playlists);
                songsFromTrackLists = await this.GetSongsFromTracklistsAsync(trackListsUrls, songsPerGenre);

                songsFromTrackLists.ForEach(s => s.GenreId = genre.Id); 

                songs.AddRange(songsFromTrackLists);
            }

            return songs;
        }

        private async Task<List<Song>> GetSongsFromTracklistsAsync(List<string> trackListsUrls, int songsPerGenre)
        {
            List<Song> songs = new List<Song>();
            string jsonTrackList;
            DeezerTrackList trackList;

            foreach (var url in trackListsUrls)
            {
                jsonTrackList = await this.GetJsonStreamFromUrlAsync(url);
                trackList = JsonConvert.DeserializeObject<DeezerTrackList>(jsonTrackList);

                songs.AddRange(trackList.Songs);

                if (songs.Count >= songsPerGenre) // we don't need more than 300 songs per Genre, would make queries slow, otherwise we would remove this
                    break;

                Thread.Sleep(120);
            }

            return songs;
        }

        private List<string> GetTracklistsURLsOfPlaylists(List<DeezerPlaylist> playlists)
        {
            List<string> trackListsURLs = new List<string>();

            foreach (var item in playlists)
            {
                trackListsURLs.Add(item.SongListURL);
                if (trackListsURLs.Count == 40) //reducing risk of exceeding request quota in other methods
                    break;
            }

            return trackListsURLs;
        }

        private async Task<List<Song>> LoadSongAlbumsInDbAsync(List<Song> songs)
        {
            Album songAlbum;

            foreach (var song in songs)
            {
                songAlbum = await this.db.Albums.FirstOrDefaultAsync(a => a.DeezerId == song.Album.DeezerId);

                if (songAlbum == null)
                {
                    song.Album.ArtistId = song.ArtistId;

                    await this.db.Albums.AddAsync(song.Album);
                    await this.db.SaveChangesAsync();
                    songAlbum = await this.db.Albums.FirstAsync(a => a.DeezerId == song.Album.DeezerId);
                }

                song.AlbumId = songAlbum.Id;
            }

            return songs;
        }

        private async Task<List<Song>> LoadSongArtistsInDbAsync(List<Song> songs)
        {
            Artist songArtist;

            foreach (var song in songs)
            {
                songArtist = await this.db.Artists.FirstOrDefaultAsync(a => a.DeezerId == song.Artist.DeezerId);

                if (songArtist == null)
                {
                    await this.db.Artists.AddAsync(song.Artist);
                    await this.db.SaveChangesAsync();

                    songArtist = await this.db.Artists.FirstAsync(a => a.DeezerId == song.Artist.DeezerId);
                }

                song.ArtistId = songArtist.Id;
            }

            return songs;
        }

        private async Task MarkSongDeletedAsync(Song songInDb)
        {
            songInDb.Name = "This song is no longer available";
            songInDb.PreviewURL = null;
            songInDb.SongURL = null;

            await db.SaveChangesAsync();
        }

        private void UpdateSongRank(string jsonSong, Song song)
        {
            var currentSongInDeezer = JsonConvert.DeserializeObject<Song>(jsonSong);
            song.Rank = currentSongInDeezer.Rank;
        }
    }
}
