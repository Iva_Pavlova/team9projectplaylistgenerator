﻿using AutoMapper;
using PlaylistGenerator.Data;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Extensions;
using PlaylistGenerator.Services.Models.Artist;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services
{
    public class ArtistService : IArtistService
    {
        private readonly PlaylistGeneratorDbContext db;
        private readonly IMapper mapper;

        /// <summary>
        /// A constructor that creates a new instance of ArtistService.
        /// </summary>
        /// <param name="db">A PlaylistGeneratorDbContext dependency injected into the constructor..</param>
        /// <param name="mapper">An IMapper dependency injected into the constructor..</param>
        public ArtistService(PlaylistGeneratorDbContext db, IMapper mapper)
        {
            this.db = db;
            this.mapper = mapper;
        }

        /// <summary>
        /// An async method which returns an Artist from the database.
        /// </summary>
        /// <param name="artistId">The id of the Artist to return.</param>
        /// <param name="requireApiKey">Whether or not to require an API key for the execution of the method.</param>
        /// <param name="apiKey">The API key to validate if requireApiKey parameter is true.</param>
        /// <returns>
        /// A Task<ArtistServiceModel>; The Artist with Id equal to albumId parameter.</returns>
        public async Task<ArtistServiceModel> GetArtistByIdAsync(Guid artistId, bool requireApiKey = false, Guid? apiKey = null)
        {
            if (requireApiKey)
                await this.ValidateAPIKeyAsync(apiKey, this.db);

            var artist = await this.db.Artists.FindAsync(artistId);

            if (artist == null)
                throw new ArgumentException("Artist Not Found.");

            var artistServiceModel = this.mapper.Map<ArtistServiceModel>(artist);

            return artistServiceModel;
        }
    }
}
