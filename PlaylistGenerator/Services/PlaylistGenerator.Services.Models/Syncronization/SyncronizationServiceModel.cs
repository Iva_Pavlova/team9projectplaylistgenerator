﻿using System;

namespace PlaylistGenerator.Services.Models.Syncronization
{
    public class SyncronizationServiceModel
    {
        public Guid Id { get; set; }

        public DateTime? Date { get; set; }

        public bool IsComplete { get; set; }

        public bool WasInterrupted { get; set; }
    }
}
