﻿using PlaylistGenerator.Services.Models.Genre;
using System.Collections.Generic;

namespace PlaylistGenerator.Services.Models.Playlist
{
    public class PlaylistCreateServiceModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsTopTracksOptionEnabled { get; set; }

        public bool IsTracksFromSameArtistEnabled { get; set; }

        public List<GenreServiceModel> Genres { get; set; }

        public string PointFirstQueryString { get; set; }

        public string PointSecondQueryString { get; set; }
    }
}
