﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PlaylistGenerator.Services.Models.News
{
    public class CollectionNewsResultsFromApi
    {
        [JsonProperty("articles")]
        public ICollection<NewsServiceModel> Articles { get; set; }
    }
}
