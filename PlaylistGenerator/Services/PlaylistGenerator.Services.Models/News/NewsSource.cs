﻿using Newtonsoft.Json;

namespace PlaylistGenerator.Services.Models.News
{
    public class NewsSource
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
