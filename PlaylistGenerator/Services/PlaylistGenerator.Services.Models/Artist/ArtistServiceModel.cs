﻿using System;

namespace PlaylistGenerator.Services.Models.Artist
{
    public class ArtistServiceModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string ArtistPageURL { get; set; }

        public string PictureURL { get; set; }

        public int AlbumCount { get; set; }

        public int FanCount { get; set; }

        public string SongListUrl { get; set; }
    }
}
