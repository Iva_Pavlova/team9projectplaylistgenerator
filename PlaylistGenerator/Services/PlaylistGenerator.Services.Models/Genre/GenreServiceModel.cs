﻿using System;

namespace PlaylistGenerator.Services.Models.Genre
{
    public class GenreServiceModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string PictureURL { get; set; }
    }
}
