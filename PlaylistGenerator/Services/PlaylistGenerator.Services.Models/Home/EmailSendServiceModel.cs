﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlaylistGenerator.Services.Models.Home
{
    public class EmailSendServiceModel
    {
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
