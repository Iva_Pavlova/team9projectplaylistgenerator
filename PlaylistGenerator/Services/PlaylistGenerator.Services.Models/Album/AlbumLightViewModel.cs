﻿using System;

namespace PlaylistGenerator.Services.Models.Album
{
    public class AlbumLightViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Artist { get; set; }
    }
}
