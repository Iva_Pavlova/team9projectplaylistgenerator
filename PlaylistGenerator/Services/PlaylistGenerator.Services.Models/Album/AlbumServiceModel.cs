﻿using System;

namespace PlaylistGenerator.Services.Models.Album
{
    public class AlbumServiceModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string SongListUrl { get; set; }

        public string Artist { get; set; }
        public Guid ArtistId { get; set; }
    }
}
