﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services;
using PlaylistGenerator.Services.Models.Genre;
using System.Threading.Tasks;
using static PlaylistGenerator.Data.PlaylistGeneratorDbSeeder;

namespace PlaylistGenerator.Console
{
    public class Program
    {
        static async Task Main()
        {
            using var db = new PlaylistGeneratorDbContext(new DbContextOptionsBuilder<PlaylistGeneratorDbContext>()
                .UseSqlServer("Server=localhost\\SQLEXPRESS;Database=PlaylistGeneratorDatabase;Integrated Security=True;")
                .Options);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Genre, GenreServiceModel>();
            });

            var mapper = new Mapper(config);

            await LoadGenresInDb(db, mapper);

            // This takes quite a while, because we must not exceed request quota for Deezer.
            await LoadSongsInDb(db, mapper);

            SeedRoles(db);

            SeedUsersRoleAdmin(db);

            SeedUsersRoleUser(db);

            SeedPlaylists(db);
        }

        private static async Task LoadGenresInDb(PlaylistGeneratorDbContext db, IMapper mapper)
        {
            System.Console.WriteLine("GenreSeed started.");
            var genreService = new GenreService(db, mapper);
            await genreService.LoadGenresInDbAsync();
            System.Console.WriteLine("GenreSeed completed!");
        }

        private static async Task LoadSongsInDb(PlaylistGeneratorDbContext db, IMapper mapper)
        {
            System.Console.WriteLine("SongSeed started. This will take a while...");
            var songService = new SongService(db, mapper);
            await songService.LoadSongsInDbAsync();
            System.Console.WriteLine("SongSeed completed!");
        }
    }
}