﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PlaylistGenerator.Data.Migrations
{
    public partial class DbSetsForPlaylistManyToManyAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Playlist_Users_UserId",
                table: "Playlist");

            migrationBuilder.DropForeignKey(
                name: "FK_PlaylistGenre_Genres_GenreId",
                table: "PlaylistGenre");

            migrationBuilder.DropForeignKey(
                name: "FK_PlaylistGenre_Playlist_PlaylistId",
                table: "PlaylistGenre");

            migrationBuilder.DropForeignKey(
                name: "FK_PlaylistSong_Playlist_PlaylistId",
                table: "PlaylistSong");

            migrationBuilder.DropForeignKey(
                name: "FK_PlaylistSong_Songs_SongId",
                table: "PlaylistSong");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlaylistSong",
                table: "PlaylistSong");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlaylistGenre",
                table: "PlaylistGenre");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Playlist",
                table: "Playlist");

            migrationBuilder.RenameTable(
                name: "PlaylistSong",
                newName: "PlaylistsSongs");

            migrationBuilder.RenameTable(
                name: "PlaylistGenre",
                newName: "PlaylistsGenres");

            migrationBuilder.RenameTable(
                name: "Playlist",
                newName: "Playlists");

            migrationBuilder.RenameIndex(
                name: "IX_PlaylistSong_SongId",
                table: "PlaylistsSongs",
                newName: "IX_PlaylistsSongs_SongId");

            migrationBuilder.RenameIndex(
                name: "IX_PlaylistGenre_GenreId",
                table: "PlaylistsGenres",
                newName: "IX_PlaylistsGenres_GenreId");

            migrationBuilder.RenameIndex(
                name: "IX_Playlist_UserId",
                table: "Playlists",
                newName: "IX_Playlists_UserId");

            migrationBuilder.AlterColumn<int>(
                name: "Duration",
                table: "Songs",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlaylistsSongs",
                table: "PlaylistsSongs",
                columns: new[] { "PlaylistId", "SongId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlaylistsGenres",
                table: "PlaylistsGenres",
                columns: new[] { "PlaylistId", "GenreId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Playlists",
                table: "Playlists",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Playlists_Users_UserId",
                table: "Playlists",
                column: "UserId",
                principalSchema: "dbo",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlaylistsGenres_Genres_GenreId",
                table: "PlaylistsGenres",
                column: "GenreId",
                principalTable: "Genres",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlaylistsGenres_Playlists_PlaylistId",
                table: "PlaylistsGenres",
                column: "PlaylistId",
                principalTable: "Playlists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlaylistsSongs_Playlists_PlaylistId",
                table: "PlaylistsSongs",
                column: "PlaylistId",
                principalTable: "Playlists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlaylistsSongs_Songs_SongId",
                table: "PlaylistsSongs",
                column: "SongId",
                principalTable: "Songs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Playlists_Users_UserId",
                table: "Playlists");

            migrationBuilder.DropForeignKey(
                name: "FK_PlaylistsGenres_Genres_GenreId",
                table: "PlaylistsGenres");

            migrationBuilder.DropForeignKey(
                name: "FK_PlaylistsGenres_Playlists_PlaylistId",
                table: "PlaylistsGenres");

            migrationBuilder.DropForeignKey(
                name: "FK_PlaylistsSongs_Playlists_PlaylistId",
                table: "PlaylistsSongs");

            migrationBuilder.DropForeignKey(
                name: "FK_PlaylistsSongs_Songs_SongId",
                table: "PlaylistsSongs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlaylistsSongs",
                table: "PlaylistsSongs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlaylistsGenres",
                table: "PlaylistsGenres");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Playlists",
                table: "Playlists");

            migrationBuilder.RenameTable(
                name: "PlaylistsSongs",
                newName: "PlaylistSong");

            migrationBuilder.RenameTable(
                name: "PlaylistsGenres",
                newName: "PlaylistGenre");

            migrationBuilder.RenameTable(
                name: "Playlists",
                newName: "Playlist");

            migrationBuilder.RenameIndex(
                name: "IX_PlaylistsSongs_SongId",
                table: "PlaylistSong",
                newName: "IX_PlaylistSong_SongId");

            migrationBuilder.RenameIndex(
                name: "IX_PlaylistsGenres_GenreId",
                table: "PlaylistGenre",
                newName: "IX_PlaylistGenre_GenreId");

            migrationBuilder.RenameIndex(
                name: "IX_Playlists_UserId",
                table: "Playlist",
                newName: "IX_Playlist_UserId");

            migrationBuilder.AlterColumn<double>(
                name: "Duration",
                table: "Songs",
                type: "float",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlaylistSong",
                table: "PlaylistSong",
                columns: new[] { "PlaylistId", "SongId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlaylistGenre",
                table: "PlaylistGenre",
                columns: new[] { "PlaylistId", "GenreId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Playlist",
                table: "Playlist",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Playlist_Users_UserId",
                table: "Playlist",
                column: "UserId",
                principalSchema: "dbo",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlaylistGenre_Genres_GenreId",
                table: "PlaylistGenre",
                column: "GenreId",
                principalTable: "Genres",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlaylistGenre_Playlist_PlaylistId",
                table: "PlaylistGenre",
                column: "PlaylistId",
                principalTable: "Playlist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlaylistSong_Playlist_PlaylistId",
                table: "PlaylistSong",
                column: "PlaylistId",
                principalTable: "Playlist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlaylistSong_Songs_SongId",
                table: "PlaylistSong",
                column: "SongId",
                principalTable: "Songs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
