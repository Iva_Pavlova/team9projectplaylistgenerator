﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlaylistGenerator.Data.Migrations
{
    public partial class AddedSyncronizationsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SongListURL",
                table: "Artists",
                newName: "SongListUrl");

            migrationBuilder.RenameColumn(
                name: "SonglistUrl",
                table: "Albums",
                newName: "SongListUrl");

            migrationBuilder.CreateTable(
                name: "Syncronisations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    IsComplete = table.Column<bool>(nullable: false),
                    WasInterrupted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Syncronisations", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Syncronisations");

            migrationBuilder.RenameColumn(
                name: "SongListUrl",
                table: "Artists",
                newName: "SongListURL");

            migrationBuilder.RenameColumn(
                name: "SongListUrl",
                table: "Albums",
                newName: "SonglistUrl");
        }
    }
}
