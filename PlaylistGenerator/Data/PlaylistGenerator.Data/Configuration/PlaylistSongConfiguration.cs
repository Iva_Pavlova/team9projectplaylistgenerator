﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PlaylistGenerator.Data.Models;

namespace PlaylistGenerator.Data.Configuration
{
    public class PlaylistSongConfiguration : IEntityTypeConfiguration<PlaylistSong>
    {
        public void Configure(EntityTypeBuilder<PlaylistSong> playlistSong)
        {
            playlistSong
                .HasKey(playSong => new { playSong.PlaylistId, playSong.SongId });

            playlistSong
                .HasOne(playSong => playSong.Playlist)
                .WithMany(playlist => playlist.Songs)
                .HasForeignKey(playSong => playSong.PlaylistId)
                .OnDelete(DeleteBehavior.Restrict);

            playlistSong
                .HasOne(playSong => playSong.Song)
                .WithMany(song => song.Playlists)
                .HasForeignKey(playSong => playSong.SongId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
