﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PlaylistGenerator.Data.Models;

namespace PlaylistGenerator.Data.Configuration
{
    public class PlaylistGenreConfiguration : IEntityTypeConfiguration<PlaylistGenre>
    {
        public void Configure(EntityTypeBuilder<PlaylistGenre> playlistGenre)
        {
            playlistGenre
                .HasKey(playGenre => new { playGenre.PlaylistId, playGenre.GenreId });

            playlistGenre
                .HasOne(playGenre => playGenre.Playlist)
                .WithMany(playlist => playlist.Genres)
                .HasForeignKey(playGenre => playGenre.PlaylistId)
                .OnDelete(DeleteBehavior.Restrict);

            playlistGenre
                .HasOne(playGenre => playGenre.Genre)
                .WithMany(genre => genre.Playlists)
                .HasForeignKey(playGenre => playGenre.GenreId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
