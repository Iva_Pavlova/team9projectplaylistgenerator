﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data.Models;
using System;

namespace PlaylistGenerator.Data
{
    public class PlaylistGeneratorDbContext : IdentityDbContext<User, Role, Guid>
    {
        public PlaylistGeneratorDbContext(DbContextOptions<PlaylistGeneratorDbContext> options) : base(options)
        {
        }

        public DbSet<Album> Albums { get; set; }

        public DbSet<Artist> Artists { get; set; }

        public DbSet<Genre> Genres { get; set; }

        public DbSet<Playlist> Playlists { get; set; }

        public DbSet<PlaylistSong> PlaylistsSongs { get; set; }

        public DbSet<PlaylistGenre> PlaylistsGenres { get; set; }

        public DbSet<Syncronization> Syncronisations { get; set; }

        public DbSet<Song> Songs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);
            modelBuilder.Entity<User>().ToTable("Users", "dbo");
        }
    }
}
