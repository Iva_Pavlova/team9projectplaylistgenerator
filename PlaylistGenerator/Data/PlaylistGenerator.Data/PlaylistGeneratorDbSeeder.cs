﻿using Microsoft.AspNetCore.Identity;
using PlaylistGenerator.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PlaylistGenerator.Data
{
    public static class PlaylistGeneratorDbSeeder
    {
        private static readonly Random random = new Random();

        private static readonly List<string> listFirstNamesMale = new List<string>()
            {
                "James",
                "Michael",
                "Robert",
                "John",
                "David",
                "William",
                "Richard",
                "Thomas",
                "Mark",
                "Charles", // 10
                "Steven",
                "Gary",
                "Joseph",
                "Donald",
                "Ronald",
                "Kenneth",
                "Paul",
                "Larry",
                "Daniel",
                "Stephen", // 20
                "Dennis",
                "Timothy",
                "Edward",
                "Jeffrey",
                "George",
                "Gregory",
                "Kevin",
                "Douglas",
                "Terry",
                "Anthony", // 30
                "Jerry",
                "Bruce",
                "Randy",
                "Brian",
                "Frank",
                "Scott",
                "Roger",
                "Raymond",
                "Peter",
                "Patrick", // 40
                "Keith",
                "Lawrence",
                "Wayne",
                "Danny",
                "Alan",
                "Gerald",
                "Ricky",
                "Carl",
                "Christopher",
                "Dale" // 50
            };

        private static readonly List<string> listFirstNamesFemale = new List<string>()
            {
                "Mary",
                "Linda",
                "Patricia",
                "Susan",
                "Deborah",
                "Barbara",
                "Debra",
                "Karen",
                "Nancy",
                "Donna", // 10
                "Cynthia",
                "Sandra",
                "Pamela",
                "Sharon",
                "Kathleen",
                "Carol",
                "Diane",
                "Brenda",
                "Cheryl",
                "Janet", // 20
                "Elizabeth",
                "Kathy",
                "Margaret",
                "Janice",
                "Carolyn",
                "Denise",
                "Judy",
                "Rebecca",
                "Joyce",
                "Teresa", // 30
                "Christine",
                "Catherine",
                "Shirley",
                "Judith",
                "Betty",
                "Beverly",
                "Lisa",
                "Laura",
                "Theresa",
                "Connie", // 40
                "Ann",
                "Gloria",
                "Julie",
                "Gail",
                "Joan",
                "Paula",
                "Peggy",
                "Cindy",
                "Martha",
                "Bonnie", // 50
            };

        private static readonly List<string> listLastNames = new List<string>()
            {
                "Smith",
                "Johnson",
                "Williams",
                "Brown",
                "Jones",
                "Garcia",
                "Miller",
                "Davis",
                "Rodriguez",
                "Martinez", // 10
                "Hernandez",
                "Lopez",
                "Gonzalez",
                "Wilson",
                "Anderson",
                "Taylor",
                "Thomas",
                "Moore",
                "Jackson",
                "Martin", // 20
                "Lee",
                "Perez",
                "Thompson",
                "White",
                "Harris",
                "Sanchez",
                "Clark",
                "Ramirez",
                "Lewis",
                "Robinson", // 30
                "Walker",
                "Young",
                "Allen",
                "King",
                "Wright",
                "Scott",
                "Torres",
                "Nguyen",
                "Hill",
                "Flores", // 40
                "Green",
                "Adams",
                "Nelson",
                "Baker",
                "Hall",
                "Rivera",
                "Campbell",
                "Mitchell",
                "Carter",
                "Roberts", // 50
            };


        public static void SeedPlaylists(PlaylistGeneratorDbContext db)
        {
            Console.WriteLine("SeedPlaylists started.");

            int numberGenresTotal = db.Genres.Count();

            // Generate 20 new Playlists.
            for (int i = 1; i <= 50; i++)
            {
                DateTime dateCreated = DateTime.UtcNow.AddYears(random.Next(-5, 1)).AddMonths(random.Next(-12, 0)).AddDays(random.Next(-31, 0));
                int durationToDubTotal = random.Next((10 /*minutes*/ * 60 /*seconds*/), (10 /*hours*/ * 60 /*minutes*/ * 60 /*seconds*/ + 1));
                int durationDubbedCurrent = 0;

                // Add new Playlist to Database.
                db.Playlists.Add(new Playlist()
                {
                    Name = $"Playlist {i}",
                    Description = $"Description {i}",
                    DateCreated = dateCreated,
                    DateModified = dateCreated,
                    DurationTravel = durationToDubTotal,
                    IsDeleted = false,
                    IsUnlisted = false,
                    Image = $"/images/playlists/playlist_{random.Next(0, 6)}.jpg",
                    UserId = db.Users.Skip(random.Next(0, db.Users.Count())).First().Id,
                });
                db.SaveChanges();

                Playlist playlistCreated = db.Playlists.First(x => x.Name == $"Playlist {i}");
                Guid playlistId = playlistCreated.Id;

                // Generate a list of Genres' Ids.
                HashSet<Guid> collectionGenresIdsToUse = new HashSet<Guid>();
                int countGenresToUse = random.Next(1, numberGenresTotal);

                while (collectionGenresIdsToUse.Count != countGenresToUse)
                {
                    collectionGenresIdsToUse.Add(db.Genres.Skip(random.Next(0, countGenresToUse)).FirstOrDefault().Id);
                }

                while (true)
                {
                    if (durationDubbedCurrent >= durationToDubTotal - (5 * 60) && durationDubbedCurrent <= durationToDubTotal + (5 * 60))
                    {
                        break;
                    }

                    Guid randomGenreId = collectionGenresIdsToUse.Skip(random.Next(0, countGenresToUse)).First();

                    var collectionSongsOfThisGenre = db.Songs.Where(x => x.GenreId == randomGenreId).Select(x => x.Id);
                    Guid randomSongId = collectionSongsOfThisGenre.Skip(random.Next(0, collectionSongsOfThisGenre.Count())).First();

                    if (!db.PlaylistsGenres.Where(x => x.PlaylistId == playlistId).Any(b => b.GenreId == randomGenreId))
                    {
                        db.PlaylistsGenres.Add(new PlaylistGenre
                        {
                            GenreId = randomGenreId,
                            PlaylistId = playlistId
                        });
                        db.SaveChanges();
                    }

                    if (!db.PlaylistsSongs.Where(x => x.PlaylistId == playlistId).Any(b => b.SongId == randomSongId))
                    {
                        db.PlaylistsSongs.Add(new PlaylistSong
                        {
                            PlaylistId = playlistId,
                            SongId = randomSongId
                        });
                        db.SaveChanges();
                        durationDubbedCurrent += db.Songs.First(x => x.Id == randomSongId).Duration;
                    }
                }

                var countSongsInPlaylist = db.PlaylistsSongs.Where(x => x.PlaylistId == playlistId).Count();
                playlistCreated.Description = $"'{playlistCreated.Name}' consists of {countSongsInPlaylist} songs that will make your journey from point A to point B sound like {playlistCreated.DurationTravel / 60} minutes in Heaven...";
                db.SaveChanges();
            }

            Console.WriteLine("SeedPlaylists completed!");
        }

        public static void SeedRoles(PlaylistGeneratorDbContext db)
        {
            Console.WriteLine("SeedRoles started.");

            db.Roles.Add(new Role
            {
                Name = "Admin",
                NormalizedName = "ADMIN",
            });
            db.SaveChanges();

            db.Roles.Add(new Role
            {
                Name = "UserManager",
                NormalizedName = "USERMANAGER"
            });
            db.SaveChanges();

            db.Roles.Add(new Role
            {
                Name = "User",
                NormalizedName = "USER"
            });
            db.SaveChanges();

            Console.WriteLine("SeedRoles completed!");
        }

        public static void SeedUsersRoleAdmin(PlaylistGeneratorDbContext db)
        {
            Console.WriteLine($"SeedUsersRoleAdmin started.");

            var hasher = new PasswordHasher<User>();

            db.Users.Add(new User
            {
                DateCreated = DateTime.UtcNow,
                DateModified = DateTime.UtcNow,
                FirstName = "Iva",
                LastName = "Pavlova",
                UserName = "nrgetic13@gmail.com",
                NormalizedUserName = "NRGETIC13@GMAIL.COM",
                Email = "nrgetic13@gmail.com",
                NormalizedEmail = "NRGETIC13@GMAIL.COM",
                EmailConfirmed = true,
                Image = "/images/users/user_profile_ivapavlova.jpg",
                IsDeleted = false,
                IsBanned = false,
                IsAdmin = true,
                LockoutEnabled = false,
                SecurityStamp = String.Concat(Array.ConvertAll(Guid.NewGuid().ToByteArray(), b => b.ToString("X2"))),
                ApiKey = Guid.NewGuid(),
            });
            db.SaveChanges();

            var ivaJustCreated = db.Users.FirstOrDefault(x => x.Email == "nrgetic13@gmail.com");
            ivaJustCreated.PasswordHash = hasher.HashPassword(ivaJustCreated, "Iva@2");
            db.SaveChanges();

            db.UserRoles.Add(new Microsoft.AspNetCore.Identity.IdentityUserRole<Guid>
            {
                RoleId = db.Roles.FirstOrDefault(x => x.Name == "Admin").Id,
                UserId = ivaJustCreated.Id,
            });
            db.SaveChanges();

            db.Users.Add(new User
            {
                DateCreated = DateTime.UtcNow,
                DateModified = DateTime.UtcNow,
                FirstName = "Petar",
                LastName = "Totev",
                UserName = "petar@petartotev.net",
                NormalizedUserName = "PETAR@PETARTOTEV.NET",
                Email = "petar@petartotev.net",
                NormalizedEmail = "PETAR@PETARTOTEV.NET",
                EmailConfirmed = true,
                Image = "/images/users/user_profile_petartotev.jpg",
                IsDeleted = false,
                IsBanned = false,
                IsAdmin = true,
                LockoutEnabled = false,
                SecurityStamp = String.Concat(Array.ConvertAll(Guid.NewGuid().ToByteArray(), b => b.ToString("X2"))),
                ApiKey = Guid.NewGuid(),
            });
            db.SaveChanges();

            var petarJustCreated = db.Users.FirstOrDefault(x => x.Email == "petar@petartotev.net");
            petarJustCreated.PasswordHash = hasher.HashPassword(petarJustCreated, "Petar@2");
            db.SaveChanges();

            db.UserRoles.Add(new Microsoft.AspNetCore.Identity.IdentityUserRole<Guid>
            {
                RoleId = db.Roles.FirstOrDefault(x => x.Name == "Admin").Id,
                UserId = petarJustCreated.Id,
            });
            db.SaveChanges();

            Console.WriteLine($"SeedUsersRoleAdmin completed!");
        }

        public static void SeedUsersRoleUser(PlaylistGeneratorDbContext db)
        {
            Console.WriteLine("SeedUsersRoleUser started.");

            var hasher = new PasswordHasher<User>();

            for (int i = 1; i <= 100; i++)
            {
                int gender = random.Next(0, 2);
                string image = gender == 0 ? $"/images/users/user_profile_man_{random.Next(0, 11)}.jpg" : $"/images/users/user_profile_woman_{random.Next(0, 11)}.jpg";
                string firstName = GetRandomFirstName(gender);
                string lastName = GetRandomLastName();
                string password = firstName.First().ToString().ToUpper() + firstName.Substring(1).ToLowerInvariant().Replace(" ", "") + "@1";
                string email = (firstName + lastName).ToLowerInvariant().Replace(" ", "") + "@email.com";
                DateTime dateCreated = DateTime.UtcNow.AddYears(random.Next(-5, 1)).AddMonths(random.Next(-12, 0)).AddDays(random.Next(-31, 0));

                if (db.Users.Any(x => x.Email == email))
                {
                    i--;
                }
                else
                {
                    db.Users.Add(new User
                    {
                        DateCreated = dateCreated,
                        DateModified = dateCreated,
                        FirstName = firstName,
                        LastName = lastName,
                        UserName = email,
                        NormalizedUserName = email.ToUpperInvariant(),
                        Email = email,
                        NormalizedEmail = email.ToUpperInvariant(),
                        EmailConfirmed = true,
                        Image = image,
                        IsDeleted = false,
                        IsBanned = false,
                        IsAdmin = false,
                        LockoutEnabled = true,
                        SecurityStamp = String.Concat(Array.ConvertAll(Guid.NewGuid().ToByteArray(), b => b.ToString("X2"))),
                    });
                    db.SaveChanges();

                    User userCreated = db.Users.FirstOrDefault(x => x.Email == email);
                    userCreated.PasswordHash = hasher.HashPassword(userCreated, password);
                    db.SaveChanges();

                    db.UserRoles.Add(new Microsoft.AspNetCore.Identity.IdentityUserRole<Guid>
                    {
                        RoleId = db.Roles.FirstOrDefault(x => x.Name == "User").Id,
                        UserId = userCreated.Id,
                    });
                    db.SaveChanges();
                }
            }

            Console.WriteLine("SeedUsersRoleUser completed!");
        }

        private static string GetRandomFirstName(int gender)
        {
            string firstName;

            if (gender == 0)
            {
                firstName = listFirstNamesMale[random.Next(0, listFirstNamesMale.Count)];
            }
            else
            {
                firstName = listFirstNamesFemale[random.Next(0, listFirstNamesFemale.Count)];
            }

            return firstName;
        }

        private static string GetRandomLastName()
        {
            return listLastNames[random.Next(0, listLastNames.Count)];
        }
    }
}
