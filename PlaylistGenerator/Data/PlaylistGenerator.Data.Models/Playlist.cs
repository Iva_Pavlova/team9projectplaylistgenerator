﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PlaylistGenerator.Data.Models
{
    public class Playlist
    {
        public Guid Id { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Name { get; set; }

        [MinLength(2)]
        [MaxLength(500)]
        public string Description { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        public string Image { get; set; }

        [Range(0, 31536000)]
        public int DurationTravel { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsUnlisted { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public ICollection<PlaylistSong> Songs { get; set; } = new HashSet<PlaylistSong>();

        public ICollection<PlaylistGenre> Genres { get; set; } = new HashSet<PlaylistGenre>();
    }
}
