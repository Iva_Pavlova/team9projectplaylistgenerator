﻿using System;

namespace PlaylistGenerator.Data.Models
{
    public class PlaylistSong
    {
        public Playlist Playlist { get; set; }
        public Guid PlaylistId { get; set; }

        public Song Song { get; set; }
        public Guid SongId { get; set; }
    }
}
