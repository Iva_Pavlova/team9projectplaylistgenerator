﻿namespace PlaylistGenerator.Data.Models.BingApiObjects
{
    public class DistanceOrigin
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
