﻿namespace PlaylistGenerator.Data.Models.BingApiObjects
{
    public class DistanceDestination
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
