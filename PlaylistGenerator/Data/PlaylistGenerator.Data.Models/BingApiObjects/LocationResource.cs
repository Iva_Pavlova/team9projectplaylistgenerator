﻿using System.Collections.Generic;

namespace PlaylistGenerator.Data.Models.BingApiObjects
{
    public class LocationResource
    {
        public string __type { get; set; }
        public List<double> bbox { get; set; }
        public string name { get; set; }
        public LocationPoint point { get; set; }
        public LocationAddress address { get; set; }
        public string confidence { get; set; }
        public string entityType { get; set; }
        public List<LocationGeocodePoint> geocodePoints { get; set; }
        public List<string> matchCodes { get; set; }
    }
}
