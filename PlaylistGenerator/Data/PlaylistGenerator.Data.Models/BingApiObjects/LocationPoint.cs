﻿using System.Collections.Generic;

namespace PlaylistGenerator.Data.Models.BingApiObjects
{
    public class LocationPoint
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }
}
