﻿namespace PlaylistGenerator.Data.Models.BingApiObjects
{
    public class LocationAddress
    {
        public string addressLine { get; set; }
        public string adminDistrict { get; set; }
        public string countryRegion { get; set; }
        public string formattedAddress { get; set; }
        public string locality { get; set; }
        public string postalCode { get; set; }
    }
}
