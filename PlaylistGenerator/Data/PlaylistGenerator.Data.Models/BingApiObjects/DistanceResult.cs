﻿namespace PlaylistGenerator.Data.Models.BingApiObjects
{
    public class DistanceResult
    {
        public int destinationIndex { get; set; }
        public int originIndex { get; set; }
        public int totalWalkDuration { get; set; }
        public double travelDistance { get; set; }
        public double travelDuration { get; set; }
    }
}
