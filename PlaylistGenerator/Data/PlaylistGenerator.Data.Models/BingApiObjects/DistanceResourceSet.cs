﻿using System.Collections.Generic;

namespace PlaylistGenerator.Data.Models.BingApiObjects
{
    public class DistanceResourceSet
    {
        public int estimatedTotal { get; set; }
        public List<DistanceResource> resources { get; set; }
    }
}
