﻿using System.Collections.Generic;

namespace PlaylistGenerator.Data.Models.BingApiObjects
{
    public class DistanceResource
    {
        public string __type { get; set; }
        public List<DistanceDestination> destinations { get; set; }
        public string errorMessage { get; set; }
        public List<DistanceOrigin> origins { get; set; }
        public List<DistanceResult> results { get; set; }
    }
}
