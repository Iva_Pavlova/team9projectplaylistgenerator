﻿using System.Collections.Generic;

namespace PlaylistGenerator.Data.Models.BingApiObjects
{
    public class LocationGeocodePoint
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
        public string calculationMethod { get; set; }
        public List<string> usageTypes { get; set; }
    }
}
