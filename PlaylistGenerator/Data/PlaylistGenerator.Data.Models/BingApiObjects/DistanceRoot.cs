﻿using System.Collections.Generic;

namespace PlaylistGenerator.Data.Models.BingApiObjects
{
    public class DistanceRoot
    {
        public string authenticationResultCode { get; set; }
        public string brandLogoUri { get; set; }
        public string copyright { get; set; }
        public List<DistanceResourceSet> resourceSets { get; set; }
        public int statusCode { get; set; }
        public string statusDescription { get; set; }
        public string traceId { get; set; }
    }
}
