﻿using System.Collections.Generic;

namespace PlaylistGenerator.Data.Models.BingApiObjects
{
    public class LocationResourceSet
    {
        public int estimatedTotal { get; set; }
        public List<LocationResource> resources { get; set; }
    }
}
