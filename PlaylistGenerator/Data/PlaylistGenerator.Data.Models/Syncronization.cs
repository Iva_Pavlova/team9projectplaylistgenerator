﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PlaylistGenerator.Data.Models
{
    public class Syncronization
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime? Date { get; set; }

        [Required]
        public bool IsComplete { get; set; }

        public bool WasInterrupted { get; set; }
    }
}
