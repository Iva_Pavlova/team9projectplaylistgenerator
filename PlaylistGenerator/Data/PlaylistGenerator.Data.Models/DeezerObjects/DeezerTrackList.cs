﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PlaylistGenerator.Data.Models.DeezerObjects
{
    public class DeezerTrackList
    {
        [JsonProperty("data")]
        public List<Song> Songs { get; set; }
    }
}
