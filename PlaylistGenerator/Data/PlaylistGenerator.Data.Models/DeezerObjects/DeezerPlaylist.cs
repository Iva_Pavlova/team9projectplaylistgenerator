﻿using Newtonsoft.Json;

namespace PlaylistGenerator.Data.Models.DeezerObjects
{
    public class DeezerPlaylist
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("tracklist")]
        public string SongListURL { get; set; }
    }
}
