﻿using Newtonsoft.Json;
using PlaylistGenerator.Data.Models.DeezerObjects;
using System.Collections.Generic;

namespace PlaylistGenerator.Data.Models
{
    public class DeezerPlaylistData
    {
        [JsonProperty("data")]
        public List<DeezerPlaylist> Playlists { get; set; }
    }
}
