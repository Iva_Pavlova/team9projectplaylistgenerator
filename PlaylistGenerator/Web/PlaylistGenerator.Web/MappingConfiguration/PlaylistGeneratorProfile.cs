﻿using AutoMapper;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Models;
using PlaylistGenerator.Services.Models.Album;
using PlaylistGenerator.Services.Models.Artist;
using PlaylistGenerator.Services.Models.Genre;
using PlaylistGenerator.Services.Models.Song;
using PlaylistGenerator.Services.Models.Syncronization;
using PlaylistGenerator.Services.Models.User;
using PlaylistGenerator.Web.Models.Album;
using PlaylistGenerator.Web.Models.Artist;
using PlaylistGenerator.Web.Models.Genre;
using PlaylistGenerator.Web.Models.News;
using PlaylistGenerator.Web.Models.Playlist;
using PlaylistGenerator.Web.Models.Song;
using PlaylistGenerator.Web.Models.Syncronization;
using PlaylistGenerator.Web.Models.User;
using System.Linq;

namespace PlaylistGenerator.Services.MappingConfiguration
{
    public class PlaylistGeneratorProfile : Profile
    {
        public PlaylistGeneratorProfile()
        {
            this.CreateMap<Playlist, PlaylistServiceModel>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => $"{src.User.UserName}"))
                .ForMember(dest => dest.UserImage, opt => opt.MapFrom(src => src.User.Image))
                .ForMember(dest => dest.DurationPlaylist, opt => opt.MapFrom(src => src.Songs.Sum(x => x.Song.Duration)))
                .ForMember(dest => dest.Rank, opt => opt.MapFrom(src => (src.Songs.Select(playlistSong => playlistSong.Song.Rank).Average())));

            this.CreateMap<PlaylistServiceModel, Playlist>()
                .ForMember(dest => dest.User, opt => opt.Ignore());

            this.CreateMap<Genre, GenreServiceModel>();

            this.CreateMap<User, UserServiceModel>().ReverseMap();

            this.CreateMap<Song, SongServiceModel>()
                .ForMember(dest => dest.Album, opt => opt.MapFrom(src => src.Album.Name))
                .ForMember(dest => dest.Artist, opt => opt.MapFrom(src => src.Artist.Name))
                .ForMember(dest => dest.Genre, opt => opt.MapFrom(src => src.Genre.Name))
                .ReverseMap();

            this.CreateMap<Album, AlbumServiceModel>()
                .ForMember(dest => dest.Artist, opt => opt.MapFrom(src => src.Artist.Name));

            this.CreateMap<Artist, ArtistServiceModel>();

            this.CreateMap<Syncronization, SyncronizationServiceModel>();
            this.CreateMap<SyncronizationServiceModel, SyncronizationViewModel>();

            this.CreateMap<AlbumServiceModel, AlbumFullViewModel>().ReverseMap();
            this.CreateMap<AlbumServiceModel, AlbumLightViewModel>().ReverseMap();

            this.CreateMap<ArtistServiceModel, ArtistFullViewModel>().ReverseMap();
            this.CreateMap<ArtistServiceModel, ArtistMediumViewModel>().ReverseMap();
            this.CreateMap<ArtistServiceModel, ArtistLightViewModel>().ReverseMap();

            this.CreateMap<GenreServiceModel, GenreFullViewModel>().ReverseMap();

            this.CreateMap<PlaylistServiceModel, PlaylistFullViewModel>().ReverseMap();
            this.CreateMap<PlaylistServiceModel, PlaylistLightViewModel>().ReverseMap();

            this.CreateMap<SongServiceModel, SongFullViewModel>().ReverseMap();
            this.CreateMap<SongServiceModel, SongMediumServiceModel>().ReverseMap();
            this.CreateMap<SongServiceModel, SongLightViewModel>().ReverseMap();

            this.CreateMap<UserServiceModel, UserFullViewModel>().ReverseMap();

            this.CreateMap<NewsServiceModel, NewsViewModel>()
                .ForMember(dest => dest.Publisher, opt => opt.MapFrom(src => src.Publisher.Name));
        }
    }
}
