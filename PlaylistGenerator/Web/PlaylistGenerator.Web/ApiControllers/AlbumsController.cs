﻿using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.ApiControllers
{
    public class AlbumsController : ControllerBase
    {
        private readonly IAlbumService albumService;

        public AlbumsController(IAlbumService albumService)
        {
            this.albumService = albumService;
        }

        [HttpGet]
        [Route("api/Albums/{id}")]
        public async Task<IActionResult> Get(Guid id, [FromQuery] Guid? apiKey)
        {
            var album = await this.albumService.GetAlbumByIdAsync(id, true, apiKey);
            return Ok(album);
        }
    }
}
