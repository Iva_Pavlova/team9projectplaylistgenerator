﻿using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Models;
using PlaylistGenerator.Services.Models.Playlist;
using PlaylistGenerator.Services.Models.User;
using PlaylistGenerator.Web.Controllers.Extensions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.ApiControllers
{
    [ApiController]
    public class PlaylistsController : ControllerBase
    {
        private readonly IPlaylistService playlistService;
        private readonly IUserService userService;

        public PlaylistsController(IPlaylistService playlistService, IUserService userService)
        {
            this.playlistService = playlistService;
            this.userService = userService;
        }

        [Route("api/Playlists/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id, [FromQuery] Guid? apiKey = null)
        {
            bool isPlaylistDeleted = await playlistService.DeletePlaylistAsync(id, true, apiKey);

            if (!isPlaylistDeleted)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpGet]
        [Route("api/Playlists")]
        public async Task<IActionResult> Get([FromQuery] Guid? apiKey = null)
        {
            var playlists = await playlistService.GetAllPlaylistsAsync(true, apiKey);

            return Ok(playlists);
        }

        [Route("api/Playlists/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get(Guid id, [FromQuery] Guid? apiKey = null)
        {
            PlaylistServiceModel playlist = await playlistService.GetPlaylistByIdAsync(id, true, apiKey);

            return Ok(playlist);
        }

        [Route("api/Playlists")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PlaylistCreateServiceModel input, [FromQuery] Guid? apiKey = null)
        {
            UserServiceModel userServiceModel = await userService.GetUserByApiKeyAsync(apiKey);

            // Create new Playlist in Database (no Image, no DurationTravel).
            PlaylistServiceModel playlistToCreate = new PlaylistServiceModel
            {
                Name = input.Name,
                Description = input.Description,
                UserId = userServiceModel.Id,
                UserName = userServiceModel.FirstName + " " + userServiceModel.LastName,
                UserImage = userServiceModel.Image,
            };
            PlaylistServiceModel playlistCreated = await playlistService.CreatePlaylistAsync(playlistToCreate);

            // Upload image and save it in wwwroot.
            await ControllerExtensionMethods.UploadImageFile(playlistToCreate.Id, "playlists", null, true);

            // Set image path to Playlist as Image property.
            await playlistService.UpdatePlaylistImageAsync(playlistCreated.Id);

            //Get DurationTravel from Bing Location API.
            int durationTravelHardcode = await playlistService.GetDurationTravelAsync(input.PointFirstQueryString, input.PointSecondQueryString);
            await playlistService.UpdatePlaylistDurationTravelAsync(playlistCreated.Id, durationTravelHardcode);

            // Get Genres From input parameter.
            var genresIdsChosenByUser = input.Genres.Select(y => y.Id);

            bool isMissionAccomplished = await playlistService.GeneratePlaylistAsync(playlistCreated.Id, durationTravelHardcode, genresIdsChosenByUser, input.IsTopTracksOptionEnabled, input.IsTracksFromSameArtistEnabled);

            return Ok(playlistCreated);
        }

        [Route("api/Playlists/{id}")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] PlaylistServiceModel model, [FromQuery] Guid? apiKey = null)
        {
            PlaylistServiceModel playlist = await playlistService.UpdatePlaylistAsync(model, true, apiKey);

            return Ok(playlist);
        }
    }
}
