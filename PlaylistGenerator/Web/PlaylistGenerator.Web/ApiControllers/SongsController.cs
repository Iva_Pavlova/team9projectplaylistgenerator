﻿using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.ApiControllers
{
    public class SongsController : ControllerBase
    {
        private readonly ISongService songService;

        public SongsController(ISongService songService)
        {
            this.songService = songService;
        }

        [HttpGet]
        [Route("api/Songs")]
        public async Task<IActionResult> Get([FromQuery] Guid? apiKey, [FromQuery] int page = 1)
        {
            var songs = await this.songService.GetAllSongsAsync(page, true, apiKey);
            return Ok(songs);
        }

        [HttpGet]
        [Route("api/Songs/{id}")]
        public async Task<IActionResult> Get(Guid id, [FromQuery] Guid? apiKey)
        {
            var song = await this.songService.GetSongByIdAsync(id, true, apiKey);
            return Ok(song);
        }

        [HttpGet]
        [Route("api/SongsByPlaylist/{playlistId}")]
        public async Task<IActionResult> GetSongsByPlaylist(Guid playlistId, [FromQuery] Guid? apiKey)
        {
            var songs = await this.songService.GetSongsByPlaylistIdAsync(playlistId, true, apiKey);
            return Ok(songs);
        }
    }
}
