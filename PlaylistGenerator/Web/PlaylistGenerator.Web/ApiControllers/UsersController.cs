﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Models.User;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.ApiControllers
{
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly UserManager<User> userManager;

        public UsersController(IUserService userService, UserManager<User> userManager)
        {
            this.userService = userService;
            this.userManager = userManager;
        }

        [Route("api/Users/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id, [FromQuery] Guid? apiKey = null)
        {
            bool isUserDeleted = await userService.DeleteUserAsync(id, true, apiKey);

            if (!isUserDeleted)
            {
                return BadRequest();
            }

            return Ok();
        }

        [Route("api/Users/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get(Guid id, [FromQuery] Guid? apiKey = null)
        {
            var userReceived = await userService.GetUserByIdAsync(id, true, apiKey);

            return Ok(userReceived);
        }

        [Route("api/Users/Register")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateUserServiceModel input)
        {
            if (ModelState.IsValid)
            {
                var userServiceModel = await this.userService.CreateUserAsync(input, this.userManager);

                if (userServiceModel == null)
                    return BadRequest();

                return Ok(userServiceModel);
            }

            return BadRequest();
        }

        [Route("api/Users/{id}")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] UserServiceModel model, [FromQuery] Guid? apiKey = null)
        {
            UserServiceModel userUpdated = await userService.UpdateUserAsync(model, true, apiKey);

            return Ok(userUpdated);
        }
    }
}
