﻿using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.ApiControllers
{
    public class ArtistsController : ControllerBase
    {
        private readonly IArtistService artistService;

        public ArtistsController(IArtistService artistService)
        {
            this.artistService = artistService;
        }

        [HttpGet]
        [Route("api/Artists/{id}")]
        public async Task<IActionResult> Get(Guid id, [FromQuery] Guid? apiKey)
        {
            var artist = await this.artistService.GetArtistByIdAsync(id, true, apiKey);
            return Ok(artist);
        }
    }
}
