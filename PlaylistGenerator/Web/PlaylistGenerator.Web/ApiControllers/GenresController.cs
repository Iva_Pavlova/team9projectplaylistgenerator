﻿using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.ApiControllers
{
    public class GenresController : ControllerBase
    {
        private readonly IGenreService genreService;

        public GenresController(IGenreService genreService)
        {
            this.genreService = genreService;
        }

        [HttpGet]
        [Route("api/Genres")]
        public async Task<IActionResult> Get([FromQuery] Guid? apiKey)
        {
            var genres = await this.genreService.GetAllGenresAsync(true, apiKey);
            return Ok(genres);
        }

        [HttpGet]
        [Route("api/Genres/{id}")]
        public async Task<IActionResult> Get(Guid id, [FromQuery] Guid? apiKey)
        {
            var genre = await this.genreService.GetGenreByIdAsync(id, true, apiKey);
            return Ok(genre);
        }
    }
}
