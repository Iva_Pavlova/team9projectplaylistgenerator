﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

// When the user scrolls the page, execute myFunction - used in Playlists/All and Users/All.
window.onscroll = function () { myFunction() };

function myFunction() {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    if (document.getElementById("myBar") != null) {
        document.getElementById("myBar").style.width = scrolled + "%";
    }
}

// Get Location and Get Coordinates function for the Playlists/Create view.
/* start GetLocation panel*/
function getLocation() {
    document.getElementById('buttonLocation').style.visibility = "hidden";

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getCoordinates);
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        console.log(latitude);
        console.log(longitude);
    }
    else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function getCoordinates(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;

    document.getElementById('point-a-input').value = latitude + ", " + longitude;
    document.getElementById('iframe-bing').src = "https://www.bing.com/maps/embed?h=400&w=600&cp=" + latitude + "~" + longitude + "&lvl=11&typ=d&sty=r&src=SHELL&FORM=MBEDV8";
    document.getElementById('iframe-bing').reload();
}
/* end GetLocation panel*/

// User profile has expandable table of playlists.
function expandListPlaylists() {
    var element = document.getElementById("table-playlists");
    if (element.style.visibility == "collapse") {
        element.style.visibility = "visible";
    }
    else {
        element.style.visibility = "collapse";
    }
}

// Playlist profile has expandable tables.
function expandList(tableId) {
    var element = document.getElementById(tableId);
    if (element.style.visibility == "collapse") {
        element.style.visibility = "visible";
    }
    else {
        element.style.visibility = "collapse";
    }
}

function displayBusyIndicator() {
    document.getElementById("loading").style.display = "block";
}

function disableUpdateGenresButton() {
    document.getElementById("updateGenresButton").disabled = true;
}

function play(elementId) {
    var btn = document.getElementById(elementId);
    var audio = document.getElementById(`audio-${elementId}`);

    if (btn.classList.contains("paused")) {
        audio.pause();
        btn.innerHTML = `<i class="tim-icons icon-triangle-right-17"></i>`;
        btn.classList.remove("paused");
    } else {
        audio.play();
        btn.innerHTML = `<i class="tim-icons icon-button-pause"></i>`;
        btn.classList.add("paused")
    }
    return false;
}

$(function () {

    //slider for duration here

    //initialize the slider
    $('#slider-range-duration').slider({
        range: true, //has range, so 2 values - start and end
        values: [0, 24], //the 2 values
        min: 0,
        max: 24,
        slide: function (event, ui) { //the event that sets all values when slider values change
            $("#duration-min").val(ui.values[0]); //set the hidden inputs
            $("#duration-max").val(ui.values[1]);
            $("#duration-min-span").html(ui.values[0]); //set the spans on the 2 sides of the slider
            $("#duration-max-span").html(ui.values[1]);
        }
    });

    //the initial hidden input value
    $("#duration-min").val($("#slider-range-duration").slider("option", "values")[0]);
    $("#duration-max").val($("#slider-range-duration").slider("option", "values")[1]);
    //the initial span values on document load 
    $("#duration-min-span").html($("#slider-range-duration").slider("option", "values")[0]);
    $("#duration-max-span").html($("#slider-range-duration").slider("option", "values")[1]);


    //slider for rank here
    $('#slider-range-rank').slider({
        range: true,
        values: [100000, 1000000],
        min: 100000,
        max: 1000000,
        step: 1000, // Determines the size or amount of each interval or step the slider takes between the min and max. 
        slide: function (event, ui) {
            $("#rank-min").val(ui.values[0]);
            $("#rank-max").val(ui.values[1]);
            $("#rank-min-span").html(ui.values[0]);
            $("#rank-max-span").html(ui.values[1]);
        }
    });

    $("#rank-min").val($("#slider-range-rank").slider("option", "values")[0]);
    $("#rank-max").val($("#slider-range-rank").slider("option", "values")[1]);

    $("#rank-min-span").html($("#slider-range-rank").slider("option", "values")[0]);
    $("#rank-max-span").html($("#slider-range-rank").slider("option", "values")[1]);
})

$(document).ready(function () {
    $().ready(function () {
        $sidebar = $('.sidebar');
        $navbar = $('.navbar');
        $main_panel = $('.main-panel');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');
        sidebar_mini_active = true;
        white_color = false;

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();



        $('.fixed-plugin a').click(function (event) {
            if ($(this).hasClass('switch-trigger')) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                } else if (window.event) {
                    window.event.cancelBubble = true;
                }
            }
        });

        $('.fixed-plugin .background-color span').click(function () {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');

            var new_color = $(this).data('color');

            if ($sidebar.length != 0) {
                $sidebar.attr('data', new_color);
            }

            if ($main_panel.length != 0) {
                $main_panel.attr('data', new_color);
            }

            if ($full_page.length != 0) {
                $full_page.attr('filter-color', new_color);
            }

            if ($sidebar_responsive.length != 0) {
                $sidebar_responsive.attr('data', new_color);
            }
        });

        $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function () {
            var $btn = $(this);

            if (sidebar_mini_active == true) {
                $('body').removeClass('sidebar-mini');
                sidebar_mini_active = false;
                blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
            } else {
                $('body').addClass('sidebar-mini');
                sidebar_mini_active = true;
                blackDashboard.showSidebarMessage('Sidebar mini activated...');
            }

            // we simulate the window Resize so the charts will get updated in realtime.
            var simulateWindowResize = setInterval(function () {
                window.dispatchEvent(new Event('resize'));
            }, 180);

            // we stop the simulation of Window Resize after the animations are completed
            setTimeout(function () {
                clearInterval(simulateWindowResize);
            }, 1000);
        });

        $('.switch-change-color input').on("switchChange.bootstrapSwitch", function () {
            var $btn = $(this);

            if (white_color == true) {

                $('body').addClass('change-background');
                setTimeout(function () {
                    $('body').removeClass('change-background');
                    $('body').removeClass('white-content');
                }, 900);
                white_color = false;
            } else {

                $('body').addClass('change-background');
                setTimeout(function () {
                    $('body').removeClass('change-background');
                    $('body').addClass('white-content');
                }, 900);

                white_color = true;
            }


        });

        $('.light-badge').click(function () {
            $('body').addClass('white-content');
        });

        $('.dark-badge').click(function () {
            $('body').removeClass('white-content');
        });
    });
});