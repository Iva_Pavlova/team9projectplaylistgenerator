﻿using PlaylistGenerator.Web.Models.Contracts;
using System;
using System.Collections.Generic;

namespace PlaylistGenerator.Web.Models.Song
{
    public class CollectionSongsLightViewModels : IPagingCollection
    {
        public string Url { get; set; }

        public IEnumerable<SongLightViewModel> Collection { get; set; }

        public int TotalCount { get; set; }

        public int PageSize { get; set; }

        public int CurrentPage { get; set; }

        public int PreviousPage
        {
            get
            {
                return this.CurrentPage - 1;
            }
        }
        public int NextPage
        {
            get
            {
                return this.CurrentPage + 1;
            }
        }

        public int FirstPage => 1;

        public int LastPage
        {
            get
            {
                return (int)Math.Ceiling((double)this.TotalCount / this.PageSize);
            }
        }

        public bool IsPreviousPageDisabled => this.CurrentPage == 1;
        public bool IsNextPageDisabled => this.CurrentPage == TotalCount;
    }
}
