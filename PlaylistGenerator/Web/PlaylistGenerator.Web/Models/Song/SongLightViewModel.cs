﻿using System;

namespace PlaylistGenerator.Web.Models.Song
{
    public class SongLightViewModel
    {
        public Guid Id { get; set; }

        public string DeezerId { get; set; }

        public string Name { get; set; }

        public string Artist { get; set; }

        public string SongURL { get; set; }

        public int Duration { get; set; }

        public string DurationInMinutes
        {
            get
            {
                int minutes = (int)Math.Floor((double)this.Duration / 60);
                int seconds = this.Duration % 60;

                string secondsString = seconds.ToString().PadLeft(2, '0');

                return $"{minutes}:{secondsString}";
            }
        }

        public string PreviewURL { get; set; }
    }
}
