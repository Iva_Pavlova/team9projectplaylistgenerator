﻿using System;

namespace PlaylistGenerator.Web.Models.Song
{
    public class SongMediumServiceModel
    {
        public Guid Id { get; set; }

        public string DeezerId { get; set; }

        public string Name { get; set; }

        public string SongURL { get; set; }

        public int Duration { get; set; }

        public string PreviewURL { get; set; }

        public string Album { get; set; }

        public string Artist { get; set; }
    }
}
