﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.Models
{
    public class SuccessViewModel
    {
        public string urlGeneral { get; set; }

        public string urlItemCreated { get; set; }
    }
}
