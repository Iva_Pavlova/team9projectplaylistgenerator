﻿using System;

namespace PlaylistGenerator.Web.Models.Artist
{
    public class ArtistLightViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string PictureURL { get; set; }
    }
}
