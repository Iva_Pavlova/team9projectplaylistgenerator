﻿using System;

namespace PlaylistGenerator.Web.Models.Artist
{
    public class ArtistMediumViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string ArtistPageURL { get; set; }

        public string PictureURL { get; set; }

        public string SongListURL { get; set; }
    }
}
