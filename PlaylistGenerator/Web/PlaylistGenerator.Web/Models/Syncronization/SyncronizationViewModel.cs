﻿using System;

namespace PlaylistGenerator.Web.Models.Syncronization
{
    public class SyncronizationViewModel
    {
        public Guid Id { get; set; }

        public DateTime? Date { get; set; }

        public bool IsComplete { get; set; }

        public bool WasInterrupted { get; set; }
    }
}
