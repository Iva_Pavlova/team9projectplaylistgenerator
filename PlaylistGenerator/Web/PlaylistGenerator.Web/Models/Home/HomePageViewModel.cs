﻿using PlaylistGenerator.Web.Models.News;
using PlaylistGenerator.Web.Models.Playlist;
using PlaylistGenerator.Web.Models.Song;
using System.Collections.Generic;

namespace PlaylistGenerator.Web.Models.Home
{
    public class HomePageViewModel
    {
        public IEnumerable<PlaylistLightViewModel> Top3Playlists { get; set; }
        public IEnumerable<SongLightViewModel> Top3Songs { get; set; }

        public IEnumerable<NewsViewModel> Top3News { get; set; }
    }
}
