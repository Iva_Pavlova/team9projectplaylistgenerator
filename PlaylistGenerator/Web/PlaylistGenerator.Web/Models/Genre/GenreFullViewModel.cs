﻿using System;

namespace PlaylistGenerator.Web.Models.Genre
{
    public class GenreFullViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string PictureURL { get; set; }

        public bool IsSelected { get; set; }
    }
}
