﻿using System.Collections.Generic;

namespace PlaylistGenerator.Web.Models.Genre
{
    public class CollectionGenresFullViewModels
    {
        public IEnumerable<GenreFullViewModel> Collection { get; set; }
    }
}
