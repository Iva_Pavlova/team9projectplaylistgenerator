﻿using Microsoft.AspNetCore.Http;
using PlaylistGenerator.Web.Models.Contracts;
using PlaylistGenerator.Web.Models.Genre;
using PlaylistGenerator.Web.Models.Song;
using System;
using System.Collections.Generic;

namespace PlaylistGenerator.Web.Models.Playlist
{
    public class PlaylistFullViewModel : IDeletable
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        public string Image { get; set; }

        public double Rank { get; set; }

        public int DurationPlaylist { get; set; }

        public int DurationTravel { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsUnlisted { get; set; }

        public int GenresCount { get; set; }

        public int SongsCount { get; set; }

        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string UserImage { get; set; }

        public IEnumerable<SongLightViewModel> Songs { get; set; }

        public IEnumerable<SongLightViewModel> SongsTop3 { get; set; }

        public IEnumerable<GenreFullViewModel> Genres { get; set; }

        public IEnumerable<PlaylistFullViewModel> PlaylistsSimilar { get; set; }

        public IFormFile ImageFile { get; set; }

        public string NameController { get; set; }

        public bool IsToBeDeletedByAdmin { get; set; }

        public int CurrentPage { get; set; }
    }
}
