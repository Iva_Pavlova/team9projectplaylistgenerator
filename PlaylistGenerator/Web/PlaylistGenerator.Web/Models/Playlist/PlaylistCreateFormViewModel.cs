﻿using Microsoft.AspNetCore.Http;
using PlaylistGenerator.Web.Models.Genre;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PlaylistGenerator.Web.Models.Playlist
{
    public class PlaylistCreateFormViewModel
    {
        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        public IFormFile ImageFile { get; set; }

        public bool IsTopTracksOptionEnabled { get; set; }

        public bool IsTracksFromSameArtistEnabled { get; set; }

        public List<GenreFullViewModel> Genres { get; set; }

        public List<Guid> GenresIdChosenByUser { get; set; }

        public string PointFirstQueryString { get; set; }

        public string PointSecondQueryString { get; set; }
    }
}
