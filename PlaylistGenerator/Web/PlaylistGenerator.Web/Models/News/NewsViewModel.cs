﻿namespace PlaylistGenerator.Web.Models.News
{
    public class NewsViewModel
    {
        public string Title { get; set; }

        public string Publisher { get; set; }

        public string Content { get; set; }

        public string Link { get; set; }

        public string ImageUrl { get; set; }
    }
}
