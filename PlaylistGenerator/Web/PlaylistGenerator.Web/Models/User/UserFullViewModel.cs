﻿using PlaylistGenerator.Web.Models.Contracts;
using PlaylistGenerator.Web.Models.Genre;
using PlaylistGenerator.Web.Models.Playlist;
using System;
using System.Collections.Generic;

namespace PlaylistGenerator.Web.Models.User
{
    public class UserFullViewModel : IDeletable
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        public string Image { get; set; }

        public Guid ApiKey { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsBanned { get; set; }

        public bool IsAdmin { get; set; }

        public int PlaylistsCount { get; set; }

        public IEnumerable<PlaylistLightViewModel> Playlists { get; set; }

        public IEnumerable<GenreFullViewModel> GenresPreferred { get; set; }
        public string Name { get; set; }
        public string NameController { get; set; }
        public bool IsToBeDeletedByAdmin { get; set; }
        public int CurrentPage { get; set; }
    }
}
