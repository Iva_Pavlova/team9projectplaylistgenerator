﻿using System;

namespace PlaylistGenerator.Web.Models.Album
{
    public class AlbumFullViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string SongListUrl { get; set; }

        public string Artist { get; set; }
        public Guid ArtistId { get; set; }
    }
}
