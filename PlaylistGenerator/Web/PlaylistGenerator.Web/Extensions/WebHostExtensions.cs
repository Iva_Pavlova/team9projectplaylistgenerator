﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Services;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.Extensions
{
    public static class WebHostExtensions
    {
        public static async Task<IHost> SeedData(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = (PlaylistGeneratorDbContext)services.GetService(typeof(PlaylistGeneratorDbContext));
                var mapper = (IMapper)services.GetService(typeof(IMapper));

                // We have the DbContext. Run migrations:
                context.Database.Migrate();

                // The database is up to date. Let's seed:
                var genreService = new GenreService(context, mapper);
                var songService = new SongService(context, mapper);

                await genreService.LoadGenresInDbAsync();
                await songService.LoadSongsInDbAsync();
            }

            return host;
        }
    }
}
