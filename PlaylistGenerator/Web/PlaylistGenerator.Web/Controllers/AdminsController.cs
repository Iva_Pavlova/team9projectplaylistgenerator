﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Web.Models.Playlist;
using PlaylistGenerator.Web.Models.Syncronization;
using PlaylistGenerator.Web.Models.User;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminsController : Controller
    {
        private readonly IUserService userService;
        private readonly IPlaylistService playlistService;
        private readonly IGenreService genreService;
        private readonly ISongService songService;
        private readonly ISyncronizationService syncronizationService;
        private readonly IMapper mapper;
        private readonly UserManager<User> userManager;


        public AdminsController(IUserService userService, IPlaylistService playlistService, IGenreService genreService, ISongService songService, ISyncronizationService syncronizationService, UserManager<User> userManager, IMapper mapper)
        {
            this.userService = userService;
            this.playlistService = playlistService;
            this.genreService = genreService;
            this.songService = songService;
            this.syncronizationService = syncronizationService;
            this.userManager = userManager;
            this.mapper = mapper;
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Genres()
        {
            var lastSyncronization = await this.syncronizationService.GetLastSyncronizationAsync();
            var model = this.mapper.Map<SyncronizationViewModel>(lastSyncronization);
            return await Task.Run(() => View(model));
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateGenres()
        {
            await this.syncronizationService.SyncronizeGenresAndSongsAsync();
            var lastSyncronization = await this.syncronizationService.GetLastSyncronizationAsync();
            var model = this.mapper.Map<SyncronizationViewModel>(lastSyncronization);

            return this.View("Genres", model);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Playlists(int page = 1)
        {
            int pageCountSize = 12;

            var collectionPlaylistsServiceModels = await playlistService.GetAllPlaylistsAsync(false, null, true);

            int totalCount = collectionPlaylistsServiceModels.Count();

            var collectionPlaylistsServiceModelsByPage = collectionPlaylistsServiceModels
                .Skip((page - 1) * pageCountSize)
                .Take(pageCountSize);

            var model = new CollectionPlaylistLightViewModels
            {
                CollectionPlaylists = collectionPlaylistsServiceModelsByPage.Select(x => new PlaylistLightViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    DateCreated = x.DateCreated,
                    DateModified = x.DateModified,
                    GenresCount = x.GenresCount,
                    SongsCount = x.SongsCount,
                    IsUnlisted = x.IsUnlisted,
                    IsDeleted = x.IsDeleted,
                    Image = x.Image,
                    DurationPlaylist = x.DurationPlaylist,
                    DurationTravel = x.DurationTravel,
                    Rank = x.Rank,
                    UserId = x.UserId,
                    UserImage = x.UserImage,
                    UserName = x.UserName,
                }),
                PageSize = pageCountSize,
                CurrentPage = page,
                TotalCount = totalCount,
                Url = "/Admins/Playlists"
            };

            return await Task.Run(() => View(model));
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Users(int page = 1)
        {
            var collectionUsersServiceModels = await userService.GetAllUsersAsync(true, page);
            var pageSizeCount = this.userService.GetPageCountSizing();
            var totalCount = this.userService.GetTotalUsersCount();

            var model = new CollectionUserFullViewModels
            {
                Users = collectionUsersServiceModels.Select(x => new UserFullViewModel
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    DateCreated = x.DateCreated,
                    DateModified = x.DateModified,
                    IsAdmin = x.IsAdmin,
                    IsBanned = x.IsBanned,
                    IsDeleted = x.IsDeleted,
                    Image = x.Image,
                    PlaylistsCount = x.PlaylistsCount,
                    ApiKey = x.ApiKey
                }),
                PageSize = pageSizeCount,
                CurrentPage = page,
                TotalCount = totalCount,
                Url = "/Admins/Users"
            };

            return this.View(model);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Welcome()
        {
            return await Task.Run(() => View());
        }
    }
}
