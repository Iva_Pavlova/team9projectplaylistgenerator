﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.Controllers.Extensions
{
    public static class ControllerExtensionMethods
    {
        public const string urlDevelopment = "../PlaylistGenerator.Web/wwwroot/images/";
        public const string urlProduction = "C:/home/site/wwwroot/wwwroot/images/";

        public static async Task<bool> UploadImageFile(Guid id, string type, IFormFile file, bool IsUploadedForTheFirstTime)
        {
            if (file == null)
            {
                if (IsUploadedForTheFirstTime)
                {
                    string sourceFile = urlDevelopment + $"{type}/default.jpg";
                    string destinationFile = urlDevelopment + $"{type}/" + id + ".jpg";
                    System.IO.File.Copy(sourceFile, destinationFile, true);
                    return true;
                }
            }
            else
            {
                using (var fileStream = new FileStream(urlDevelopment + $"{type}/" + id + ".jpg", FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                    return true;
                }
            }
            return false;
        }
    }
}