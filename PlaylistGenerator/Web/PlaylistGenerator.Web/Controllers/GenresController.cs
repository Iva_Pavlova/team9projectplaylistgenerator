﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Web.Models.Genre;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.Controllers
{
    public class GenresController : Controller
    {
        private readonly IGenreService genreService;
        private readonly IMapper mapper;

        public GenresController(IGenreService genreService, IMapper mapper)
        {
            this.genreService = genreService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> All()
        {
            var genres = await this.genreService.GetAllGenresAsync();

            var genreLightViewModels = this.mapper.Map<IEnumerable<GenreFullViewModel>>(genres);

            var model = new CollectionGenresFullViewModels
            {
                Collection = genreLightViewModels
            };

            return this.View(model);
        }
    }
}
