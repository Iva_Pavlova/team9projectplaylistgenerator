﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Models;
using PlaylistGenerator.Web.Controllers.Extensions;
using PlaylistGenerator.Web.Models;
using PlaylistGenerator.Web.Models.Genre;
using PlaylistGenerator.Web.Models.Playlist;
using PlaylistGenerator.Web.Models.Song;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.Controllers
{
    public class PlaylistsController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly IPlaylistService playlistService;
        private readonly IGenreService genreService;
        private readonly ISongService songService;
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public PlaylistsController(UserManager<User> userManager, SignInManager<User> signInManager, IPlaylistService playlistService, IGenreService genreService, ISongService songService, IUserService userService, IMapper mapper)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.playlistService = playlistService;
            this.genreService = genreService;
            this.songService = songService;
            this.userService = userService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> All(int page = 1)
        {
            int pageCountSize = 12;

            IEnumerable<PlaylistServiceModel> collectionPlaylistsServiceModels = await playlistService.GetAllPlaylistsAsync(false, null, false);

            int totalCount = collectionPlaylistsServiceModels.Count();

            var collectionPlaylistsServiceModelsByPage = collectionPlaylistsServiceModels
                .OrderByDescending(x => x.Rank)
                .Skip((page - 1) * pageCountSize)
                .Take(pageCountSize);

            CollectionPlaylistLightViewModels model = new CollectionPlaylistLightViewModels();
            model.CollectionPlaylists = this.mapper.Map<IEnumerable<PlaylistLightViewModel>>(collectionPlaylistsServiceModelsByPage);

            model.Url = "/Playlists/All";
            model.PageSize = pageCountSize;
            model.CurrentPage = page;
            model.TotalCount = totalCount;

            var genresServiceModels = await genreService.GetAllGenresAsync(false, null);
            model.Genres = this.mapper.Map<IEnumerable<GenreFullViewModel>>(genresServiceModels).ToList();

            return this.View(model);
        }

        [HttpPost]
        public async Task<IActionResult> All(CollectionPlaylistLightViewModels input)
        {
            IEnumerable<PlaylistServiceModel> playlists;

            if (input.NameToSearchForFilter != null)
            {
                // Filter By Name
                playlists = await playlistService.GetPlaylistsByNameContainsSubstring(input.NameToSearchForFilter);
            }
            else
            {
                playlists = await playlistService.GetAllPlaylistsAsync(false, null, false);

                // Filter By Duration
                if (input.DurationMinHoursFilter != 0 || input.DurationMaxHoursFilter != 0)
                {
                    playlists = playlistService.FilterByRange(playlists, "duration", input.DurationMinHoursFilter, input.DurationMaxHoursFilter);
                }

                // Filter By Rank
                if (input.RankMinFilter != 0 || input.RankMaxFilter != 0)
                {
                    playlists = playlistService.FilterByRange(playlists, "rank", input.RankMinFilter, input.RankMaxFilter);
                }

                // Filter By Genres
                if (input.Genres != null && input.Genres.Count() != 0)
                {
                    var genresIdsChosenByUser = input.Genres.Where(x => x.IsSelected == true).Select(y => y.Id);
                    playlists = playlistService.FilterByGenre(playlists, genresIdsChosenByUser);
                }

                // Sort
                string sortMethod = input.SortMethod.ToString().ToLowerInvariant();
                string sortOrder = input.SortOrder.ToString().ToLowerInvariant();

                if (sortMethod == "sort")
                {
                    sortMethod = "rank";
                }
                if (sortOrder == "order")
                {
                    sortOrder = "desc";
                }

                playlists = playlistService.Sort(playlists, sortMethod, sortOrder);
            }

            CollectionPlaylistLightViewModels model = new CollectionPlaylistLightViewModels();
            model.CollectionPlaylists = this.mapper.Map<IEnumerable<PlaylistLightViewModel>>(playlists);
            
            var genresServiceModels = await genreService.GetAllGenresAsync(false, null);
            model.Genres = this.mapper.Map<IEnumerable<GenreFullViewModel>>(genresServiceModels).ToList();

            model.CurrentPage = 1;
            model.Url = "/Playlists/All";

            return this.View(model);
        }

        public async Task<IActionResult> MyPlaylists(string email)
        {
            var user = await userService.GetUserByEmailAsync(email);
            var collectionPlaylistsByUserId = await playlistService.GetPlaylistsByUserIdAsync(user.Id);
            CollectionPlaylistLightViewModels model = new CollectionPlaylistLightViewModels();
            model.CollectionPlaylists = this.mapper.Map<IEnumerable<PlaylistLightViewModel>>(collectionPlaylistsByUserId);

            return this.View(model);
        }

        public async Task<IActionResult> PlaylistById(Guid id)
        {
            var playlistServiceModel = await playlistService.GetPlaylistByIdAsync(id);

            var songs = await songService.GetSongsByPlaylistIdAsync(playlistServiceModel.Id);
            var genres = await genreService.GetGenresByPlaylistId(playlistServiceModel.Id);

            var durationPlaylist = playlistServiceModel.DurationPlaylist;
            var durationRangeMin = durationPlaylist - (1 /*hour*/ * 60 /*minutes*/ * 60 /*seconds*/);
            var durationRangeMax = durationPlaylist = (1 /*hour*/ * 60 /*minutes*/ * 60 /*seconds*/);

            var playlistsAll = await playlistService.GetAllPlaylistsAsync();
            playlistsAll = playlistService.FilterByRange(playlistsAll, "duration", durationRangeMin, durationRangeMax);
            playlistsAll = playlistService.FilterByGenre(playlistsAll, genres.Select(x => x.Id));
            var playlistsSimilarTop5 = playlistsAll.Where(x => x.Id != playlistServiceModel.Id).OrderBy(x => x.Rank).Take(3);

            var model = this.mapper.Map<PlaylistFullViewModel>(playlistServiceModel);
            model.Songs = this.mapper.Map<IEnumerable<SongLightViewModel>>(songs);
            model.SongsTop3 = this.mapper.Map<IEnumerable<SongLightViewModel>>(songs.OrderBy(x => x.Rank).Take(3));
            model.Genres = this.mapper.Map<IEnumerable<GenreFullViewModel>>(genres);
            model.PlaylistsSimilar = this.mapper.Map<IEnumerable<PlaylistFullViewModel>>(playlistsSimilarTop5);
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> Create()
        {
            var genresServiceModels = await genreService.GetAllGenresAsync(false, null);
            var genreViewModels = this.mapper.Map<IEnumerable<GenreFullViewModel>>(genresServiceModels).ToList();

            PlaylistCreateFormViewModel model = new PlaylistCreateFormViewModel
            {
                Genres = genreViewModels
            };

            return await Task.Run(() => View(model));
        }

        [HttpPost]
        [Authorize]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PlaylistCreateFormViewModel input)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(input);
            }

            var userDataModel = await this.userManager.GetUserAsync(this.User);
            var userServiceModel = await this.userService.GetUserByIdAsync(userDataModel.Id);

            // Create new Playlist in Database (no Image, no DurationTravel).
            PlaylistServiceModel playlistToCreate = new PlaylistServiceModel
            {
                Name = input.Name,
                Description = input.Description,
                UserId = userServiceModel.Id,
                UserName = userServiceModel.FirstName + " " + userServiceModel.LastName,
                UserImage = userServiceModel.Image,
            };
            PlaylistServiceModel playlistCreated = await playlistService.CreatePlaylistAsync(playlistToCreate);

            // Upload image and save it in wwwroot.
            await ControllerExtensionMethods.UploadImageFile(playlistToCreate.Id, "playlists", input.ImageFile, true);

            // Set image path to Playlist as Image property.
            await playlistService.UpdatePlaylistImageAsync(playlistCreated.Id);

            //Get DurationTravel from Bing Location API.
            int durationTravel = await playlistService.GetDurationTravelAsync(input.PointFirstQueryString, input.PointSecondQueryString);
            await playlistService.UpdatePlaylistDurationTravelAsync(playlistCreated.Id, durationTravel);

            // Get Genres From input parameter.
            var genresIdsChosenByUser = input.Genres.Where(x => x.IsSelected == true).Select(y => y.Id);

            bool isMissionAccomplished = await playlistService.GeneratePlaylistAsync(playlistCreated.Id, durationTravel, genresIdsChosenByUser, input.IsTopTracksOptionEnabled, input.IsTracksFromSameArtistEnabled);

            return RedirectToAction("Success", "Home", new SuccessViewModel
            {
                urlGeneral = "/playlists/all",
                urlItemCreated = $"/Playlists/PlaylistById/{playlistCreated.Id}"
            });
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Remove(Guid id, bool isRemovedByAdmin, int page = 1)
        {
            await playlistService.DeletePlaylistAsync(id, false, null);

            if (isRemovedByAdmin)
            {
                return Redirect($"/Admins/Playlists?page={page}");
            }
            return Redirect($"/Playlists/All?page={page}");
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> SwapUnlistedStatus(Guid id, bool isSwappedByAdmin, int page = 1)
        {
            await playlistService.SwapUnlistedStatusByIdAsync(id);

            if (isSwappedByAdmin)
            {
                return Redirect($"/Admins/Playlists?page={page}");
            }
            return Redirect($"/Playlists/All?page={page}");
        }

        [Authorize]
        public async Task<IActionResult> Update(Guid id)
        {
            PlaylistServiceModel playlistToUpdate = await this.playlistService.GetPlaylistByIdAsync(id);

            var input = this.mapper.Map<PlaylistFullViewModel>(playlistToUpdate);

            return this.View(input);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Update(PlaylistFullViewModel input)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(input);
            }

            PlaylistServiceModel playlistToUpdate = new PlaylistServiceModel
            {
                Id = input.Id,
                Name = input.Name,
                Description = input.Description
            };

            await this.playlistService.UpdatePlaylistAsync(playlistToUpdate);

            // Upload image and save it in wwwroot.
            await ControllerExtensionMethods.UploadImageFile(input.Id, "playlists", input.ImageFile, false);

            return RedirectToAction("Success", "Home", new SuccessViewModel
            {
                urlGeneral = "/Playlists/All",
                urlItemCreated = $"/Playlists/PlaylistById/{input.Id}"
            });
        }

        public async Task<IEnumerable<PlaylistFullViewModel>> AjaxMethod(string substring)
        {
            var playlistsThatContainSubstring = await this.playlistService.GetPlaylistsByNameContainsSubstring(substring);

            return this.mapper.Map<IEnumerable<PlaylistFullViewModel>>(playlistsThatContainSubstring);
        }
    }
}
