﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Models.Home;
using PlaylistGenerator.Web.Models;
using PlaylistGenerator.Web.Models.Home;
using PlaylistGenerator.Web.Models.News;
using PlaylistGenerator.Web.Models.Playlist;
using PlaylistGenerator.Web.Models.Song;
using PlaylistGenerator.Web.Models.User;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserService userService;
        private readonly IHomeService homeService;
        private readonly ISongService songService;
        private readonly IPlaylistService playlistService;
        private readonly IMapper mapper;

        public HomeController(ILogger<HomeController> logger, IUserService userService, IHomeService homeService, ISongService songService, IPlaylistService playlistService, IMapper mapper)
        {
            _logger = logger;
            this.userService = userService;
            this.homeService = homeService;
            this.playlistService = playlistService;
            this.songService = songService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var top3Playlists = (await this.playlistService.GetAllPlaylistsAsync()).OrderBy(p => p.Rank).Take(3);
            var top3Songs = await this.songService.GetTop3SongsAsync();
            var top3News = await this.homeService.GetMusicNews();

            var playlistViewModels = this.mapper.Map<IEnumerable<PlaylistLightViewModel>>(top3Playlists);
            var songViewModels = this.mapper.Map<IEnumerable<SongLightViewModel>>(top3Songs);
            var newsViewModels = this.mapper.Map<IEnumerable<NewsViewModel>>(top3News);

            var model = new HomePageViewModel
            {
                Top3Playlists = playlistViewModels,
                Top3Songs = songViewModels,
                Top3News = newsViewModels
            };

            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]

        public IActionResult Success(SuccessViewModel model)
        {
            try
            {
                return this.View(model);
            }
            catch (ArgumentException ex)
            {
                return Redirect($"/Home/Error?errorMessage={ex.Message}");
            }
        }

        public async Task<IActionResult> GenerateApiKey(Guid id)
        {
            await userService.UpdateUserApiKeyAsync(id);

            return LocalRedirect("/Identity/Account/Manage");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult PageNotFound()
        {
            return View();
        }

        public IActionResult EmailSend()
        {
            return this.View();
        }

        public async Task<IActionResult> About()
        {
            var users = await userService.GetAllUsersAsync();
            var adminsOnly = users.Where(x => x.IsAdmin == true);
            CollectionUserFullViewModels model = new CollectionUserFullViewModels();
            model.Users = mapper.Map<IEnumerable<UserFullViewModel>>(adminsOnly);
            return this.View(model);
        }

        [HttpPost]
        public IActionResult EmailSend(EmailSendViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        EmailSendServiceModel modelService = new EmailSendServiceModel
                        {
                            Subject = model.Subject,
                            Message = model.Message,
                        };

                        var isEmailSent = homeService.SendEmail(modelService);

                        if (isEmailSent)
                        {
                            return RedirectToAction("EmailSuccess", "Home");
                        }
                        else
                        {
                            return RedirectToAction("EmailError", "Home");
                        }
                    }
                    catch (SmtpException ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
            catch (Exception)
            {
                ViewBag.Error = "Some Error";
            }

            return View();
        }

        public IActionResult EmailSuccess()
        {
            return this.View();
        }

        public IActionResult EmailError()
        {
            return this.View();
        }

        public async Task<IActionResult> GetMusicNews()
        {
            var news = await this.homeService.GetMusicNews();
            var model = this.mapper.Map<NewsViewModel>(news);

            return this.PartialView(model);
        }
    }
}
