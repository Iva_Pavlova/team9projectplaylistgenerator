﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Models;
using PlaylistGenerator.Services.Models.Genre;
using PlaylistGenerator.Services.Models.User;
using PlaylistGenerator.Web.Models.Genre;
using PlaylistGenerator.Web.Models.Playlist;
using PlaylistGenerator.Web.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Web.Controllers
{
    public class UsersController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IGenreService genreService;
        private readonly IPlaylistService playlistService;

        public UsersController(UserManager<User> userManager, SignInManager<User> signInManager, IUserService userService, IPlaylistService playlistService, IGenreService genreService, IMapper mapper)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.userService = userService;
            this.playlistService = playlistService;
            this.genreService = genreService;
            this.mapper = mapper;
        }

        [Authorize]
        public async Task<IActionResult> All(int page = 1)
        {
            IEnumerable<UserServiceModel> collectionUserServiceModels = await userService.GetAllUsersAsync(false, page);
            int pageSizeCount = this.userService.GetPageCountSizing();
            int totalCount = this.userService.GetTotalUsersCount();

            CollectionUserFullViewModels model = new CollectionUserFullViewModels();

            model.Users = collectionUserServiceModels.Select(x => new UserFullViewModel
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                DateCreated = x.DateCreated,
                DateModified = x.DateModified,
                IsBanned = x.IsBanned,
                IsAdmin = x.IsAdmin,
                IsDeleted = x.IsDeleted,
                Image = x.Image,
                PlaylistsCount = x.PlaylistsCount,
            });

            model.Url = "/Users/All";
            model.PageSize = pageSizeCount;
            model.CurrentPage = page;
            model.TotalCount = totalCount;

            return this.View(model);
        }

        [Authorize]
        public async Task<IActionResult> UserById(Guid id)
        {
            UserServiceModel userServiceModel = await userService.GetUserByIdAsync(id);

            UserFullViewModel model = this.mapper.Map<UserFullViewModel>(userServiceModel);

            if (userServiceModel.PlaylistsCount != 0)
            {
                IEnumerable<PlaylistServiceModel> playlistsServicesOfUser = await playlistService.GetPlaylistsByUserIdAsync(id);

                IEnumerable<Guid> playlistsIdsOfUser = playlistsServicesOfUser.Select(x => x.Id);

                HashSet<GenreServiceModel> genresPreferred = new HashSet<GenreServiceModel>();

                foreach (var playlistId in playlistsIdsOfUser)
                {
                    var genresOfPlaylists = await genreService.GetGenresByPlaylistId(playlistId);

                    foreach (var genre in genresOfPlaylists)
                    {
                        if (!genresPreferred.Any(x => x.Id == genre.Id))
                        {
                            genresPreferred.Add(genre);
                        }
                    }
                }

                model.Playlists = this.mapper.Map<IEnumerable<PlaylistLightViewModel>>(playlistsServicesOfUser);
                model.GenresPreferred = this.mapper.Map<IEnumerable<GenreFullViewModel>>(genresPreferred);
            }

            return this.View(model);
        }

        public async Task<IActionResult> Remove(Guid id, bool isRemovedByAdmin, int page = 1)
        {
            await userService.DeleteUserAsync(id);

            if (isRemovedByAdmin)
            {
                return Redirect($"/Admins/Users?page={page}");
            }
            return Redirect($"/Users/All?page={page}");
        }

        public async Task<IActionResult> SwapBanStatus(Guid id, bool isSwappedByAdmin, int page = 1)
        {
            await userService.SwapUserBanStatusByIdAsync(id);

            if (isSwappedByAdmin)
            {
                return Redirect($"/Admins/Users?page={page}");
            }
            return Redirect($"/Users/All?page={page}");
        }
    }
}
