# Team9Project3PlaylistGenerator

GitLab repo: https://gitlab.com/Iva_Pavlova/team9projectplaylistgenerator<br/>
Trello board: https://trello.com/b/iVcwicGU/team9project3playlistgenerator<br/>
Azure link: https://playlistravel.azurewebsites.net<br/>

---

## Technologies

---

PlaylistGenerator uses the following additional frameworks:

- **AutoMapper** (10.1.1)
- **Hangfire**.AspNetCore (1.7.17)
- **Hangfire**.SqlServer (1.7.17)
- **jQuery** (3.5.1)
- Microsoft.AspNetCore.**Identity.EntityFrameworkCore** (3.1.9)
- Microsoft.**EntityFrameworkCore**.SqlServer (3.1.9)
- Microsoft.**EntityFrameworkCore**.Tools (3.1.9)
- Microsoft.**EntityFrameworkCore**.InMemory (3.1.9)
- **Moq** (4.15.1)
- **Serilog** (2.10.0)
- **Serilog**.AspNetCore (3.4.0)
- Swashbuckle.AspNetCore.**Swagger**Gen (5.6.3)
- Swashbuckle.AspNetCore.**Swagger**UI (5.6.3)

---

## External Services

---

PlaylistGenerator uses the following external services:

- Deezer (API)  
  https://developers.deezer.com/
- Bing Maps (API)  
  https://www.microsoft.com/en-us/maps/documentation
- News API  
  https://newsapi.org
- Creative Tim (Bootstrap Template)  
  https://www.creative-tim.com/

---

## Screenshots

---

### Homepage:

You can go ahead and create a new Playlist for a trip, or you could browse through music news, top Playlists and songs!

![homepage](PlaylistGenerator.Resources/homepage.jpg)

---

### My Playlists:

This is where all the Playlists you've created will be!

![myplaylists_page](PlaylistGenerator.Resources/myplaylists_page.jpg)

---

### Playlists:

Browse through existing Playlists, Sort & Filter to find the type of Playlists you're looking for!

![playlists_page](PlaylistGenerator.Resources/playlists_page.jpg)

---

### Single Playlist:

Found a Playlist you like? Here you can see all the details you need!

![singleplaylist_page](PlaylistGenerator.Resources/singleplaylist_page.jpg)

---

### Users:

Here you can find more Users like you, check out their profiles and Playlists!

![users_page](PlaylistGenerator.Resources/users_page.jpg)

---

### Genres:

You can browse through all the Genres in the app. Enjoying a certain Genre? Check out all Playlists of that Genre!

![genres_page](PlaylistGenerator.Resources/genres_page.jpg)

---

**ADMIN PAGES**

---

### Admin: Genres

An Admin can manually trigger a syncronization of Genres and Songs. Details of last syncronization are also present.

![admin_genres_page](PlaylistGenerator.Resources/admin_genres_page.jpg)

---

### Admin: Playlists

An Admin can Edit, Unlist/Enlist and Delete Playlists.

![admin_playlists_page](PlaylistGenerator.Resources/admin_playlists_page.jpg)

---

## General Information

---

Solution 'PlaylistGenerator' includes 8 projects:

0. Web > PlaylistGenerator.Web > API

1. Web > PlaylistGenerator.Web > MVC

2. Services > PlaylistGenerator.Services
3. Services > PlaylistGenerator.Services.Models
4. Services > PlaylistGenerator.Services.UnitTests
5. Services > PlaylistGenerator.Services.IntegrationTests

6. Data > PlaylistGenerator.Data
7. Data > PlaylistGenerator.Data.Models

8. Console > PlaylistGenerator.Console

---

## Web Application

---

### 0. Web > PlaylistGenerator.Web > API

---

The API functionality is implemented inside the .NET Core 3.1 MVC Web Application.  
All the services provided are organized inside the 'ApiControllers' directory.

To use the API services one needs to provide a valid Api Key into the query string of each request. For example:

https:/localhost:5001/api/playlists?apiKey={API Key here)}
https:/localhost:5001/api/playlists/{playlist Id}?apiKey={API Key here}

Users get their API key through their 'Manage Your Account' profile in the Web Application.
One user has access to only 1 API Key.

---

A screenshot of the Operations - API Documentation by Swagger

![sqlserver_database_main](PlaylistGenerator.Resources/swagger_operations.png)

---

A screenshot of the Schemas - API Documentation by Swagger

![sqlserver_database_main](PlaylistGenerator.Resources/swagger_schemas.png)

---

### 1. Web > PlaylistGenerator.Web > MVC

---

PlaylistGenerator.Web is a .NET Core 3.1 MVC Web Application.

It includes 4 main sections.

0. ApiControllers > Read more in the 'Web Api (REST) Documentation'.

1. Controllers >

2. (View)Models >

3. Views >

---

It also includes 6 additional sections with significant impact on the logic of the project:

1. /Areas/Identity/Pages/Razor pages

- control all that involves identity and users

2. /Extensions/WebHostExtensions.cs

- a static class that contains static extension methods.

3. /logs/log-YYYYMMDD.txt files

- created by Serilog and register all events that the application endures
- added in the .gitignore file so the repository would not contain any of these files

4. /MappingConfiguration/PlaylistGenerationProfile.cs class

- has all the instructions Automapper uses to map:
  - Data Models -> Service Models
  - Service Models <-> View Models

5. /Middlewares/ExceptionHandlingMiddleware.cs

- responsible for handling all errors that the user might hit.
- 404 would redirect to our PageNotFound view
- 500 to our Error view.

---

## Services Layer (Business Logic)

---

### 2. Services > PlaylistGenerator.Services

---

PlaylistGenerator.Services is a .NET Core 3.1 Class Library project.

It includes:

- /Contracts
  - includes a number of interfaces that raise the level of abstraction
- /Extensions/ServiceExtensionMethods.cs
  - a class with a set of static extension methods that are used throughout the project
- /Providers/DateTimeProvider.cs
  - has a GetDateTime() method that returns DateTime.UtcNow
- AlbumService.cs
- ArtistService.cs
- GenreService.cs
- HomeService.cs
- PlaylistService.cs
- SongService.cs
- SynchronizationService.cs
- UserService.cs

---

### 3. Services > PlaylistGenerator.Services.Models

---

PlaylistGenerator.Services.Models is a .NET Core 3.1 Class Library project.

It includes:

- /Album
- /Artist
- /Genre
- /Home
- /News
- /Playlist
- /Song
- /Synchronization
- /User

---

### 4. Services > PlaylistGenerator.Services.UnitTests

---

PlaylistGenerator.Services.UnitTests is a .NET Core 3.1 MSTest Test Project.

It includes **147 unit tests** that examine the successful and constant behavior of the business logic of the Services Layer.

Microsoft.EntityFrameworkCore.InMemory Version=3.1.9 is used for the test project.

The database provider provides an in-memory database for the tests to use.

---

### 5. Services > PlaylistGenerator.Services.IntegrationTests

---

PlaylistGenerator.Services.IntegrationTests is a .NET Core 3.1 MSTest Test Project.

It includes **7 unit tests** that examine the successful behavior of the Services Layer where foreign APIs are called.

Microsoft.EntityFrameworkCore.InMemory Version=3.1.9 is used for the test project.

The database provider provides an in-memory database for the tests to use.

---

## Data Layer (Database)

---

### 6. Data > PlaylistGenerator.Data

---

PlaylistGenerator.Data is a .NET Core 3.1 Class Library project.

It includes:

- /Configuration - contains a series of instructions on the relations between entities in the database
- /Migrations - contains a series of migrations that changed the database structure
- PlaylistGeneratorDbContext.cs - creates a context for the database
- PlaylistGeneratorDbSeeder.cs - contains a series of methods that, once called, seed the database with entities

---

### 7. Data > PlaylistGenerator.Data.Models

---

PlaylistGenerator.Data.Models is a .NET Core 3.1 Class Library project.

It includes:

- /BingApiObjects - all classes for objects that a JSON coming from the Bing Api would contain
- /DeezerObjects - all classes for objects that a JSON coming from the Deezer Api would contain
- Album.cs
- Artist.cs
- Genre.cs
- Playlist.cs
- PlaylistGenre.cs - creates many-to-many relationship between Playlists and Genres
- PlaylistSong.cs - creates many-to-many relationship between Playlists and Songs
- Role.cs - is responsible for Identity
- Song.cs
- Synchronization.cs
- User.cs - is responsible for Identity

---

### Database / Class Diagrams

---

![sqlserver_database_main](PlaylistGenerator.Resources/sqlserver_database_main.png)

---

### Database / Identity

---

![sqlserver_database_identity](PlaylistGenerator.Resources/sqlserver_database_identity.png)

---

## Console Application (Seed purposes)

---

### 8. Console > PlaylistGenerator.Console

---

PlaylistGenerator.Console is a .NET Core 3.1 Web Application project.

The Main() method in Program.cs calls a series of methods that seed information in the Database of the application.

They are called in a certain order, as entities in the database have certain relations that create dependencies between them.

_For example, a Song should not be created as long as the Author and the Album of this Song are already registered in the Database._

Here is the order of the methods called in Main():

🏁  
await LoadGenresInDb(db, mapper);  
👇  
await LoadSongsInDb(db, mapper); // Also adds Artists and Albums  
👇  
SeedRoles(db);  
👇  
SeedUsersRoleAdmin(db);  
👇  
SeedUsersRoleUser(db);  
👇  
SeedPlaylists(db);  
🏁

---

## How do I clone and run the project locally?

1. After you clone the project, you need to go to Microsoft SQL Server Management Studio and _create a new Database for Hangfire_ (Hangfire is used for sceduled background tasks in the project and needs its own database):

![create_hangfire_database](PlaylistGenerator.Resources/Create_Hangfire_Database.png)

2. Open the project in your IDE. If you want to seed some data (Randomly generated Playlists and Users) in order to see what the app would look like if it was running online, _update your database to the latest migration_ and _run the PlaylistGenerator.Console Project_; This will take a while since it also contacts a foreign API (Deezer).

![run_console_project](PlaylistGenerator.Resources/Run_Console_Project.png)

3. You are all set! You can now run the PlaylistGenerator.Web Project.  
   _Note:_ If you did not seed any data using the PlaylistGenerator.Console, the first time you run the app will take a while, since it will contact the Deezer API to Load data aboud Music.

---

## Credits

---

![thanks_guys](PlaylistGenerator.Resources/credits_trainers_mentors.jpg)  
We really appreciate all we learnt from you.  
All those hours you spent to answer dozens of questions early in the morning, around midnight, not to mention the weekends.  
We thank you so much!  
Hats off.
